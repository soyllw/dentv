#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

require 'pathname.rb'

LOWER_SINGLE = {
      "і"=>"i","ґ"=>"g","ё"=>"yo","№"=>"#","є"=>"e",
      "ї"=>"yi","а"=>"a","б"=>"b",
      "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"zh",
      "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
      "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
      "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
      "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"'",
      "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
}
LOWER_MULTI = {
  "ье"=>"ie",
  "ьё"=>"ie",
}
UPPER_SINGLE = {
  "Ґ"=>"G","Ё"=>"YO","Є"=>"E","Ї"=>"YI","І"=>"I",
  "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
  "Д"=>"D","Е"=>"E","Ж"=>"ZH","З"=>"Z","И"=>"I",
  "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
  "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
  "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
  "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"'","Ы"=>"Y","Ь"=>"",
  "Э"=>"E","Ю"=>"YU","Я"=>"YA",
}
UPPER_MULTI = {
  "ЬЕ"=>"IE",
  "ЬЁ"=>"IE",
}
LOWER = (LOWER_SINGLE.merge(LOWER_MULTI)).freeze
UPPER = (UPPER_SINGLE.merge(UPPER_MULTI)).freeze
MULTI_KEYS = (LOWER_MULTI.merge(UPPER_MULTI)).keys.sort_by {|s| s.length}.reverse.freeze

def transliterate(str)
  chars = str.scan(%r{#{MULTI_KEYS.join '|'}|\w|.})
  result = ""
  chars.each_with_index do |char, index|
    if UPPER.has_key?(char) && LOWER.has_key?(chars[index+1])
      # combined case
      result << UPPER[char].downcase.capitalize
    elsif UPPER.has_key?(char)
      result << UPPER[char]
    elsif LOWER.has_key?(char)
      result << LOWER[char]
    else
      result << char
    end
  end
  result
end

=begin
if not f.end_with? ".360p.mp4", ".720p.mp4"
  if not File.exists? "#{f}.360p.mp4"
    # 360p
    `ffmpeg -i #{f} -vprofile main -threads 0 -vcodec libx264 -preset slow -g 5 -f mp4 -s 640x360 -b:v 1000k -maxrate 1000k -bufsize 3000k -b:a 128k -strict experimental #{f}.360p.conv.mp4`
    `qt-faststart #{f}.360p.conv.mp4 #{f}.360p.mp4`
    `rm #{f}.360p.conv.mp4`
  end
  if not File.exists? "#{f}.720p.mp4"
    # 720p
    `ffmpeg -i #{f} -vprofile high -threads 0 -vcodec libx264 -preset slow -g 5 -f mp4 -s 1280x720 -b:v 3500k -maxrate 3500k -bufsize 7000k -b:a 160k -strict experimental #{f}.720p.conv.mp4`
    `qt-faststart #{f}.720p.conv.mp4 #{f}.720p.mp4`
    `rm #{f}.720p.conv.mp4`
  end
end
=end

def convert(indir, outdir)
  Dir.glob("#{indir}/*").each do |f|
    infile = Pathname.new f
    ext = infile.extname
    output = transliterate infile.basename.sub_ext('').to_s
    output.gsub! /[^a-zA-Z0-9]+/iu, '_'
    output = outdir + output
    `ffmpeg -i '#{infile.cleanpath}' -vprofile main -threads 0 -vcodec libx264 -preset slow -g 25 -f mp4 -s 640x360 -b:v 1000k -maxrate 1000k -bufsize 3000k -b:a 128k -strict experimental '#{output}.360p.conv.mp4'`
    `qt-faststart '#{output}.360p.conv.mp4' '#{output}.360p.mp4'`
    `rm '#{output}.360p.conv.mp4'`
    `ffmpeg -i '#{infile.cleanpath}' -vprofile high -threads 0 -vcodec libx264 -preset slow -g 25 -f mp4 -s 1280x720 -b:v 3500k -maxrate 3500k -bufsize 7000k -b:a 160k -strict experimental '#{output}.720p.conv.mp4'`
    `qt-faststart '#{output}.720p.conv.mp4' '#{output}.720p.mp4'`
    `rm '#{output}.720p.conv.mp4'`
  end
end

if ARGV.length == 2
  convert ARGV[0], ARGV[1]
else
  puts "Usage: video_conv.rb <video_files_directory> <output_directory>"
end