var swiper = new Swiper('.swiper-container', {
    /*speed: 10500,*/
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
    effect: 'fade',
    autoplay: 10500,
    autoplayDisableOnInteraction: false
});