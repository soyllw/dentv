(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    definition(module.exports, localRequire(name), module);
    var exports = cache[name] = module.exports;
    return exports;
  };

  var require = function(name) {
    var path = expand(name, '.');

    if (has(cache, path)) return cache[path];
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex];
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '"');
  };

  var define = function(bundle) {
    for (var key in bundle) {
      if (has(bundle, key)) {
        modules[key] = bundle[key];
      }
    }
  }

  globals.require = require;
  globals.require.define = define;
  globals.require.brunch = true;
})();

window.require.define({"application": function(exports, require, module) {
  var routes, ViliApplication, Application;
  routes = require('routes');
  ViliApplication = require('vili/application');
  module.exports = Application = (function(superclass){
    var prototype = extend$((import$(Application, superclass).displayName = 'Application', Application), superclass).prototype, constructor = Application;
    prototype.routes = routes;
    prototype.firstRun = true;
    function Application(){
      superclass.apply(this, arguments);
    }
    return Application;
  }(ViliApplication));
  function extend$(sub, sup){
    function fun(){} fun.prototype = (sub.superclass = sup).prototype;
    (sub.prototype = new fun).constructor = sub;
    if (typeof sup.extended == 'function') sup.extended(sub);
    return sub;
  }
  function import$(obj, src){
    var own = {}.hasOwnProperty;
    for (var key in src) if (own.call(src, key)) obj[key] = src[key];
    return obj;
  }
}});

window.require.define({"initialize": function(exports, require, module) {
  var Application;
  Application = require('application');
  $(function(){
    var app;
    moment.lang('ru');
    app = new Application();
    return app.initialize();
  });
}});

window.require.define({"lib/utils": function(exports, require, module) {
  (function() {
    var utils;

    utils = {
      paginate: function(meta) {
        var data, end, n, pageNumber, start, total;
        pageNumber = 1 + parseInt(meta.offset / meta.limit);
        total = 1 + parseInt(meta.total_count / meta.limit);
        start = pageNumber >= 5 ? pageNumber : 1;
        end = _.min([start + 10, total]);
        data = {
          range: (function() {
            var _results;
            _results = [];
            for (n = start; start <= end ? n <= end : n >= end; start <= end ? n++ : n--) {
              _results.push(n);
            }
            return _results;
          })(),
          hasPrev: start > 1,
          hasNext: end < total,
          number: pageNumber
        };
        return data;
      }
    };

    module.exports = utils;

  }).call(this);
  
}});

window.require.define({"lib/video_thumb": function(exports, require, module) {
  var VideoThumb;
  module.exports = VideoThumb = (function(){
    VideoThumb.displayName = 'VideoThumb';
    var prototype = VideoThumb.prototype, constructor = VideoThumb;
    prototype.selector = null;
    prototype.animating = false;
    prototype.hovering = false;
    prototype.current = 0;
    prototype.total = 0;
    prototype.timer = null;
    prototype.options = {
      autoStart: true,
      selector: null,
      delay: 12000,
      transitionDelay: 600,
      withPager: true,
      pagerClass: 'pager',
      pageSelectedClass: 'selected'
    };
    prototype.constructor = function(options){
      this.options = _.extend(this.options, options);
      this.selector = $(this.options.selector);
      this.total = this.selector.children().size() - 1;
      this.selector.on('mouseover', this.hoverChange);
      this.selector.on('mouseout', this.hoverChange);
      if (this.options.withPager) {
        this.createPager();
      }
      if (this.options.autoStart) {
        return this.timer = setTimeout(this.switchToNextPage, this.options.delay);
      }
    };
    prototype.setTimer = function(){
      if (this.timer !== null) {
        return this.timer = setTimeout(this.switchToNextPage, this.options.delay);
      }
    };
    prototype.hoverChange = function(){
      this.hovering = !this.hovering;
      return this.setTimer();
    };
    prototype.createPager = function(){
      var i, to$, el;
      this.pager = $("<ul class='" + this.options.pagerClass + "'></ul>");
      this.selector.parent().parent().append(this.pager);
      for (i = 0, to$ = this.total; i <= to$; ++i) {
        el = $('<a href="#"></a>');
        if (i === 0) {
          el.addClass(this.options.pageSelectedClass);
        }
        this.pager.append($('<li></li>').append(el));
      }
      return this.pager.on('click', 'li a', this.switchPage);
    };
    prototype.switchPage = function(evt){
      var num;
      num = $(evt.target).parent().prevAll().size();
      this.animateSwitch(num);
      return false;
    };
    prototype.switchToNextPage = function(){
      if (this.current === this.total) {
        return this.animateSwitch(0);
      } else {
        return this.animateSwitch(this.current + 1);
      }
    };
    prototype.animateSwitch = function(num){
      var prevWidths, totalShift, animateOptions;
      if (this.animating || this.hovering) {
        return;
      }
      this.animating = true;
      prevWidths = this.selector.children().slice(0, num).map()(function(){
        var self;
        self = $(this);
        return self.outerWidth() + parseInt(self.css('marginLeft')) + parseInt(self.css('marginRight'));
      });
      totalShift = _.reduce(prevWidths, function(a, n){
        return a + n;
      }, 0);
      animateOptions = {
        marginLeft: -totalShift
      };
      this.selector.animate(animateOptions, this.options.transitionDelay, this.animationComplete);
      if (this.options.withPager) {
        $('a', this.pager).removeClass(this.options.pageSelectedClass);
        $('a', this.pager).eq(num).addClass(this.options.pageSelectedClass);
      }
      return this.current = num;
    };
    prototype.animationComplete = function(){
      this.animating = false;
      return this.setTimer();
    };
    prototype.dispose = function(){
      this.timer = null;
      this.selector.off('**');
      if (this.options.withPager) {
        return this.pager.off('**');
      }
    };
    function VideoThumb(){
      this.animationComplete = bind$(this, 'animationComplete', prototype);
      this.animateSwitch = bind$(this, 'animateSwitch', prototype);
      this.switchToNextPage = bind$(this, 'switchToNextPage', prototype);
      this.switchPage = bind$(this, 'switchPage', prototype);
      this.hoverChange = bind$(this, 'hoverChange', prototype);
    }
    return VideoThumb;
  }());
  function bind$(obj, key, target){
    return function(){ return (target || obj)[key].apply(obj, arguments) };
  }
}});

window.require.define({"models/entries": function(exports, require, module) {
  var Entry, Entries;
  Entry = (function(superclass){
    var prototype = extend$((import$(Entry, superclass).displayName = 'Entry', Entry), superclass).prototype, constructor = Entry;
    function Entry(){}
    return Entry;
  }(Backbone.Model));
  Entries = (function(superclass){
    var prototype = extend$((import$(Entries, superclass).displayName = 'Entries', Entries), superclass).prototype, constructor = Entries;
    prototype.model = Entry;
    prototype.url = '/edda/v1/entries/';
    function Entries(){}
    return Entries;
  }(Backbone.Collection));
  module.exports = {
    Entry: Entry,
    Entries: Entries
  };
  function extend$(sub, sup){
    function fun(){} fun.prototype = (sub.superclass = sup).prototype;
    (sub.prototype = new fun).constructor = sub;
    if (typeof sup.extended == 'function') sup.extended(sub);
    return sub;
  }
  function import$(obj, src){
    var own = {}.hasOwnProperty;
    for (var key in src) if (own.call(src, key)) obj[key] = src[key];
    return obj;
  }
}});

window.require.define({"routes": function(exports, require, module) {
  module.exports = function(route){
    route("", "home");
    route("content/view/:slug/", "entry");
  };
}});

window.require.define({"templates/base": function(exports, require, module) {
  module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {
  attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
  var buf = [];
  with (locals || {}) {
  var interp;
  var __indent = [];
  buf.push('\n<header>\n  <header><a href="/" class="sublogo">День-ТВ. Аналитический онлайн-телеканал</a>\n  </header>\n</header>');
  }
  return buf.join("");
  };
}});

window.require.define({"templates/entry": function(exports, require, module) {
  module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {
  attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
  var buf = [];
  with (locals || {}) {
  var interp;
  var __indent = [];
  buf.push('\n<header>\n  <header><a href="/" class="sublogo">День-ТВ. Аналитический онлайн-телеканал</a>\n    <h6>О программе</h6>\n    <div class="standard-block">');
  if ( false)
  {
  buf.push('<img src="entry.program.logo" width="750" height="88"/>');
  }
  else
  {
  buf.push('\n      <div class="program-logo-placeholder"></div>');
  }
  buf.push('\n      <h5>entry.program.title</h5>\n    </div>\n  </header>\n</header>\n<article class="main-column">\n  <div class="page-content">\n    <ul class="entryparts">\n      <li>entrparts-list-here</li>\n      <li class="info"><span class="comments-count"><i class="icon"></i>entry.comments_count</span>\n        <time datetime="%Y-%m-%d%H:%MZ" class="pubdate">entry.released_at|ru_strftime(True, \'%d %B, %H:%M\')</time>\n      </li>\n      <li class="info">\n        <dl class="rating">\n          <dt><i class="icon">Рейтинг</i></dt>\n          <dd><span>entry.get_rating</span></dd>\n        </dl>\n        <dl class="tags">\n          <dt><i class="icon"></i>Теги:</dt>\n          <dd>\n            <ul>\n              <li><a href="tag.get_absolute_url()">tag.name</a></li>\n            </ul>\n          </dd>\n        </dl>\n      </li>\n      <li class="social">\n        <div data-lang="ru" data-via="DenTvRu" data-count="vertical" class="socialite twitter-share"></div>\n        <div data-type="vertical" data-verb="1" data-height="24" class="socialite vkontakte-like"></div>\n        <div data-send="false" data-layout="box_count" data-font="arial" data-show-faces="false" data-width="65" class="socialite facebook-like"></div>\n        <div data-size="tall" class="socialite googleplus-one"></div>\n      </li>\n    </ul>\n    <div>другие выпуски и прочее</div>\n  </div>\n</article>\n<aside class="side-column">\n  <h6>Статистика программы</h6>\n  <div class="block info">\n    <dl>\n      <dt>Создана:</dt>\n      <dd>entry.program.created_at|ru_strftime(True, \'%d %B %Y\')</dd>\n      <dt>Всего выпусков</dt>\n      <dd>entry.program.entries.count()</dd>\n      <dt>Последний выпуск:</dt>\n      <dd>entry.program.get_latest_entry().released_at|ru_strftime(True,\'%d %B %Y\')</dd>\n      <dt>Подписчиков:</dt>\n      <dd>entry.program.subscribers_count</dd>\n      <dt>Всего комментариев:</dt>\n      <dd>entry.program.comments_count</dd>\n    </dl>\n  </div>\n  <h6>Программа в интернете</h6>\n  <div class="block info">\n    <dl class="links">\n      <dt class="rss"></dt>\n      <dd><a href="programs.feed.item">RSS подписка</a></dd>\n    </dl>\n  </div>\n  <dl class="participants"></dl><!--[if !user || !user.is_authenticated()]>\n  <h6>Регистрация</h6>\n  <div class="block"><a href="/accounts/register/" class="register-banner"></a></div><![endif]-->\n  <h6><a href="news.view.list">Срочное &ndash; кратко</a>\n    <ul class="block info news"></ul><!--for item in latest_news\n    <li>\n      <h5><a href="item.get_absolute_url">item.pub_date fuzzy_distance(common[\'now\'])</a></h5>\n      <p><a href="item.get_absolute_url">item.title</a></p>\n    </li>-->\n  </h6>\n  <div class="socialite facebook-likebox">\n    <div data-id="38085148" class="socialite vkontakte-group"></div>\n  </div>\n</aside>');
  }
  return buf.join("");
  };
}});

window.require.define({"templates/home": function(exports, require, module) {
  module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {
  attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
  var buf = [];
  with (locals || {}) {
  var interp;
  var __indent = [];
  buf.push('\n<header>\n  <article class="top-videos">\n    <div class="slider">\n      <ul class="videos"></ul>\n    </div>\n  </article>\n  <article class="actual-slider">\n    <h6>Актуальные темы</h6>\n    <ul></ul>\n  </article>\n</header>\n<article class="main-column">\n  <article>\n    <h6>Основное</h6>\n    <ul class="video-tiles page-content"></ul>\n  </article>\n</article>\n<aside class="side-column">\n  <h6>Присоединяйтесь к нам</h6>\n  <ul class="block follow">\n    <li><a href="http://www.facebook.com/DenTvRu" class="facebook"></a></li>\n    <li><a href="http://vk.com/public38085148" class="vk"></a></li>\n    <li><a href="http://twitter.com/DenTvRu" class="twitter"></a></li>\n    <li><a href="http://www.youtube.com/channel/UCZNKg6NI7mah4i3dE6m0oYw" class="youtube"></a></li>\n  </ul><!--[if !user || !user.is_authenticated()]>\n  <h6>Регистрация</h6>\n  <div class="block"><a href="/accounts/register/" class="register-banner"></a></div><![endif]-->\n  <h6><a href="news.view.list">Срочное &ndash; кратко</a>\n    <ul class="block info news"></ul><!--for item in latest_news\n    <li>\n      <h5><a href="item.get_absolute_url">item.pub_date fuzzy_distance(common[\'now\'])</a></h5>\n      <p><a href="item.get_absolute_url">item.title</a></p>\n    </li>-->\n  </h6>\n  <div class="socialite facebook-likebox">\n    <div data-id="38085148" class="socialite vkontakte-group"></div>\n  </div>\n</aside>');
  }
  return buf.join("");
  };
}});

window.require.define({"templates/sidebar": function(exports, require, module) {
  module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {
  attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
  var buf = [];
  with (locals || {}) {
  var interp;
  var __indent = [];
  buf.push('\n<header>\n  <header><a href="/" class="sublogo">День-ТВ. Аналитический онлайн-телеканал</a>\n  </header>\n</header>\n<article class="main-column">\n</article>\n<aside class="side-column">\n  <h6>Присоединяйтесь к нам</h6>\n  <ul class="block follow">\n    <li><a href="http://www.facebook.com/DenTvRu" class="facebook"></a></li>\n    <li><a href="http://vk.com/public38085148" class="vk"></a></li>\n    <li><a href="http://twitter.com/DenTvRu" class="twitter"></a></li>\n    <li><a href="http://www.youtube.com/channel/UCZNKg6NI7mah4i3dE6m0oYw" class="youtube"></a></li>\n  </ul><!--[if !user || !user.is_authenticated()]>\n  <h6>Регистрация</h6>\n  <div class="block"><a href="/accounts/register/" class="register-banner"></a></div><![endif]-->\n  <h6><a href="news.view.list">Срочное &ndash; кратко</a>\n    <ul class="block info news"></ul><!--for item in latest_news\n    <li>\n      <h5><a href="item.get_absolute_url">item.pub_date fuzzy_distance(common[\'now\'])</a></h5>\n      <p><a href="item.get_absolute_url">item.title</a></p>\n    </li>-->\n  </h6>\n  <div class="socialite facebook-likebox">\n    <div data-id="38085148" class="socialite vkontakte-group"></div>\n  </div>\n</aside>');
  }
  return buf.join("");
  };
}});

window.require.define({"views/entry": function(exports, require, module) {
  var ViliView, template, EntryView;
  ViliView = require('vili/view');
  template = require('templates/entry');
  module.exports = EntryView = (function(superclass){
    var prototype = extend$((import$(EntryView, superclass).displayName = 'EntryView', EntryView), superclass).prototype, constructor = EntryView;
    prototype.template = template;
    prototype.options = {
      selector: 'section.bodycopy > article'
    };
    function EntryView(){
      superclass.apply(this, arguments);
    }
    return EntryView;
  }(ViliView));
  function extend$(sub, sup){
    function fun(){} fun.prototype = (sub.superclass = sup).prototype;
    (sub.prototype = new fun).constructor = sub;
    if (typeof sup.extended == 'function') sup.extended(sub);
    return sub;
  }
  function import$(obj, src){
    var own = {}.hasOwnProperty;
    for (var key in src) if (own.call(src, key)) obj[key] = src[key];
    return obj;
  }
}});

window.require.define({"views/home": function(exports, require, module) {
  var ViliView, template, entries, HomeView;
  ViliView = require('vili/view');
  template = require('templates/home');
  entries = require('models/entries');
  module.exports = HomeView = (function(superclass){
    var prototype = extend$((import$(HomeView, superclass).displayName = 'HomeView', HomeView), superclass).prototype, constructor = HomeView;
    prototype.template = template;
    prototype.options = {
      selector: 'section.bodycopy > article'
    };
    prototype.initialize = function(){
      this.entries = new entries.Entries;
      return this.entries.fetch();
    };
    function HomeView(){
      superclass.apply(this, arguments);
    }
    return HomeView;
  }(ViliView));
  function extend$(sub, sup){
    function fun(){} fun.prototype = (sub.superclass = sup).prototype;
    (sub.prototype = new fun).constructor = sub;
    if (typeof sup.extended == 'function') sup.extended(sub);
    return sub;
  }
  function import$(obj, src){
    var own = {}.hasOwnProperty;
    for (var key in src) if (own.call(src, key)) obj[key] = src[key];
    return obj;
  }
}});

window.require.define({"views/subviews/homevideofeed": function(exports, require, module) {
  var ViliView, template, HomeVideoFeedView;
  ViliView = require('vili/view');
  template = 'templates/subviews/homevideofeed';
  module.exports = HomeVideoFeedView = (function(superclass){
    var prototype = extend$((import$(HomeVideoFeedView, superclass).displayName = 'HomeVideoFeedView', HomeVideoFeedView), superclass).prototype, constructor = HomeVideoFeedView;
    prototype.template = template;
    prototype.models = {
      feed: null
    };
    prototype.options = {
      selector: '.main-column'
    };
    prototype.events = {
      "click ul.pages a": "changePage"
    };
    function HomeVideoFeedView(){
      this.changePage = bind$(this, 'changePage', prototype);
      this.models.feed = [];
    }
    prototype.changePage = function(evt){
      return false;
    };
    prototype.postRender = function(){
      return superclass.prototype.postRender;
    };
    return HomeVideoFeedView;
  }(ViliView));
  function bind$(obj, key, target){
    return function(){ return (target || obj)[key].apply(obj, arguments) };
  }
  function extend$(sub, sup){
    function fun(){} fun.prototype = (sub.superclass = sup).prototype;
    (sub.prototype = new fun).constructor = sub;
    if (typeof sup.extended == 'function') sup.extended(sub);
    return sub;
  }
  function import$(obj, src){
    var own = {}.hasOwnProperty;
    for (var key in src) if (own.call(src, key)) obj[key] = src[key];
    return obj;
  }
}});

window.require.define({"vili/application": function(exports, require, module) {
  var Application;
  module.exports = Application = (function(){
    Application.displayName = 'Application';
    var prototype = Application.prototype, constructor = Application;
    prototype.firstRun = true;
    prototype.router = null;
    prototype.view = null;
    prototype.container = 'body';
    prototype.routeHandler = function(view){
      var this$ = this;
      return function(params){
        params == null && (params = {});
        if (this$.view != null) {
          this$.view.dispose();
        }
        this$.view = new (require("views/" + view))({
          container: this$.container,
          params: params
        });
        if (!this$.firstRun) {
          this$.view.render();
        } else {
          this$.view.postRender();
        }
        return this$.firstRun = false;
      };
    };
    prototype.initRouter = function(){
      this.router = new Backbone.Router;
      $(this.container).on('click', 'a', this.navigate);
      this.routes(this.addRoute);
      return Backbone.history.start({
        pushState: true
      });
    };
    prototype.addRoute = function(pattern, view){
      return this.router.route(pattern, view, this.routeHandler(view));
    };
    prototype.navigate = function(evt){
      var href;
      evt.preventDefault();
      href = $(evt.target).attr('href');
      this.router.navigate(href, true);
      return false;
    };
    prototype.initialize = function(){
      return this.initRouter();
    };
    function Application(){
      this.navigate = bind$(this, 'navigate', prototype);
      this.addRoute = bind$(this, 'addRoute', prototype);
    }
    return Application;
  }());
  function bind$(obj, key, target){
    return function(){ return (target || obj)[key].apply(obj, arguments) };
  }
}});

window.require.define({"vili/view": function(exports, require, module) {
  var ViliView;
  module.exports = ViliView = (function(superclass){
    var prototype = extend$((import$(ViliView, superclass).displayName = 'ViliView', ViliView), superclass).prototype, constructor = ViliView;
    prototype.subviews = null;
    prototype.options = {
      container: null,
      selector: null
    };
    function ViliView(options){
      superclass.call(this, options);
      this.setElement(
      $(this.options.selector, this.options.container));
    }
    prototype.templateData = function(){};
    prototype.render = function(){
      var i$, ref$, len$, subview;
      this.$el.empty().html(
      this.template(this.templateData()));
      this.postRender();
      if (this.subviews != null) {
        for (i$ = 0, len$ = (ref$ = this.subviews).length; i$ < len$; ++i$) {
          subview = ref$[i$];
          subview.render();
        }
      }
    };
    prototype.postRender = function(){};
    prototype.dispose = function(){
      return console.log('disposed');
    };
    return ViliView;
  }(Backbone.View));
  function extend$(sub, sup){
    function fun(){} fun.prototype = (sub.superclass = sup).prototype;
    (sub.prototype = new fun).constructor = sub;
    if (typeof sup.extended == 'function') sup.extended(sub);
    return sub;
  }
  function import$(obj, src){
    var own = {}.hasOwnProperty;
    for (var key in src) if (own.call(src, key)) obj[key] = src[key];
    return obj;
  }
}});

