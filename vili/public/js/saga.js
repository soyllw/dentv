$(function(){
  $('a[rel=related]').click(function(){
    var $this = $(this);
    window.open($this.attr('href') + '?_popup=1', '', 'menubar=no,location=no,resizable=yes,scrollbars=yes,status=no');
    return false;
  });
});