fs = require 'fs'
path = require 'path'

exports.config =
  minify: false
  paths:
    compass: './config.rb'
  files:
    javascripts:
      joinTo:
        'js/app.js': /^app/
        'js/vendor.js': /^vendor/

      order:
        before: [
          'vendor/scripts/jquery-1.8.3.js',
          'vendor/scripts/lodash.js',
          'vendor/scripts/socialite.js'
        ]

    #stylesheets:
    #  joinTo: 'css/style.css'
