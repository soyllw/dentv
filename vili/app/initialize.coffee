Application = require 'application'
Dropdown    = require 'lib/dropdown'
Modal       = require 'lib/modal'
Unislider   = require 'lib/unislider'
Tabs        = require 'lib/tabs'

$ ->
  $('html').removeClass('no-js').addClass 'js'
  new Modal
  new Dropdown $('.nav ul li:has(ul)')
  new Unislider
  new Tabs
  new Application