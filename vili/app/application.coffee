module.exports = class Application
  constructor: ->
    Socialite.setup
      facebook:
        lang: 'ru_RU'
        appId: 346827865371446
      twitter:
        lang: 'ru'
      vkontakte:
        apiId: 2976846
      googleplus:
        lang: 'ru-RU'
    Socialite.load()
