require 'compass-h5bp'
require 'animation'

sass_path = 'app/styles'
images_path = 'app/assets/images'
css_dir = 'public/stylesheets'
fonts_dir = 'app/assets/fonts'
http_fonts_path = '/static/fonts/'

if environment == :production
	http_path = '/'
	http_stylesheets_path = 'http://static.dentv.ru/stylesheets'
	http_images_path = 'http://static.dentv.ru'
	http_generated_images_path = 'http://static.dentv.ru/images'
	http_javascripts_path = 'http://static.dentv.ru/javascripts'
	http_fonts_path = 'http://static.dentv.ru/fonts'
else
	http_path = '/static/'
end

generated_images_dir = 'public/images'

# PNG optimization
on_sprite_saved do |filename|
  `pngcrush -rem alla -reduce -brute #{filename} #{filename}.sup`
  `mv #{filename}.sup #{filename}`
end

# CSSO
on_stylesheet_saved do |filename|
  `node_modules/csso/bin/csso -i #{filename} -o #{filename}`
end
