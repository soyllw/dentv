#-*- coding: utf-8 -*-
import sys, os
import inspect

sys.path.append(os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '../front')))


from front import settings
from django.core.management import setup_environ
from django.utils.html import strip_tags
from django.utils.encoding import force_unicode
setup_environ(settings)

extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.viewcode',
              'sphinxcontrib.fancybox',
              'qubic.sphinx.graphvizinclude',
              'sphinx.ext.inheritance_diagram']
templates_path = ['_templates']
language = 'ru'
source_suffix = '.rst'
master_doc = 'index'
project = u'DenTv'
copyright = u'2012, Konstantin Kirillov, Vladimir Shemankov, H359'
version = '1.0'
release = '1.0'
exclude_patterns = ['_build']
pygments_style = 'sphinx'
html_theme = 'default'
html_static_path = ['_static']
htmlhelp_basename = 'DenTvdoc'

autoclass_content = "class"
autodoc_member_order = "bysource"

latex_elements = {
    'papersize': 'a4paper',
    'pointsize': '11pt'
}
latex_show_urls = 'footnote'

latex_documents = [
  ('index', 'DenTv.tex', u'Документация веб-приложения проекта "DenTv"',
   u'Konstantin Kirillov, Vladimir Shemankov, H359', 'manual'),
]

inheritance_graph_attrs = dict(rankdir='LR', fontsize=18, ratio='compress')

def process_docstring(app, what, name, obj, options, lines):
    # This causes import errors if left outside the function
    from django.db import models
    
    # Only look at objects that inherit from Django's base model class
    if inspect.isclass(obj) and issubclass(obj, models.Model):
        # Grab the field list from the meta class
        fields = obj._meta._fields()
    
        for field in fields:
            # Decode and strip any html out of the field's help text
            help_text = strip_tags(force_unicode(field.help_text))
            
            # Decode and capitalize the verbose name, for use if there isn't
            # any help text
            verbose_name = force_unicode(field.verbose_name).capitalize()
            
            if help_text:
                # Add the model field to the end of the docstring as a param
                # using the help text as the description
                lines.append(u':param %s: %s' % (field.attname, help_text))
            else:
                # Add the model field to the end of the docstring as a param
                # using the verbose name as the description
                lines.append(u':param %s: %s' % (field.attname, verbose_name))
                
            # Add the field's type to the docstring
            lines.append(u':type %s: %s' % (field.attname, type(field).__name__))
    
    # Return the extended docstring
    return lines  
  
def setup(app):
    # Register the docstring processor with sphinx
    app.connect('autodoc-process-docstring', process_docstring)  