.. DenTv documentation master file, created by
   sphinx-quickstart on Tue Sep 18 23:33:11 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документация веб-приложения проекта "День-ТВ"
=============================================

Общее устройство приложения
---------------------------

Приложение реализовано на базе фреймворка `Django <http://www.djangoproject.com>`_. В качестве
основной БД используется СУБД `Postgre <http://www.postgresql.org>`_. Подсистема кеша использует
хранилище `Redis <http://redis.io>`_.

Отображение предметной области
------------------------------

Предметная область кодируется с помощью DSL django-моделей.

Пользователи (описываемые моделью :class:`siteuser.models.User`), являются как обычными посетителями
сайта, так и ведущими, гостями и другими участниками выпусков. Некоторые пользователи могут владеть программой, которая в таком случае трактуется как видеоблог, но имеет все "возможности" программы. Каждый участник выпуска может, при желании, участвовать в комментировании, отвечать на вопросы пользователей и т.д.

Пользователи могут своершать вход на сайт как с помощью обычных реквизитов доступа, так и с помощью аккаунтов социальных сетей.

Программы отражаются моделью :class:`programs.models.Program`. Кроме основных полей (название, время
создания и т.п.) программа имеет владельца. Если программа не нуждается в специальном владельце, то
используется специальный фиктивный "системный" владелец.

Программы содержат выпуски. Выпуск отражается набором моделей:

* :class:`entries.models.Entry` -- основная модель, описывающая выпуск
* :class:`entries.models.EntryPart` -- модель, описывающая составные части выпуска (видео, текст)
* :class:`entries.models.EntryUserRole` -- модель, отражающая нагруженную связь ``Entry``<->``User``,
  добавляет к этой связи поля, которые описывают роль пользователя в выпуске.

Выпуски могут быть комментируемы -- через обобщенную связь модели :class:`entries.models.Entry` и комментария :class:`comments.models.Comment`. Аналогично, с помощью :class:`voting.models.Vote` осуществляется рейтингование выпуска (а также комментариев).

Участникам выпуска пользователями могут быть заданы вопросы, которые описываются моделями:

* :class:`entries.models.Question` -- основная модель вопроса
* :class:`entries.models.QuestionSupport` -- модель поддержки вопроса, которая выражает голосование
  пользователей за тот или иной вопрос.

К программе, выпуску (и, вообще говоря, любому объекту) может быть привязан опрос, которые описывается модеями:

* :class:`colloquium.models.Poll` -- модель опроса
* :class:`colloquium.models.Option` -- модель варианта ответа на вопрос
* :class:`colloquium.models.Answer` -- модель ответа пользователя на вопрос опроса

Аналогично опросам, (произвольные) объекты можно снабжать тегами, отбирать объекты по тегам и т.п. Теги отражены моделью :class:`topics.models.Tag`.
Теги могут быть объединены в темы (модель :class:`topics.models.Topic`).

Также, на сайте представлены новости, которые описываются моделью :class:`news.models.NewsItem`.

Рассылка почты происходит через почтовую подсистему -- модуль ``mail``.

Схема связей в объектной модели системы изображается следующим графом:

.. fancybox:: dentv_model_layer.png

Примерная схема базы данных порождаемая такой объектной моделью:

.. fancybox:: hierarchical.png

Модуль admin
------------

.. automodule:: admin.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: admin.models
    :parts: 1

.. automodule:: admin.resources
    :members:
    :show-inheritance:

.. inheritance-diagram:: admin.resources
    :parts: 1

.. automodule:: admin.rest_utils
    :members:
    :show-inheritance:

.. automodule:: admin.views
    :members:
    :show-inheritance:

.. automodule:: admin.forms
    :members:
    :show-inheritance:

.. inheritance-diagram:: admin.forms
    :parts: 1


Модуль comments
---------------

.. automodule:: comments.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: comments.models
    :parts: 1

.. automodule:: comments.forms
    :members:
    :show-inheritance:

.. inheritance-diagram:: comments.forms
    :parts: 1

.. automodule:: comments.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: comments.views
    :parts: 1


Модуль colloquium
-----------------

.. automodule:: colloquium.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: colloquium.models
    :parts: 1

.. automodule:: colloquium.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: colloquium.views
    :parts: 1


Модуль entries
--------------

.. automodule:: entries.models
    :members:
    :show-inheritance:


.. inheritance-diagram:: entries.models
    :parts: 1

.. automodule:: entries.managers
    :members:
    :show-inheritance:

.. automodule:: entries.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: entries.views
    :parts: 1

.. automodule:: entries.feeds
    :members:
    :show-inheritance:

.. automodule:: entries.signals
    :members:
    :show-inheritance:

.. automodule:: entries.management.commands.get_entries_view_count
    :members:

Модуль mail
-----------

.. automodule:: mail.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: entries.models
    :parts: 1

.. automodule:: mail.views
    :members:
    :show-inheritance:

.. automodule:: mail.management.commands.send_emails
    :members:

Модуль news
-----------

.. automodule:: news.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: news.models
    :parts: 1

.. automodule:: news.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: news.views
    :parts: 1

Модуль programs
---------------

.. automodule:: programs.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: programs.models
    :parts: 1

.. automodule:: programs.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: programs.views
    :parts: 1

Модуль siteuser
---------------

.. automodule:: siteuser.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: siteuser.models
    :parts: 1

.. automodule:: siteuser.fields
    :members:
    :show-inheritance:

.. inheritance-diagram:: siteuser.fields
    :parts: 1

.. automodule:: siteuser.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: siteuser.views
    :parts: 1

Модуль static_content
---------------------

.. automodule:: static_content.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: static_content.models
    :parts: 1

.. automodule:: static_content.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: static_content.views
    :parts: 1

Модуль tags
-----------

.. automodule:: tags.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: tags.views
    :parts: 1

Модуль topics
-------------

.. automodule:: topics.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: topics.models
    :parts: 1

.. automodule:: topics.views
    :members:
    :show-inheritance:

.. inheritance-diagram:: topics.views
    :parts: 1

Модуль utils
------------

.. automodule:: utils.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: utils.models
    :parts: 1

.. automodule:: utils.view_utils
    :members:
    :show-inheritance:

.. inheritance-diagram:: utils.view_utils
    :parts: 1

Модуль voting
-------------

.. automodule:: voting.models
    :members:
    :show-inheritance:

.. inheritance-diagram:: voting.models
    :parts: 1

.. automodule:: voting.views
    :members:
    :show-inheritance:

Автоматизация развертывания
---------------------------

.. automodule:: fabfile
    :members:

Индексы и таблицы
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
