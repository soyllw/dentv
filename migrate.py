import psycopg2

#DANGER: This file contains scenes of dirty hacking, inefficient database operating and crutches.
#Please consider not looking here at all. 
#You have been warned.

#connect
connection_from = psycopg2.connect("dbname=dentv_prod user=postgres")
connection_to = psycopg2.connect("dbname=dentvnew user=postgres")

#get cursor
cur_from = connection_from.cursor()
cur_to = connection_to.cursor()

#get table list
cur_from.execute("select tablename from pg_catalog.pg_tables where tableowner='dentv';")
tables = [x for (x,) in cur_from.fetchall()]

print(tables)
tables.remove("siteuser_user")
tables.remove("auth_user")
print(["auth_user"])

user_map = {}

for tablename in ["auth_user"]:
    print(tablename)
    selection_string = 'select * from {0};'.format(tablename,)
    purging_string = 'delete from {0};'.format(tablename,)
    cur_from.execute(selection_string)
    meta_to_select = cur_to.execute(selection_string)
    meta_to = [w[0] for w in cur_to.description]
    meta = [w[0] for w in cur_from.description]
    rows = [] 
    for row in cur_from.fetchall():
        item_from = dict(zip(meta, row))
        item_to = {}
        for k in meta_to:
            item_to[k] = item_from[k]
        rows.append(item_to)
    print "table {0}".format(tablename)
    if len(rows) > 0:
        cur_to.execute(purging_string)
        keys_seq = rows[0].keys()
        insertion_string = "insert into {0} ({1}) values ({2});".format(tablename, ', '.join(keys_seq), ', '.join(['%s' for s in keys_seq]))
        for val in rows:
            values = [val[k] for k in keys_seq]
            cur_to.execute(insertion_string, values)
        seq_reset_string = "select setval('{0}_id_seq', max(id)) from {0};".format(tablename)
        cur_to.execute(seq_reset_string)

for tablename in ["siteuser_user"]:
    tablename_to = "siteuser_user"
    selection_string = 'select * from {0};'.format(tablename,)
    purging_string = 'delete from {0};'.format(tablename_to,)
    cur_from.execute(selection_string)
    if tablename in ["siteuser_userprofile"]:
        meta = [w[0] for w in cur_from.description]
        rows = []
        for row in cur_from.fetchall():
            item = dict(zip(meta, row))
            item["user_ptr_id"] = item["user_id"]
            user_map[item["id"]] = item["user_id"]
            del item["user_id"]
            del item["id"]
            rows.append(item)
        keys_seq = rows[0].keys()
        cur_to.execute(purging_string)
        insertion_string = "insert into {0} ({1}) values ({2});".format(tablename_to, ', '.join(keys_seq), ', '.join(['%s' for s in keys_seq]))
        for row in rows:
            values = [row[k] for k in keys_seq]
            cur_to.execute(insertion_string, values)


for tablename in tables:
    print "table {0}".format(tablename)
    if "celery" in tablename or tablename == "content_dentvvideo":
        print("skipped", tablename)
        continue
    selection_string = 'select * from {0};'.format(tablename,)
    purging_string = 'delete from {0};'.format(tablename,)
    cur_from.execute(selection_string)
    meta_to_select = cur_to.execute(selection_string)
    meta_to = [w[0] for w in cur_to.description]
    meta = [w[0] for w in cur_from.description]
    rows = [] 
    for row in cur_from.fetchall():
        item_from = dict(zip(meta, row))
        item_to = {}
        for k in meta_to:
            if k in ["user_id"] and tablename not in ["social_auth_usersocialauth"]:
                print(item_from)
                item_to[k] = user_map[item_from[k]]
            elif k in ["author_id"] and tablename in ["comments_comment"]:
                item_to[k] = user_map[item_from["author_id"]]
            elif k in ["role_text", "comment"] and tablename in ["entries_entryuserrole"]:
                if not item_from[k] or item_from[k] == "":
                    item_to[k] = ""
                else:
                    item_to[k] = item_from[k]
            elif k in ["on_top"] and tablename in ["programs_program"]:
                if not (k in item_from.keys()) or item_from[k] == "":
                    item_to[k] = False
                else:
                    item_to[k] = item_from[k]
            elif tablename in ["comments_comment"]:
                if k in ["enabled"]:
                    if not (k in item_from.keys()) or item_from[k] == "":
                        item_to[k] = True
                    else:
                        item_to[k] = item_from[k]
                elif k in ["reason"]:
                    if not k in item_from.keys():
                        item_to[k] = ""
                    else:
                        item_to[k] = item_from[k]
                elif k in ["rating"]:
                    if not k in item_from.keys():
                        item_to[k] = 0
                    else:
                        item_to[k] = item_from[k]
                else:
                    item_to[k] = item_from[k]
            elif tablename in ["news_newsitem"]:
                if k in ["rating", "comments_count"]:
                    if not k in item_from.keys():
                        item_to[k] = 0
                    else:
                        item_to[k] = item_from[k]
                else:
                    item_to[k] = item_from[k]
            elif k in item_from.keys():
                item_to[k] = item_from[k]
            else:
                pass
        rows.append(item_to)
    if len(rows) > 0:
        cur_to.execute(purging_string)
        keys_seq = rows[0].keys()
        insertion_string = "insert into {0} ({1}) values ({2});".format(tablename, ', '.join(keys_seq), ', '.join(['%s' for s in keys_seq]))
        for val in rows:
            values = [val[k] for k in keys_seq]
            cur_to.execute(insertion_string, values)
        if 'id' in keys_seq:
            seq_reset_string = "select setval('{0}_id_seq', max(id)) from {0};".format(tablename)
            cur_to.execute(seq_reset_string)

connection_to.commit()

#close our stuff
cur_from.close()
cur_to.close()

connection_from.close()
connection_to.close()

#HERE BE DRAGONS
