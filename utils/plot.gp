set xdata time
set timefmt "%d-%m-%Y"
set format x "%d.%m.%y"
set format y "%10.0f"
set xrange ["01/09/12":"01/02/13"]
set yrange [0:1800000]
set xtics "09/12", 2592000

set xlabel "Дата"
set ylabel "Просмотры"
set nokey
set grid

#set terminal postscript eps enhanced color font "Helvetiva,10"
set size 1.0, 1.0
set terminal svg
set output "video_views.svg"

plot "video_views.dat" using 1:2 smooth unique