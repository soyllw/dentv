from reporters.youtube import YTPlotter
from reporters.facebook import FBPlotter
from reporters.vkontakte import VKPlotter

if __name__ == '__main__':
  YTPlotter().read_csv('yt_stats.csv').plot()
  FBPlotter().read_csv('fb_stats.csv').plot()
  VKPlotter().read_csv('vk_stats.csv').plot()