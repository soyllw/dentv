#-*- coding: utf-8 -*-
import csv
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.dates import MONDAY, MonthLocator, DateFormatter, WeekdayLocator, date2num
import matplotlib.font_manager as fm
from datetime import datetime, timedelta


class FBPlotter(object):
  fieldnames = [
    (1, 'share', u'Распространение контента'), 
    (4, 'impact', u'Упоминания'),
    (8, 'new_likes', u'Понравилось'),
    (9, 'new_dislikes', u'Разонравилось'),
    (33, 'saw_something', u'Увидели контент'),
    (39, 'social_share', u'Увидели контент у друзей'),
    (54, 'daily_visits', u'Кол-во просмотров страницы'),
    (70, 'daily_content_presentations', u'КОл-во просмотров контента')
  ]

  def read_csv(self, filename):
    with open(filename, 'r') as f:
      reader = csv.DictReader(f)
      self.data = []
      for row in reader:
        pack = {}
        date = datetime.strptime(row[reader.fieldnames[0]], '%Y-%m-%d').date()
        for pos, fname, _ in self.fieldnames:
          try:
            pack[fname] = int(row[reader.fieldnames[pos]])
          except ValueError:
            pack[fname] = 0
        self.data.append((date, pack))
    return self

  def compact_data(self):
    sixdays = timedelta(days=6)
    result = {x[1]: 0 for x in self.fieldnames}
    for x, y in self.data:
      for xx, yy in y.items():
        result[xx] += yy
      if x.weekday() == 6:
        yield ((x - sixdays), result)
        result = {x[1]: 0 for x in self.fieldnames}

  def color(self):
    colors = ['r', 'k', 'g', 'y', 'b', 'm', 'c']
    while True:
      for color in colors:
        yield color

  def plot(self):
    mpl.rcParams['text.usetex'] = False
    fp1 = fm.FontProperties(
      fname = '/usr/share/fonts/truetype/DejaVuSans.ttf'
    )

    self.data.sort(key=lambda w: w[0])
    self.data = list(self.compact_data())
    self.figure = plt.figure()
    mondays = WeekdayLocator(MONDAY)
    months = MonthLocator(range(1,13), bymonthday=1, interval=1)
    monthsFmt = DateFormatter('%m/%y')

    self.fbviews = self.figure.add_subplot(111)
    self.fbviews.xaxis_date()
    dates = date2num([x[0] for x in self.data])
    width = 10.0 / len(self.fieldnames)
    color = self.color()
    for n, p in enumerate(self.fieldnames):
      self.fbviews.bar(
        dates + n * width,
        [x[1][p[1]] for x in self.data],
        width = width,
        align = 'center',
        color = color.next(),
        label = p[2]
      )
    self.fbviews.xaxis.set_major_locator(months)
    self.fbviews.xaxis.set_minor_locator(mondays)
    self.fbviews.xaxis.set_major_formatter(monthsFmt)
    self.fbviews.autoscale(tight=True)
    self.fbviews.grid(True)

    self.figure.autofmt_xdate()
    plt.legend(prop=fp1)
    plt.savefig('social_fb.png')