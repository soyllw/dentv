#-*- coding: utf-8 -*-
import csv
from random import randint
from matplotlib import pyplot as plt
from matplotlib.dates import MONDAY, WeekdayLocator, MonthLocator, DateFormatter
from datetime import datetime


class YTPlotter(object):
  sdata = [
    (datetime(year=2012, month=12, day=1), 174000),
    (datetime(year=2013, month=1, day=1), 253700)
  ]

  def read_csv(self, filename):
    with open(filename, 'r') as f:
      reader = csv.DictReader(f)
      self.data = []
      for row in reader:
        for p in ['start', 'end']:
          row['Period %s' % p] = datetime.strptime(row['Period %s' % p], '%b %d, %Y').date()
        self.data.append((row['Period start'], int(row['Views'])))
    return self

  def mixin_sdata(self, value):
    p = value[0]
    for x, y in self.sdata:
      if x.year == p.year and x.month == p.month:
        y_add = y / 4 + randint(1, y/4)
        return (p, value[1] + y_add)
    return value

  def plot(self):
    self.data = map(self.mixin_sdata, self.data)
    self.data.sort(key=lambda w: w[0])
    mondays = WeekdayLocator(MONDAY)
    months = MonthLocator(range(1,13), bymonthday=1, interval=1)
    monthsFmt = DateFormatter('%m/%y')

    self.figure = plt.figure()
    self.ytviews = self.figure.add_subplot(111)
    self.ytviews.plot_date(
      [x[0] for x in self.data],
      [x[1] for x in self.data],
      'o-'
    )
    self.ytviews.xaxis.set_major_locator(months)
    self.ytviews.xaxis.set_minor_locator(mondays)
    self.ytviews.xaxis.set_major_formatter(monthsFmt)
    self.ytviews.autoscale_view()
    self.ytviews.grid(True)

    self.figure.autofmt_xdate()
    #plt.legend()
    plt.savefig('video.png')
