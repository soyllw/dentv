#-*- coding: utf-8 -*-
import csv
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.dates import MONDAY, MonthLocator, DateFormatter, WeekdayLocator, date2num
import matplotlib.font_manager as fm
from datetime import datetime, timedelta


class VKPlotter(object):
  fieldnames = [
    ('reach', u'Охват'),
    ('visitors', u'Посетители'),
    ('views', u'Просмотры')
  ]

  def read_csv(self, filename):
    with open(filename, 'r') as f:
      reader = csv.reader(f)
      pack = {}
      reader.next() # skip header
      for row in reader:
        date = datetime.strptime(row[0], '%d.%m.%Y').date()
        if date not in pack:
          pack[date] = {}
        pack[date][row[1]] = int(row[4])
    self.data = pack.items()
    return self

  def compact_data(self):
    sixdays = timedelta(days=6)
    result = {}
    for x, y in self.data:
      for xx, yy in y.items():
        if xx not in result:
          result[xx] = 0
        result[xx] += yy
      if x.weekday() == 6:
        yield ((x - sixdays), result)
        result = {}

  def color(self):
    colors = ['r', 'k', 'g', 'y', 'b', 'm', 'c']
    while True:
      for color in colors:
        yield color

  def plot(self):
    mpl.rcParams['text.usetex'] = False
    fp1 = fm.FontProperties(
      fname = '/usr/share/fonts/truetype/DejaVuSans.ttf'
    )
    self.data.sort(key=lambda w: w[0])
    self.data = list(self.compact_data())
    
    self.figure = plt.figure()
    mondays = WeekdayLocator(MONDAY)
    months = MonthLocator(range(1,13), bymonthday=1, interval=1)
    monthsFmt = DateFormatter('%m/%y')

    self.vkviews = self.figure.add_subplot(111)
    self.vkviews.xaxis_date()
    dates = date2num([x[0] for x in self.data])
    width = 5.0 / len(self.fieldnames)
    color = self.color()
    for n, p in enumerate(self.fieldnames):
      self.vkviews.bar(
        dates + n * width,
        [x[1][p[0]] for x in self.data],
        width = width,
        align = 'center',
        color = color.next(),
        label = p[1]
      )
    self.vkviews.xaxis.set_major_locator(months)
    self.vkviews.xaxis.set_minor_locator(mondays)
    self.vkviews.xaxis.set_major_formatter(monthsFmt)
    self.vkviews.autoscale(tight=True)
    self.vkviews.grid(True)

    self.figure.autofmt_xdate()
    plt.legend(prop=fp1)
    plt.savefig('social_vk.png')