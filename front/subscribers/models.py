# -*- coding: utf-8 -*-
from django.db import models

class Subscriber(models.Model):
    email = models.EmailField(verbose_name=u'Email', blank=False, null=False)

    class Meta:
        verbose_name = u'Подписчики'
        verbose_name_plural = u'Подписчики'

    def __unicode__(self):
        if self.email:
            return self.email
        else:
            return u'Email'