# coding: utf-8
from django.db.models.signals import post_save
from django.db.models.loading import get_models

from subscribers.models import Subscriber
from mail.models import EmailTemplate
import datetime


def send_init_email(sender, instance, *args, **kwargs):
    email = EmailTemplate.objects.get(key='subscribe_init')
    email.send([instance.email], date=datetime.datetime.now())


post_save.connect(send_init_email, sender=Subscriber, dispatch_uid='subscribers.signals.post_save_send_init_email')