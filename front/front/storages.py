#-*- coding: utf-8 -*-
from urllib import quote
from urlparse import urljoin

from django.core.files.storage import FileSystemStorage
from django.utils.encoding import smart_str

class AuschwitzStorage(FileSystemStorage):
    def url(self, name):
        if self.base_url is None:
            raise ValueError("This file is not accessible via a URL.")
        name = '%s' % quote(smart_str(name).replace("\\", "/"), safe=b"/~!*()'")
        return urljoin(self.base_url, name)