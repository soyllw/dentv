import pytils.translit

SOCIAL_AUTH_EXTRA_DATA = True

SOCIAL_AUTH_ASSOCIATE_BY_MAIL = False #TODO: workaround for user association since Twitter and VK do not provide emails

SOCIAL_AUTH_COMPLETE_URL_NAME  = 'socialauth_complete'
SOCIAL_AUTH_ASSOCIATE_URL_NAME = 'socialauth_associate_complete'

SOCIAL_AUTH_FORCE_RANDOM_USERNAME = True
SOCIAL_AUTH_USERNAME_FIXER = lambda u: pytils.translit.slugify(u)

SOCIAL_AUTH_USER_MODEL = 'siteuser.User'

LOGIN_URL          = '/accounts/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL    = '/'

# SOCIAL_AUTH_PIPELINE = (
#     'social_auth.backends.pipeline.social.social_auth_user',
#     'social_auth.backends.pipeline.associate.associate_by_email',
#     'social_auth.backends.pipeline.misc.save_status_to_session',
#     'social_auth.backends.pipeline.user.create_user',
#     'social_auth.backends.pipeline.social.associate_user',
#     'social_auth.backends.pipeline.social.load_extra_data',
#     'social_auth.backends.pipeline.user.update_user_details',
# )

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.associate.associate_by_email',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    'siteuser.pipeline.create_or_update_profile',
)

OPENID_SREG_EXTRA_DATA = [
	('email', 'email'), 
	('fullname', 'fullname'), 
	('nickname', 'nickname'), 
	('dob', 'dob'), 
	('gender', 'gender'), 
	('country', 'country')]

OPENID_AX_EXTRA_DATA = [
	('http://openid.net/schema/contact/internet/email', 'email'),
	('http://openid.net/schema/contact/web/blog', 'blog'),
	('http://openid.net/schema/gender', 'gender'),
	#TODO: add more from http://stackoverflow.com/questions/7403536/list-of-available-attributes-for-http-axschema-org-and-http-schemas-openid-n
	]

GOOGLE_SREG_EXTRA_DATA = OPENID_SREG_EXTRA_DATA
GOOGLE_AX_EXTRA_DATA = OPENID_AX_EXTRA_DATA

TWITTER_EXTRA_DATA = [('email', 'email')]
FACEBOOK_EXTRA_DATA = [('email', 'email')]