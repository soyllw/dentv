from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.db.models import permalink
from django.forms.widgets import Widget

from django.contrib import admin
from django.contrib.admin import widgets


from front.views import HomeView, SearchView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'social/', include('social_auth.urls')),
    url(r'accounts/', include('siteuser.urls')),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^404.html$', 'front.views.view404', name='view-404'),
    url(r'^programs/', include('programs.urls')),
    url(r'^content/', include('entries.urls')),
    url(r'^voting/', include('voting.urls')),
    url(r'^tags/', include('tags.urls')),
    url(r'^topics/', include('topics.urls')),
    url(r'^colloquium/', include('colloquium.urls')),
    url(r'^pages/', include('static_content.urls')),
    url(r'^qa_list/', include('qa_app.urls')),
    url(r'^search/$', SearchView.as_view()),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^saga/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT})
    )

handler404 = 'front.views.view404'
