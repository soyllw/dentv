import random
import itertools
from datetime import datetime, timedelta

from django.db.models import Count, Max
from django.views.generic import ListView

from djorm_expressions.base import SqlExpression

from utils.view_utils import TemplateViewMixin, render_to, render_to_response
from utils.cache import cached

from entries.models import Entry, EntryPromotion, YTLiveEvent, AirSchedule, EntryAnnounce
from colloquium.models import Poll
from programs.models import Program
from siteuser.models import User
from topics.models import Topic
from utils.models import get_content_type_by_class


class HomeView(TemplateViewMixin, ListView):
    paginate_by = 17
    template_name = 'home.html'
    context_object_name = 'entries'
    main = None

    def get_queryset(self):
        # TODO: cache this
        self.main = Entry.archive.defer('content').all()[0:4]
        forget = self.main[:1]
        return Entry.archive.select_related().exclude(pk__in=forget).defer('content')

    def get_latest_announces(self):
        return Entry.announce.defer('content').\
               prefetch_related('user_roles', 'user_roles__user').all()[0:4]

    def get_context_data(self, **kwargs):
        now = datetime.now()
        context = super(HomeView, self).get_context_data(**kwargs)
        context['main'] = self.main
        try:
            if 'debug' in self.request.GET:
                context['live'] = YTLiveEvent.objects.prefetch_related('debatees').latest('id')
            else:
                context['live'] = YTLiveEvent.objects.filter(start__lte=now, stop__gte=now).latest('id')
        except YTLiveEvent.DoesNotExist:
            context['live'] = None
        context['grid'] = AirSchedule.actual()
        context['poll'] = Poll.get_latest_active()
        # context['promo'] = Entry.announce.all()
        context['promo'] = EntryAnnounce.objects.filter(start_at__lte=now, stop_at__gte=now)
        now = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        context['top_entries'] = cached(Entry.get_top_blog_entries, 'top_entries', 60 * 10)
        context['authors'] = User.objects.filter(enabled=True).exclude(person_email=u'').order_by('position').all()
        print(context['authors'])
        return context


def view404(request):
    response = render_to_response('404.html', request, {})
    response.status_code = 404
    return response


@render_to('admin.html')
def admin(request):
    return {}


class SearchView(TemplateViewMixin, ListView):
    #FIXME: temp code for show users video

    paginate_by = 9
    template_name = 'search.html'
    context_object_name = 'entries'
    is_live = False
    is_user = False

    def get_queryset(self):
        self.query = self.request.GET.get('query', '').strip()
        n = datetime.now()
        #print(self.query)
        if self.request.GET.get('live', False):
            self.is_live = True
            self.query = u'*{0}'.format(self.query)

        if self.request.GET.get('user', '').strip() == '1':
            print(self.request.GET.get('user', '').strip())
            self.is_user = True
            print('we have user in query')

            return Entry.objects.filter(released_at__lte=n, enabled=True, user_roles__user=self.query)\
                    .select_related()\
                    .order_by('-released_at')

        return Entry.searcher.filter(released_at__lte=n, enabled=True)\
                    .search(query=self.query, rank_field='rank').select_related()\
                    .order_by('-released_at', '-rank')

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        #FIXME
        if self.query:
            context['query'] = self.query
        if self.is_user:
            context['query'] = User.objects.get(pk=self.query).display_name
        context['users'] = User.objects.filter(display_name__icontains = self.query)
        context['is_user'] = self.is_user
        print(context['paginator'].__dict__, 'context')
        return context
