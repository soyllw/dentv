import urllib


class PJAXLoginStatusMiddleware(object):
    def process_response(self, request, response):
        if not request.path.startswith('/edda/'):
            if hasattr(request, 'user') and request.user.is_authenticated():
                try:
                    response['X_AUTH_DATA'] = urllib.quote(request.user.screen_name.encode('utf-8'))
                except AttributeError:
                    pass
        return response