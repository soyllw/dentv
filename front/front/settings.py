#-*- coding: utf-8 -*-
import os, sys
from warnings import warn

DEBUG = True
TEMPLATE_DEBUG = DEBUG
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
MAKO_MODULE_DIRECTORY = os.path.abspath(os.path.join(ROOT_PATH, '_mako'))

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

FIXTURE_DIRS = (
    os.path.abspath(os.path.join(ROOT_PATH, '../fixtures')),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'dentv_prod',                      # Or path to database file if using sqlite3.
        'USER': 'postgres',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-RU'
SITE_ID = 1
USE_I18N = False
USE_L10N = False
USE_TZ = False

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.abspath(os.path.join(ROOT_PATH, '../../media'))

STATIC_ROOT = os.path.abspath(os.path.join(ROOT_PATH, '../../static'))

STATIC_URL = '/static/'


STATICFILES_DIRS = (
    os.path.abspath(os.path.join(ROOT_PATH, "../static")),
    os.path.abspath(os.path.join(ROOT_PATH, "../../vili/public"))
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'ekejzz*luqh1n-fbuglacc2wo956d^qkp*+t+5_6p_i-j07ayu'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'front.middleware.PJAXLoginStatusMiddleware',
    #'raven.contrib.django.middleware.SentryResponseErrorIdMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
)

CUSTOM_USER_MODEL = 'siteuser.User'

ROOT_URLCONF = 'front.urls'

WSGI_APPLICATION = 'front.wsgi.application'

DATETIME_INPUT_FORMATS = (
    '%Y-%m-%dT%H:%M:%S',
    '%Y-%m-%d %H:%M:%S', '%Y-%m-%d %H:%M', '%Y-%m-%d',
    '%m/%d/%Y %H:%M:%S', '%m/%d/%Y %H:%M', '%m/%d/%Y',
    '%m/%d/%y %H:%M:%S', '%m/%d/%y %H:%M', '%m/%d/%y',
)

TEMPLATE_DIRS = (
    os.path.join(ROOT_PATH, 'templates'),
)

GRAPPELLI_INDEX_DASHBOARD = 'saga.dashboard.CustomIndexDashboard'
GRAPPELLI_ADMIN_TITLE = u'Система управления проектом &laquo;День-ТВ&raquo;'

INSTALLED_APPS = (
    'grappelli.dashboard',
    'grappelli',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'static_content',
    'tags',
    'entries',
    'programs',
    'siteuser',
    'social_auth',
    'imagekit',
    'voting',
    'colloquium',
    'topics',
    'mail',
    'utils',
    'south',
    'content',
    'tinymce',
    'features',
    'external',
    'qa_app',
    'advertising',
    'books',
    'topbanners',
    'subscribers',
)

FEATURES_CONF = {
    'sources': {
        'entries': ('Entry',)
    }
}

TEST_EXCLUDE = (
    'django',
    'djangosphinx',
    'taggit',
    'imagekit',
    'social_auth'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    #'django.core.context_processors.static',
    #'django.core.context_processors.media',
    'django.contrib.auth.context_processors.auth',
    #'django.core.context_processors.debug',
    #'django.core.context_processors.i18n',
    #'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'siteuser.context_processors.social_shit',
    'front.context_processors.common',
    #'front.context_processors.applist'
)

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    #'social_auth.backends.google.GoogleOAuthBackend',
    #'social_auth.backends.google.GoogleOAuth2Backend',
    #'social_auth.backends.google.GoogleBackend',
    #'social_auth.backends.yahoo.YahooBackend',
    #'social_auth.backends.browserid.BrowserIDBackend',
    #'social_auth.backends.contrib.linkedin.LinkedinBackend',
    #'social_auth.backends.contrib.livejournal.LiveJournalBackend',
    #'social_auth.backends.contrib.orkut.OrkutBackend',
    #'social_auth.backends.contrib.foursquare.FoursquareBackend',
    #'social_auth.backends.contrib.github.GithubBackend',
    #'social_auth.backends.contrib.dropbox.DropboxBackend',
    #'social_auth.backends.contrib.flickr.FlickrBackend',
    #'social_auth.backends.contrib.instagram.InstagramBackend',
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'social_auth.backends.OpenIDBackend',
    #'social_auth.backends.contrib.bitbucket.BitbucketBackend',
    'siteuser.auth_backends.CustomUserModelBackend',
    'django.contrib.auth.backends.ModelBackend',
)

if DEBUG:
    DEFAULT_FILE_STORAGE = 'front.storages.AuschwitzStorage'
    #INSTALLED_APPS += ('debug_toolbar',)
    #INTERNAL_IPS = ('127.0.0.1',)
    #DEBUG_TOOLBAR_PANELS = ('debug_toolbar.panels.sql.SQLDebugPanel',)

TEST_RUNNER = 'front.test_runner.AdvancedTestSuiteRunner'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'simple': {
            'format': u'%(levelname)s %(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False
        }
    }
}

EMAIL_REGISTRATION_KEY = 'registration_complete'
EMAIL_RECOVERY_KEY = 'recovery'
EMAIL_PASSWORD_KEY = 'password'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = '25'
DEFAULT_FROM_EMAIL = 'auto@dentv.ru'


try:
    from front.auth_settings import *
except ImportError:
    warn("Unable to import authentication settings for social_auth")

try:
    from front.local_settings import *
except ImportError:
    warn("Unable to import local settings for application")

try:
    from tasks.settings import *
except ImportError:
    warn("Unable to import celery tasks from tasks app")

if 'test' in sys.argv:
    DATABASES['default']['ENGINE'] = 'django.db.backends.sqlite3'


VIDEO_ROOT = '' # legacy
FACEBOOK_APP_ID = '' 
VKONTAKTE_APP_ID = ''
