#-*- coding: utf-8 -*-
import git
from datetime import datetime
from django.db.models import Sum, Count
from django.contrib import admin
from django.conf import settings
from django.utils.safestring import mark_safe

from utils.cache import cached

from programs.models import Program
from siteuser.models import User
from advertising.models import AdBanner
from books.models import Book
from topbanners.models import TopBanner


#commit_hash = git.Repo().commits()[0].id
commit_hash = 'klj4h35kjl34h5kj34'

def get_top_programs():
    return list(Program.objects.filter(on_top=True)\
           .annotate(entries_num=Count('entries'), views=Sum('entries__views_count'))\
           .exclude(entries_num=0, logo=''))


def get_top_bloggers():
    return list(User.objects.filter(has_blog=True).exclude(pk=1).order_by('display_name'))

def get_banners():
    #FIXME: this is dirty
    '''try:
        left_banner =  AdBanner.objects.filter(active=True, position=1).all()
        left_banner = left_banner
    except:
        left_banner = None
    try:
        right_banner = AdBanner.objects.filter(active=True, position=2).all()
        right_banner = right_banner
    except:
        right_banner = None'''

    left_banners = AdBanner.objects.filter(active=True, position=1).all()
    right_banners = AdBanner.objects.filter(active=True, position=2).all()
    print(left_banners, "LEFT_BANNERS")    

    return {'left_banners' : left_banners, 'right_banners' : right_banners }

def get_sliders():
    sliders = TopBanner.objects.filter(active=True).order_by('position').all()
    return sliders


def get_books():
    return Book.objects.filter(active=True).all()

def common(request):
    return {
        'common': {
            'top_programs': cached(get_top_programs, 'top_programs', 60 * 10),
            'top_bloggers': cached(get_top_bloggers, 'top_bloggers', 60 * 10),
            'banners' : get_banners(),
            'sliders' : get_sliders(),
            'books' : get_books(),
            'commit_hash': commit_hash,
            'now': datetime.now()
        }
    }
