#from django.db.models import CharField
#from django.db.models.signals import class_prepared
import os, sys
sys.path.insert(0, '/home/dentv/dentv_site/dentv/front/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "front.settings")
print(sys.path)
from django.db.models import CharField
from django.db.models.fields import EmailField
from django.db.models.signals import class_prepared



def add_field(sender, **kwargs):
    """
    class_prepared signal handler that checks for the model named
    MyModel as the sender, and adds a CharField
    to it.
    """
    if sender.__name__ == "siteuser_user":
        field = EmailField("person_email", max_length=75, null=True, blank=True)
        field.contribute_to_class(sender, "person_email")

class_prepared.connect(add_field)

