#coding: utf-8
"""
Представления
~~~~~~~~~~~~~"""
from django.views.generic import DetailView

from utils.view_utils import TemplateViewMixin

from static_content.models import Entry

class EntryView(TemplateViewMixin, DetailView):
    "Представление, отображающее статическую страницу, сохраненную в базе"
    template_name = 'static/item.html'
    context_object_name = 'item'
    queryset = Entry.objects.all()