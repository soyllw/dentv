from django.contrib import admin

from static_content.models import Entry

class EntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug')
    
admin.site.register(Entry, EntryAdmin)