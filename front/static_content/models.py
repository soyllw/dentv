#coding: utf-8
"""
Модели
~~~~~~"""
from django.db import models

from utils.models import TitledSlugEntry


class Entry(TitledSlugEntry):
    "Модель статической страницы"
    text = models.TextField(verbose_name=u'Текст статической страницы')
