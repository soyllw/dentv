from django.conf.urls import patterns, url

from views import EntryView

urlpatterns = patterns('',
    url(r'^item/(?P<slug>[-_A-Za-z0-9]+)/$', EntryView.as_view(), name='static.view.item'),
)
