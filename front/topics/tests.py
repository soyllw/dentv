from datetime import datetime

from django.test import TestCase
from django.core.urlresolvers import reverse

from taggit.models import Tag

from siteuser.models import User
from comments.models import Comment
from programs.models import Program
from entries.models import Entry
from topics.models import Topic


def sieg_heil():
    test_user = User.objects.create(
        first_name = 'Adolf',
        last_name  = 'Hitler',
        username   = 'adolf',
        dob        = datetime(year=1889,month=4, day=20),
        gender     = 1,
        location   = 'Berlin, Deutschland',
        occupation = 'You know... Europe ;)',
        about      = 'Famous writer and painter')
    test_user.set_password('reich')
    test_user.is_staff = True
    test_user.save()
    return test_user


class CommentsTestCase(TestCase):

    def setUp(self):
        self.adolf     = sieg_heil()
        self.get_adolf = lambda: User.objects.get(pk=self.adolf.pk)
        self.program1  = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        self.program2  = Program.objects.create(title='test program2', owner=self.adolf, created_at=datetime.now())
        self.entry1    = Entry.objects.create(released_at=datetime.now(), program=self.program1, title='test entry', thumb_source="kekeke")
        self.entry2    = Entry.objects.create(released_at=datetime.now(), program=self.program1, title='test entry', thumb_source="kekeke")
        self.entry3    = Entry.objects.create(released_at=datetime.now(), program=self.program2, title='test entry', thumb_source="kekeke")
        self.tag1      = Tag.objects.create(name='tag1', slug='tag1')
        self.tag2      = Tag.objects.create(name='tag2', slug='tag2')
        self.tag3      = Tag.objects.create(name='tag3', slug='tag3')
        self.entry1.tags.add('tag1')
        self.entry2.tags.add('tag1')
        self.entry2.tags.add('tag2')
        self.entry2.tags.add('tag3')
        self.entry3.tags.add('tag2')

    def tearDown(self):
        for m in [User, Program, Entry, Comment, Tag]:
            m.objects.all().delete()

    def test_topic_creating(self):
        topic1 = Topic.objects.create(title="Topic1",slug="slug1")
        topic1.tags.add(self.tag1)
        topic1.save()
        self.assertEqual(Entry.objects.filter(tags__in=topic1.tags.all()).count(), 2)

    def test_topic_view_single(self):
        self.assertEqual(self.client.get(reverse('topic.view.item', kwargs={'slug': 'slug1'})).status_code, 404)
        topic1 = Topic.objects.create(title="Topic1",slug="slug1")
        self.assertEqual(self.client.get(reverse('topic.view.item', kwargs={'slug': 'slug1'})).status_code, 200)

    def test_topic_view_list(self):
        self.assertEqual(self.client.get(reverse('topic.view.list')).status_code, 200)        