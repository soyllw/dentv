#coding: utf-8
"""
Модели
~~~~~~"""
import pytils.translit

from django.db import models

from djorm_pgarray.fields import ArrayField
from djorm_expressions.models import ExpressionManager
from djorm_expressions.base import SqlExpression

from utils.models import TitledSlugEntry
from entries.models import Entry


class Topic(TitledSlugEntry):
    "Тема - набор материалов, объединенных одним или несколькими тегами."
    is_actual = models.BooleanField(default=False, verbose_name=u"Актуальная")
    tags = ArrayField(dbtype="text", verbose_name=u"Теги", default=lambda: [])
    latest_entry = models.ForeignKey(Entry, verbose_name=u"Последняя запись по теме", null=True, blank=True, on_delete=models.SET_NULL)
    position = models.IntegerField(verbose_name=u"Положение в списке тем", default=0)

    objects = ExpressionManager()

    class Meta:
        verbose_name = u"Тема"
        verbose_name_plural = u"Темы"
        ordering = ['position', 'title']

    def __unicode__(self):
        return self.title

    def update_latest_entry(self, exclude=None, *args, **kwargs):
        try:
            self.latest_entry = Entry.archive.where(SqlExpression("tags", "&&", self.tags)).latest('released_at')
        except Entry.DoesNotExist:
            self.latest_entry = None
        self.save()

    def save(self, *args, **kwargs):
        if self.latest_entry is None:
            try:
                self.latest_entry = Entry.archive.where(SqlExpression("tags", "&&", self.tags)).latest('released_at')
            except Entry.DoesNotExist:
                self.latest_entry = None
        if self.tags is None:
            self.tags = []
        super(Topic, self).save(*args, **kwargs)

    def tags_string(self):
        return u', '.join(self.tags)
    tags_string.short_description = u'Теги составляющие тему'

    @models.permalink
    def get_absolute_url(self):
        "Ссылка на страницу просмотра темы"
        return ('topic.view.item', (), {'slug': self.slug})

import topics.signals