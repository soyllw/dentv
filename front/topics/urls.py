from django.conf.urls import patterns, url

from topics.views import TopicView, TopicListView

urlpatterns = patterns('',
    url(r'^view/(?P<slug>[-_A-Za-z0-9]+)/$', TopicView.as_view(), name='topic.view.item'),
    url(r'^list/$', TopicListView.as_view(), name='topic.view.list'),
)
