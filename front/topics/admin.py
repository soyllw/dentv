from django.contrib import admin
from django.forms import ModelForm

from topics.models import Topic
from tags.fields import ArrayEditField, ArrayEditWidget

class TopicAdminForm(ModelForm):
    tags = ArrayEditField(widget=ArrayEditWidget)

    class Meta:
        model = Topic


class TopicAdmin(admin.ModelAdmin):
    list_display = ('title', 'tags_string', 'is_actual')
    list_filter = ('is_actual',)
    search_fields = ('title',)
    exclude = ('latest_entry',)
    form = TopicAdminForm


admin.site.register(Topic, TopicAdmin)