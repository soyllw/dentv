#coding: utf-8
"""
Представления
~~~~~~~~~~~~~"""
from django.shortcuts import get_object_or_404
from django.views.generic import ListView

from djorm_expressions.base import SqlExpression

from utils.view_utils import TemplateViewMixin
from utils.models import get_content_type_by_class

from entries.models import Entry
from topics.models import Topic


class TopicView(TemplateViewMixin, ListView):
    "Просмотр записей по данной теме"
    template_name = 'topics/item.html'
    paginate_by = 12
    context_object_name = 'items'

    def get_queryset(self):
        "Получение записей, объединенных данной темой"
        self.topic = get_object_or_404(Topic, slug=self.kwargs['slug'])
        tags  = self.topic.tags
        tagged_articles = Entry.archive.distinct().defer('content').where(SqlExpression("tags", "&&", tags))
        return tagged_articles

    def get_context_data(self, **kwargs):
        "Установка дополнительных контекстных переменных"
        context = super(TopicView, self).get_context_data(**kwargs)
        context['topic'] = self.topic
        return context


class TopicListView(TemplateViewMixin, ListView):
    "Общий список тем"
    template_name = 'topics/list.html'
    paginate_by = 6
    context_object_name = 'items'

    def get_queryset(self):
        "Получение списка тем"
        sql = Topic.objects.exclude(latest_entry__isnull=True).\
              distinct('latest_entry__id').\
              prefetch_related('latest_entry').order_by('-latest_entry__id')
        return sql
    
    def get_context_data(self, **kwargs):
        "Установка дополнительных контекстных переменных"
        context = super(TopicListView, self).get_context_data(**kwargs)
        return context