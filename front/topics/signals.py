#coding: utf-8
from django.db.models.signals import post_save, post_delete
from django.core.cache import cache
from django.dispatch import receiver

from djorm_expressions.base import SqlExpression

from entries.models import Entry
from topics.models import Topic

def drop_actual_topics_cache(*args, **kwargs):
    """
    Обработчик сигнала сброса кеша анонсов.
    """
    cache.delete('actual_topics')

post_save.connect(drop_actual_topics_cache, sender=Entry, dispatch_uid='topics.signals.entry_post_save_drop_cache')
post_delete.connect(drop_actual_topics_cache, sender=Entry, dispatch_uid='topics.signals.entry_post_delete_drop_cache')
post_save.connect(drop_actual_topics_cache, sender=Topic, dispatch_uid='topics.signals.topic_post_save_drop_cache')
post_delete.connect(drop_actual_topics_cache, sender=Topic, dispatch_uid='topics.signals.topic_post_delete_drop_cache')

@receiver(post_save, sender=Entry)
def update_topics_on_entry_save(sender, instance, **kwargs):
    topics_latest = Topic.objects.filter(latest_entry=sender)
    for topic in topics_latest:
        topic.update_latest_entry()
    topics_in_question = Topic.objects.where(SqlExpression('tags', '&&', instance.tags))
    for topic in topics_in_question:
        if not topic in topics_latest:
            topic.update_latest_entry()

@receiver(post_delete, sender=Entry)
def update_topics_on_entry_delete(sender, **kwargs):
    topics_with_none = Topic.objects.filter(latest_entry=None)
    for topic in topics_with_none:
        topic.update_latest_entry()
