# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Topic.tags'
        db.add_column(u'topics_topic', 'tags',
                      self.gf('djorm_pgarray.fields.ArrayField')(default=None, dbtype='varchar(255)', null=True, blank=True),
                      keep_default=False)


        # Changing field 'Topic.slug'
        db.alter_column(u'topics_topic', 'slug', self.gf('autoslug.fields.AutoSlugField')(unique=True, max_length=1024, populate_from='title', unique_with=()))

    def backwards(self, orm):
        # Deleting field 'Topic.tags'
        db.delete_column(u'topics_topic', 'tags')


        # Changing field 'Topic.slug'
        db.alter_column(u'topics_topic', 'slug', self.gf('autoslug.fields.AutoSlugField')(max_length=1024, unique_with=(), unique=True, populate_from=None))

    models = {
        u'topics.topic': {
            'Meta': {'object_name': 'Topic'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_actual': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '1024', 'populate_from': "'title'", 'unique_with': '()'}),
            'tags': ('djorm_pgarray.fields.ArrayField', [], {'default': 'None', 'dbtype': "'varchar(255)'", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'})
        }
    }

    complete_apps = ['topics']