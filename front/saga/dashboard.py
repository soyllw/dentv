# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
            
        self.children.append(modules.AppList(
            u'Модули',
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            exclude=('django.contrib.*',),
        ))

        self.children.append(modules.Group(
            u'Системные модули',
            column=1,
            collapsible=True,
            children = [
                modules.AppList(
                    u'Управление',
                    column=1,
                    collapsible=False,
                    models=('django.contrib.*',),
                ),
            ]
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            u'Недавние действия',
            limit=15,
            collapsible=False,
            column=2,
        ))


