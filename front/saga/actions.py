#coding: utf-8

def disable_comments(modeladmin, request, queryset):
    queryset.update(comments_allowed=False)
disable_comments.short_description = u"Отключить комментарии"

def enable_comments(modeladmin, request, queryset):
    queryset.update(comments_allowed=True)
enable_comments.short_description = u"Включить комментарии"