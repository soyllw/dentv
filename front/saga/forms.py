#coding: utf-8

from django import forms


class CacheDeleteForm(forms.Form):
    key = forms.CharField(label=u"Ключ кеша для удаления", required=True, max_length=255)