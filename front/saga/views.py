# Create your views here.

from django.views.generic import FormView
from django.core.urlresolvers import reverse
from django.core.cache import cache

from saga.forms import CacheDeleteForm


class CacheDelete(FormView):
    form_class = CacheDeleteForm
    template_name = 'admin_off/cache_control.html'

    def form_valid(self, form):
        data = form.cleaned_data
        cache_key = data.get('key')
        cache.delete(cache_key)
        form_class = self.get_form_class()
        return_form = form_class()
        return self.render_to_response(self.get_context_data(form=return_form, success=True))