from django.contrib.auth.management import create_permissions
from django.db.models import signals
from django.contrib.auth import models as auth_app

signals.post_syncdb.disconnect(
    create_permissions,
    sender=auth_app,
    dispatch_uid = "django.contrib.auth.management.create_permissions")