#coding: utf-8
"""
Представления
~~~~~~~~~~~~~"""
from django.shortcuts import get_object_or_404
from django.views.generic import ListView

from djorm_expressions.base import SqlExpression

from utils.view_utils import TemplateViewMixin

from entries.models import Entry

class TaggedItemsView(TemplateViewMixin, ListView):
    """Отображение записей под данным тегом. Служит для отображения выпусков программ."""
    template_name = 'tags/item.html'
    paginate_by = 6
    context_object_name = 'items'
    tag = ''

    def get_queryset(self):
        self.tag = self.kwargs['name']
        tagged_articles = Entry.objects.where(SqlExpression("tags", "@>", [self.tag]))
        return Entry.archive.filter(pk__in=tagged_articles).defer('content')

    def get_context_data(self, **kwargs):
        """Добавляет дополнительные данные в контекст, необходимые для построения страницы по шаблону"""
        context = super(TaggedItemsView, self).get_context_data(**kwargs)
        context['tag'] = self.tag
        print(context)
        return context

item = TaggedItemsView.as_view()
