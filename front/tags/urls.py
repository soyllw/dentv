#coding: utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    #url(ur'^view/(?P<name>[-_A-Za-z0-9А-Яа-я Ёё?!\"\'\(\)]+)/$', 'tags.views.item', name='tags.view.item'),
    url(ur'^view/(?P<name>[^/]+)/$', 'tags.views.item', name='tags.view.item'),
)
