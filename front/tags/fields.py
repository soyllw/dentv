#coding: utf-8
import re

from django.forms import fields
from django.forms import TextInput
from django.contrib.admin.widgets import AdminTextareaWidget

from djorm_pgarray.fields import ArrayField

PATTERN = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')


class ArrayEditField(fields.Field):
    def __init__(self, *args, **kwargs):
        super(ArrayEditField, self).__init__(*args, **kwargs)

    def unquote_token(self, token):
        if token[0] in "'\"":
            return token.strip(token[0])
        else:
            return token

    def clean(self, value):
        values = PATTERN.split(value)[1::2]
        return [self.unquote_token(token) for token in values]


class ArrayEditWidget(AdminTextareaWidget):
    def __init__(self, *args, **kwargs):
        super(ArrayEditWidget, self).__init__(*args, **kwargs)

    def quote_token(self, token):
        if "'" in token:
            return u'"{0}"'.format(unicode(token))
        elif '"' in token:
            return u"'{0}'".format(unicode(token))
        elif ' ' in token:
            return u'"{0}"'.format(token)
        else:
            return unicode(token)

    def render(self, name, value, *args, **kwargs):
        #print "value", value
        if isinstance(value, (list, set)):
            value = u' '.join(set([self.quote_token(token) for token in value]))
        return super(ArrayEditWidget, self).render(name, value, *args, **kwargs)