#-*- coding: utf-8 -*-
"""
~~~~~~
Модели
~~~~~~
"""
from django.db import models
from django.db.models import Count
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from siteuser.models import User

from utils.models import TitledSlugEntry


class Poll(TitledSlugEntry):
    """
    Модель опроса.
    """
    #content_type = models.ForeignKey(ContentType, null=True, blank=True, related_name='polls', verbose_name=u'Тип обобщенного объекта')
    #object_id = models.IntegerField(verbose_name=u'Идентификатор обобщенного объекта')
    #content_object = generic.GenericForeignKey('content_type', 'object_id')
    created_at = models.DateTimeField(verbose_name=u'Время создания', editable=False, auto_now_add=True)
    is_active = models.BooleanField(verbose_name=u'Активен?', default=True)
    allow_multiple = models.BooleanField(default=False, verbose_name=u'Допускает множественный ответ')

    class Meta:
        verbose_name = u'Опрос'
        verbose_name_plural = u'Опросы'

    def save(self, *args, **kwargs):
        super(Poll, self).save(*args, **kwargs)
        if self.is_active:
            Poll.objects.exclude(pk=self.pk).update(is_active=False)

    def user_has_voted(self, user):
        return self.answers.filter(user=user).count() > 0

    @property
    def options_and_votes(self):
        r = getattr(self, '__options_and_votes', None)
        if r is None:
            r = self.options.annotate(votes=Count('answers'))
            setattr(self, '__options_and_votes', r)
        return r

    @property
    def overall_votes(self):
        """
        Возвращает количество поучавствовавших в опросе пользователей.
        """
        return sum(x.votes for x in self.options_and_votes)

    @staticmethod
    def get_latest_active():
        try:
            return Poll.objects.select_related().filter(is_active=True).latest('created_at')
        except Poll.DoesNotExist:
            pass


class Option(models.Model):
    """
    Модель варианта ответа на опрос.
    """
    poll = models.ForeignKey(Poll, related_name='options', verbose_name=u'Опрос')
    position = models.PositiveIntegerField(verbose_name=u'Позиция')
    option = models.CharField(max_length=255, verbose_name=u'Текст ответа')

    def __unicode__(self):
        return self.option

    class Meta:
        ordering = ('position',)
        verbose_name = u'Ответ в опросе'
        verbose_name_plural = u'Ответы в опросах'


class Answer(models.Model):
    """
    Модель ответа на опрос.
    """
    poll = models.ForeignKey(Poll, related_name='answers', verbose_name=u'Опрос')
    user = models.ForeignKey(User, related_name='poll_answers', verbose_name=u'Пользователь')
    created_at = models.DateTimeField(verbose_name=u'Время голосования', auto_now_add=True)
    chosen = models.ForeignKey(Option, related_name='answers', verbose_name=u'Выбранный вариант ответа')

    class Meta:
        unique_together = ('user', 'chosen')
        verbose_name = u'Ответ пользователя в опросе'
        verbose_name_plural = u'Ответы пользователей в опросах'


def get_poll_for_item(item, user):
    """
    Возвращает опрос, связанный с объектом ``item``, заполняя переменную ``user_has_voted`` если ``user`` участвовал в опросе.
    """
    try:
        poll = Poll.objects.prefetch_related('options')\
               .filter(object_id=item.pk, content_type=ContentType.objects.get_for_model(item.__class__))\
               .latest('created_at')
        if user.is_authenticated():
            poll.user_has_voted = Answer.objects.filter(poll=poll, user=user).count() > 0
        else:
            poll.user_has_voted = False
        return poll
    except Poll.DoesNotExist:
        return None
