from django.contrib import admin

from colloquium.models import Poll, Option, Answer


class OptionAdmin(admin.StackedInline):
  model = Option
  max_num = None
  extra = 0


class PollAdmin(admin.ModelAdmin):
  list_display = ('title', 'created_at')
  date_hierarchy = 'created_at'
  search_fields = ('title',)
  inlines = [OptionAdmin]


admin.site.register(Poll, PollAdmin)
#admin.site.register(Option, admin.ModelAdmin)
#admin.site.register(Answer, admin.ModelAdmin)
