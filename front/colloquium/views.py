#-*- coding: utf-8 -*-
"""
~~~~~~~~~~~~~
Представления
~~~~~~~~~~~~~
"""
from django.http import HttpResponseNotAllowed, HttpResponseBadRequest
from django.shortcuts import get_object_or_404, redirect


from colloquium.models import Poll, Option, Answer


def vote(request, poll_id):
    if request.method != 'POST': return HttpResponseNotAllowed()
    if not request.user.is_authenticated(): return HttpResponseBadRequest()
    poll = get_object_or_404(Poll, pk=poll_id)
    payload_key = u'poll.%d' % poll.pk
    options = Option.objects.filter(id__in=request.POST.getlist(payload_key))
    for option in options:
        Answer.objects.create(poll=poll, user=request.user, chosen=option)
    return redirect(request.POST.get('next'))
