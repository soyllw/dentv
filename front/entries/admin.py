#coding: utf-8
from datetime import datetime, timedelta
from django.contrib import admin
from django.db import models
from django.forms import ModelForm, TextInput

from djorm_pgarray.fields import ArrayField
from imagekit.admin import AdminThumbnail

from fields import RichTextField
from widgets import FCKEditorWidget
from tinymce.widgets import TinyMCE

from entries.models import Entry, EntryUserRole, EntryPromotion, Question, EntryImportTask, EntryOnAir, YTLiveEvent, AirSchedule, EntryAnnounce, DebateItem
from siteuser.models import User
from content.models import YoutubeVideo, HtmlText, MarkdownText
from tags.fields import ArrayEditField, ArrayEditWidget
from saga.actions import enable_comments, disable_comments
#from airadmin import AirAdmin


mce_attrs = {
    'plugins': 'paste',
    'theme': 'advanced',
    'paste_auto_cleanup_on_paste': True,
    'theme_advanced_buttons3_add': 'pastetext,pasteword,selectall'
}


class YouTubeVideoInline(admin.StackedInline):
    model = YoutubeVideo
    max_num = None
    extra = 0

    formfield_overrides = {
        models.TextField: {'widget': TextInput(attrs={'class': 'vTextField'})},
    }

class HtmlTextInline(admin.StackedInline):
    model = HtmlText
    max_num = None
    extra = 0

    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(mce_attrs=mce_attrs, attrs={'cols': 80, 'rows': 30})},
    }


class MarkdownTextInline(admin.StackedInline):
    model = MarkdownText
    max_num = None
    extra = 0


class UserRoleInline(admin.TabularInline):
    model = EntryUserRole
    max_num = None
    extra = 0
    raw_id_fields = ('user',)
    autocomplete_lookup_fields = {
        'fk': ['user']
    }


class EntryAdminForm(ModelForm):
    tags = ArrayEditField(widget=ArrayEditWidget)

    class Meta:
        model = Entry


class EntryAdmin(admin.ModelAdmin):
    date_hierarchy = 'released_at'
    list_display = ('title', 'program', 'released_at', 'admin_thumb_source')
    list_filter = ('program',)
    search_fields = ('title',)
    exclude = ('content',)
    inlines = [UserRoleInline, YouTubeVideoInline, HtmlTextInline, MarkdownTextInline]
    form = EntryAdminForm
    admin_thumb_source = AdminThumbnail(image_field='thumb_190')
    admin_thumb_source.short_description = u'Обложка'
    actions = [disable_comments, enable_comments]


class EntryPromotionAdmin(admin.ModelAdmin):
    list_display = ('entry', 'start', 'end')
    search_fields = ('entry',)
    raw_id_fields = ('entry',)
    autocomplete_lookup_fields = {
        'fk': ['entry']
    }


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('entry', 'email', 'text', 'created_at', 'support')
    list_filter = ('entry',)


class EntryOnAirInline(admin.StackedInline):
    model = EntryOnAir
    max_num = None
    extra = 5
    inline_classes = ('grp-collapse grp-open',)
    raw_id_fields = ('entry',)
    autocomplete_lookup_fields = {
        'fk': ['entry']
    }


class AirScheduleAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    inlines = [EntryOnAirInline]


class EntryImportTaskAdmin(admin.ModelAdmin):
    exclude = ('entry',)
    list_display = ('link', 'finished')

    def has_delete_permission(self, request, obj=None):
        return False


class DebateItemsInline(admin.StackedInline):
    model = DebateItem
    extra = 0


class YTLiveEventAdmin(admin.ModelAdmin):
    list_display = ('start', 'stop', 'link')
    inlines = [DebateItemsInline]


admin.site.register(EntryPromotion, EntryPromotionAdmin)
admin.site.register(Entry, EntryAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(EntryImportTask, EntryImportTaskAdmin)
admin.site.register(YTLiveEvent, YTLiveEventAdmin)
admin.site.register(AirSchedule, AirScheduleAdmin)
admin.site.register(EntryAnnounce)
#admin.site.register(EntryOnAir)
