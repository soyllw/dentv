from django.forms import Textarea
from django.forms.util import flatatt
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode


class FCKEditorWidget(Textarea):
  class Media:
    js = ('/static/ckeditor/ckeditor.js', '/static/ckeditor/django.js')
  
  def render(self, name, value, attrs={}):
    if value is None:
      value = ''
    final_attrs = self.build_attrs(attrs, name=name)
    final_attrs['data-fck'] = "true"
    value = conditional_escape(force_unicode(value))
    textarea = """<textarea {0}>{1}</textarea>""".format(flatatt(final_attrs), value)
    script = ''
    if "__prefix__" not in final_attrs['id']:
      script = """<script>CKEDITOR.replace("{0}");</script>""".format(final_attrs['id'])
    return mark_safe(textarea + script)