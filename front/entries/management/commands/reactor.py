from django.core.management import BaseCommand
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from external.models import Token
from entries.models import EntryImportTask, EntryExportTask, Entry
from content.models import YoutubeVideo

import urllib2, urlparse, time, simplejson
from urllib import urlencode
from datetime import datetime
from apiclient.discovery import build

"""
vk auth url:
https://oauth.vk.com/authorize?client_id=4512984&redirect_uri=https://oauth.vk.com/blank.html&scope=wall,pages,video,groups,offline
https://oauth.vk.com/access_token?client_id=4512984&client_secret=...&code=...&redirect_uri=https://oauth.vk.com/blank.html
"""

class Command(BaseCommand):
    def import_video(self, task):
        response = self.service.videos().list(part='snippet', id=task.get_youtube_id()).execute()
        item = response['items'][0]['snippet']
        thumbnail = sorted(item['thumbnails'].values(), key=lambda w: -w['width'])[0]
        entry = Entry(enabled = False, title=item['title'], description=item['description'], program_id=task.program_id)
        n = datetime.now()
        thumb_fname = '%s-%s' % (str(n.date()), urlparse.urlparse(thumbnail['url']).path.split('/')[-1])
        thumb_temp = NamedTemporaryFile(delete=True)
        thumb_temp.write(urllib2.urlopen(thumbnail['url']).read())
        thumb_temp.flush()
        entry.thumb_source.save(thumb_fname, File(thumb_temp), save=True)
        YoutubeVideo.objects.create(entry=entry, content=task.get_canonical_url())
        task.entry = entry
        task.finished = True
        task.save()

    def export_entry_to_vk(self, task):
        vk_token = self.tokens.get('Vkontakte')
        dentv_vk  = '38085148'
        zavtra_vk = '32969466'
        vk_url = 'https://api.vk.com/method'
        vk_video_data = { 'name': task.entry.title.encode('utf-8')
                        , 'description': task.entry.description.encode('utf-8')
                        , 'wallpost': 0
                        , 'link': task.entry.video_links[0]
                        , 'access_token': vk_token.refresh_token
                        , 'group_id': dentv_vk
                        }
        vk_post_data = { 'friends_only': 1
                       , 'from_group': 1
                       , 'access_token': vk_token.refresh_token
                       , 'message': vk_video_data['name']
                       }
        response = simplejson.loads(urllib2.urlopen('%s/video.save' % vk_url, urlencode(vk_video_data)).read()).get('response')
        urllib2.urlopen(response['upload_url']).read()
        for group in [dentv_vk, zavtra_vk]:
          post = urlencode(dict( vk_post_data
                               , owner_id = '-%s' % group
			       , services = 'twitter,facebook'
                               , attachments = 'video%s_%s' % (response['owner_id'], response['vid'])
                               ))
          urllib2.urlopen('%s/wall.post' % vk_url, post).read()

    def export_entry_to_fb(self, task):
        pass

    def start_loop(self):
        while True:
            for task in EntryImportTask.objects.filter(finished=False):
		try:
                    self.import_video(task)
                except:
                    pass
            for task in EntryExportTask.objects.select_related().filter(finished=False):
                try:
                    #self.export_entry_to_fb(task)
                    #self.export_entry_to_vk(task)
                    task.finished = True
                    task.save()
                except:
                    pass
            time.sleep(180)

    def handle(self, *args, **kwargs):
        self.tokens = dict((x.get_service_display(), x) for x in Token.objects.all())
        self.service = build("youtube", "v3", developerKey=self.tokens.get('YouTube').api_key)
        self.start_loop()
