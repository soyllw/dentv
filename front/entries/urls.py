from django.conf.urls import patterns, url

from entries.views import EntryView, PromotionView, SubProjectIndex
from entries.feeds import EntriesFeed

urlpatterns = patterns('',
  #url(r'^(?P<project>(red|white))\-project/$', SubProjectIndex.as_view(), name='entries.view.sub_project'),
  url(r'^view/(?P<slug>[-_A-Za-z0-9]+)/$', EntryView.as_view(), name='entries.view.item'),
  url(r'^questions/$', 'entries.views.add_question', name='entries.view.add_question'),
  url(r'^promotion/$', PromotionView.as_view(), name='entries.views.promotion'),
  url(r'^feed/$', EntriesFeed(), name='entries.feed'),
)
