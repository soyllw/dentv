#-*- coding: utf-8 -*-
"""
~~~~~~~~~~~~~
Представления
~~~~~~~~~~~~~
"""
from datetime import datetime

from django.views.generic import DetailView, ListView
from django.http import HttpResponseNotAllowed
from django.shortcuts import redirect
from django.contrib import messages
from django.db.models import Q

from djorm_expressions.base import SqlExpression

#from colloquium.models import get_poll_for_item
from utils.view_utils import TemplateViewMixin

from entries.models import Entry, EntryPromotion
from entries.forms import QuestionForm
from content.models import YoutubeVideo

class SubProjectIndex(TemplateViewMixin, ListView):
    template_name = 'entries/sub_project.html'
    paginate_by = 15

    def get_context_data(self, *args, **kwargs):
        context = super(SubProjectIndex, self).get_context_data(*args, **kwargs)
        context['project'] = self.kwargs['project']
        context['promo'] = self.promo
        return context

    def get_queryset(self):
        self.promo = Entry.archive.latest('released_at')
        qs = Entry.archive.select_related()
        if self.kwargs['project'] == 'red':
            pass
        else:
            pass
        return qs


class EntryView(TemplateViewMixin, DetailView):
    """
    CBV выпуска
    """
    template_name = 'entries/item.html'
    context_object_name = 'entry'

    def get_queryset(self):
        n = datetime.now()
        return Entry.objects.filter(released_at__lte=n).select_related().prefetch_related(
            'questions', 'user_roles', 'user_roles__user'
        )

    def get_context_data(self, *args, **kwargs):
        #from django.db import connection
        context = super(EntryView, self).get_context_data(*args, **kwargs)
        ents = Entry.objects.filter(slug=self.kwargs['slug'])
        tags = []
        tagged = []
        #query_parts = []
        for tag in ents[0].tags:
            tagged_articles = Entry.objects.where(SqlExpression("tags", "@>", [tag]))[0:10]
            for article in tagged_articles:
                tagged.append(article.id)
        tagged = set(tagged)
        tagged = list(tagged)
        tagged = sorted(tagged, reverse=True)
        #    tag_str = tag[1:]
        #    tags.append(tag_str)
        #    query_parts.append("tags && ARRAY['" + tag_str + "']")
        #query_part = " AND ".join(query_parts)
        #cursor = connection.cursor()
        #cursor.execute("""
        #        SELECT
        #          id
        #        FROM
        #          entries_entry
        #        WHERE
        #          """ + query_part + """
        #        ORDER BY
        #          released_at DESC
        #        """)
        #list_ = cursor.fetchall()
        #print list_
        #entries = map(lambda x: x[1], cursor.fetchall())
        #print Entry.objects.filter(tags__in=tags).distinct()
        leftbar_entries = []
        rightbar_entries = []
        for id in tagged[0:3]:
            entry = Entry.objects.defer("content").get(pk=id)
            rightbar_entries.append(entry)
        for author in ents[0].user_roles.all():
            leftbar_entries.append(author)
        print(rightbar_entries)
        context['leftbar_entries'] = leftbar_entries
        context['rightbar_entries'] = rightbar_entries
        context['youtube_id'] = ents[0].video_id()
        return context


class PromotionView(TemplateViewMixin, ListView):
    """
    CBV раскручиваемых выпусков
    """
    template_name = 'entries/promotion.html'
    allow_empty = False

    def get_queryset(self):
        now = datetime.now()
        return EntryPromotion.objects.select_related().filter(
            (Q(start__isnull = True) | Q(start__lte = now)) &\
            (Q(end__isnull = True) | Q(end__gte = now))
        )


def add_question(request):
    """
    принимает ``post`` запрос пользователя с формой добавления вопроса :class:`entries.forms.QuestionForm`,
    добавляет опрос и перенаправляет пользователя к выпуску, к которому относится вопрос.
    В случае неверного заполнения формы возвращает список ошибок.
    """
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user
            instance.save()
            return redirect(request.POST.get('next', instance.entry.get_absolute_url()))
        else:
            [messages.error(request, error.as_text(), extra_tags="comment_form") for error in form.errors.values()]
            return redirect(request.POST.get('next', request.META.get('HTTP_REFERER')))
    return HttpResponseNotAllowed()
