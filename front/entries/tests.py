from datetime import datetime

from django.test import TestCase
from django.core.urlresolvers import reverse

from siteuser.models import User
from programs.models import Program
from entries.models import Entry


def sieg_heil():
    test_user = User.objects.create(
        first_name = 'Adolf',
        last_name  = 'Hitler',
        username   = 'adolf',
        dob        = datetime(year=1889,month=4, day=20),
        gender     = 1,
        location   = 'Berlin, Deutschland',
        occupation = 'You know... Europe ;)',
        about      = 'Famous writer and painter')
    test_user.set_password('reich')
    test_user.is_staff = True
    test_user.save()
    return test_user


class EntriesTestCase(TestCase):

    def setUp(self):
        self.adolf = sieg_heil()

    def tearDown(self):
        for m in [User, Program, Entry]:
            m.objects.all().delete()

    def test_entry_feed_presence(self):
        self.assertEqual(self.client.get(reverse('entries.feed')).status_code, 404)
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        entry   = Entry.objects.create(released_at=datetime.now(), program=program, title='test entry', thumb_source="kekeke")
        self.assertEqual(self.client.get(reverse('entries.feed')).status_code, 200)
