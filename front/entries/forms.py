#-*- coding: utf-8 -*-
"""
~~~~~
Формы
~~~~~
"""
from django.forms import ModelForm, Textarea, HiddenInput, IntegerField

from entries.models import Question


class QuestionForm(ModelForm):
    """
    Форма вопроса авторам.
    """
    parent = IntegerField(widget=HiddenInput, required=False)

    class Meta:
        model = Question
        fields = ('entry', 'text')
        widgets = {
            'text': Textarea(attrs={'cols': 30, 'rows': 10}),
            'entry': HiddenInput,
        }
