#-*- coding: utf-8 -*-
"""
~~~~~~
Модели
~~~~~~
"""
from urlparse import urlparse, parse_qs
from datetime import datetime, timedelta
import itertools

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models.loading import get_models
from django.utils.datastructures import SortedDict
from django.utils.html import strip_tags

from djorm_pgarray.fields import ArrayField
from djorm_expressions.models import ExpressionManager

from imagekit.models import ImageSpecField
from imagekit.processors.resize import ResizeToFit, ResizeToFill

from djorm_pgfulltext.models import SearchManager
from djorm_pgfulltext.fields import VectorField

from utils.models import ExtendedModel, TitledSlugEntry,\
                         Rateable, ViewsCountable, Commentable,\
                         get_content_type

from siteuser.models import User
from programs.models import Program
from content.models import YoutubeVideo

from entries.managers import EntryArchiveManager, EntryAnnounceManager

from typo import typography


class Entry(ExtendedModel, TitledSlugEntry, Rateable, ViewsCountable):
    """
    Модель выпуска.
    """
    released_at  = models.DateTimeField(verbose_name=u'Время выхода', default=lambda: datetime.now())
    enabled      = models.BooleanField(verbose_name=u'Отображать?', default=True)
    description  = models.TextField(verbose_name=u'Анонс', blank=True)
    program      = models.ForeignKey(Program, verbose_name=u'Программа', related_name='entries')
    content      = models.TextField(verbose_name=u'Содержимое', null=True, blank=True)
    thumb_source = models.ImageField(upload_to='entries/thumbs', verbose_name=u'Обложка')

    thumb_640 = ImageSpecField([ResizeToFill(640, 360, 'b')], source='thumb_source', format='JPEG')
    thumb_400 = ImageSpecField([ResizeToFill(400, 225, 'b')], source='thumb_source', format='JPEG')
    thumb_190 = ImageSpecField([ResizeToFill(190, 113, 'b')], source='thumb_source', format='JPEG')
    thumb_300 = ImageSpecField([ResizeToFill(300, 169, 'b')], source='thumb_source', format='JPEG')

    def _thumb_640(self):
        try:
            return self.thumb_640
        except:
            return None
    thumb_640 = property(_thumb_640)

    #def _thumb_400(self):
    #    try:
    #        return self.thumb_400
    #    except:
    #        return None
    #thumb_400 = property(_thumb_400)

    def _thumb_190(self):
        try:
            return self.thumb_190
        except:
            return None
    thumb_190 = property(_thumb_190)

    def _thumb_300(self):
        try:
            return self.thumb_300
        except:
            return None
    thumb_300 = property(_thumb_300)

    #: Теги
    tags = ArrayField(dbtype="text", verbose_name=u"Теги", default=lambda: [])

    search_index = VectorField()

    #: стандартный менеджер
    objects = ExpressionManager()
    #: архивный менеджер
    archive = EntryArchiveManager()
    #: менеджер анонсов
    announce = EntryAnnounceManager()
    #: менеджер поиска
    searcher = SearchManager(
        fields=(('title', 'A'), ('description', 'B'), ('content', 'C')),
        #fields = ('title', 'description', 'content'),
        config='pg_catalog.russian',
        search_field='search_index',
        auto_update_search_field=True
    )

    class Meta:
        verbose_name = u'Выпуск'
        verbose_name_plural = u'Выпуски'
        ordering = ('-released_at', 'id')

    def __unicode__(self):
        return u'%s' % self.title

    def compile(self, *args, **kwargs):
        """
        Компилирует выпуск из имеющихся в БД частей различного рода, сортируя их в соответствии с атрибутом `position`.
        Скомпилированное содержимое выпуска помещается в поле `content` объекта.
        """
        parts = sorted(filter(lambda x: x.enabled, self.parts), key=lambda x: x.position)
        content = []

        # TODO: tests
        for position, parts_pos in itertools.groupby(parts, key=lambda x: x.position):
            parts_pos = list(parts_pos)
            if hasattr(parts_pos[0].__class__, 'compile_group'):
                parts_types = map(lambda x: x.__class__.__name__, parts_pos)
                """
                all_same = reduce(lambda a,x: a == x and x, parts_types, parts_types[0])
                if not all_same:
                    raise Exception, "Multi-valued positions disallowed for %s" % parts_types[0]
                else:
                    content.append(parts_pos[0].__class__.compile_group(parts_pos))
                """
            elif len(parts_pos) == 1:
                content.append(parts_pos[0].compile())
            else:
                raise Exception, "Multi-valued positions disallowed for %s" % parts_pos[0].__class__.__name__

        content = u''.join(content)
        # Maybe, there is no need to save in compile.
        # 1) Separate concerns: compile compiles, save saves
        # 2) Actual saving of compiled content is done via signal, which will be invoked
        #    in infinite loop if we save in compile method
        # -- if save: self.save()
        return content

    @staticmethod
    def full_recompile():
        for e in Entry.objects.all():
            content = e.compile(save=False)
            Entry.objects.filter(pk=e.pk).update(content=content)

    def get_parts(self):
        import content.models as mdl
        parts = []
        for model in get_models(mdl):
            [parts.append(part) for part in model.objects.filter(entry=self).all()]
        return sorted(parts, key=lambda x: x.position)

    def set_parts(self, parts):
        all_parts = self.get_parts()
        [part.delete() for part in all_parts if part not in parts]
        for part in parts:
            part.entry = self
            part.save()

    parts = property(get_parts, set_parts)

    @property
    def video_links(self):
        return [p.content for p in self.parts if p.is_video]

    def video_id(self):
        id_video = u''
        for p in self.parts:
            if p.is_video:
                url_result = urlparse(p.content)
                id_video = url_result.query.split('=')[1].strip()
                break
        return id_video

    def get_related(self, num=5):
        from django.db import connection
        ctype = get_content_type(self)
        query = """
        SELECT
          fid.document_id, SUM(ff.relative_weight) as weight
        FROM
          features_featureindocument AS fid
        INNER JOIN
          features_featurefrequency AS ff
          ON ff.feature_id = fid.feature_id
        WHERE
          fid.document_id <> %(did)s
          AND
          fid.document_type_id = %(dtid)s
          AND
          fid.feature_id IN (
            SELECT
              feature_id
             FROM
               features_featureindocument
             WHERE
               document_id = %(did)s
               AND
               document_type_id = %(dtid)s
          )
        GROUP BY fid.document_id
        ORDER BY weight DESC
        LIMIT %(num)s
        """ % {'did': self.id, 'dtid': ctype.id, 'num': num}
        cursor = connection.cursor()
        cursor.execute(query)
        result = [p[0] for p in cursor.fetchall()]
        cursor.close()
        return Entry.archive.filter(pk__in=result).defer('content')

    def save(self, *args, **kwargs):
        """
        Сохраняет выпуск в БД, обновляя статус блоггера у владельца программы, к которой относится выпуск.
        """
        self.description = typography(self.description)
        if self.tags is None:
            self.tags = []
        super(Entry, self).save(*args, **kwargs)
        owner = self.program.owner
        if owner.pk != 1:
            # TODO: deletion?
            owner.has_blog = True
            owner.save()

    @models.permalink
    def get_absolute_url(self):
        """
        Абсолютный URL выпуска на сайте.
        """
        return ('entries.view.item', (), {'slug': self.slug})

    @property
    def features(self):
        """
        Генерирует поток свойств документа в виде кортежа: (код свойства, значение свойства)
        """
        for t in self.tags: yield ('tag', t.lower())
        yield ('program', self.program.title.lower())
        for a in self.user_roles.select_related(): yield ('user', a.user.display_name.lower())
        """
        if self.content is not None:
            content = strip_tags(self.content)
            for x in
        """

    @property
    def others(self):
        """
        Выпуски, относящиеся к той же программе, что и данный.
        """
        return Entry.archive.filter(program=self.program).exclude(pk=self.pk).defer('content')

    @property
    def question_form(self):
        """
        Привязанная форма вопросов авторам выпуска.
        """
        from entries.forms import QuestionForm
        return QuestionForm(initial={'entry': self})

    def get_user_roles(self):
        """
        Возвращает словарь: ``роль в выпуске`` => [``список участников``]
        """
        users = SortedDict()
        for role in self.user_roles.all():
            if role.user.pk != 1:
                users.setdefault(role.get_role_display(), []).append(role.user)
        return users

    @staticmethod
    def get_top_blog_entries():
        from django.db import connection
        cursor = connection.cursor()
        cursor.execute("""
        SELECT
          DISTINCT ON (e.program_id) e.program_id,
          e.id
        FROM
          entries_entry AS e
        INNER JOIN
          programs_program AS p
          ON p.id = e.program_id AND p.owner_id <> 1
        WHERE
          e.released_at < NOW()
        ORDER BY
          e.program_id DESC, e.released_at DESC
        """)
        entries = map(lambda x: x[1], cursor.fetchall())
        return Entry.archive.prefetch_related('program__owner').\
               filter(pk__in = entries).\
               exclude(program__owner__avatar_source = '').\
               defer('content')[0:4]

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)

    @property
    def authors(self):
        #print('allo')
        users = set()
        for role in self.user_roles.all():
            if role.user.pk != 1:
                users.add(role.user.display_name)
        #print(users)
        #print(', '.join(users))
        return ', '.join(users)

class EntryUserRole(ExtendedModel):
    """
    Модель ролей пользователей в выпуске.
    """

    #: типы ролей
    ROLES = (
        (1, u'Ведущие'),
        (2, u'Гости'),
        (3, u'Постоянные гости'),
        (4, u'Корреспонденты'),
    )
    position = models.IntegerField(default=0, verbose_name=u'Позиция')
    entry = models.ForeignKey(Entry, verbose_name=u'Выпуск', related_name='user_roles')
    user = models.ForeignKey(User, verbose_name=u'Пользователь', related_name='entries')
    role = models.IntegerField(choices=ROLES, verbose_name=u'Роль', default=1)
    role_text = models.CharField(verbose_name=u'Роль в выпуске (если нет среди предопределенных)', max_length=255, blank=True)
    comment = models.TextField(verbose_name=u'Комментарий к роли', blank=True)

    class Meta:
        verbose_name = u'Пользователь в выпуске'
        verbose_name_plural = u'Пользователи в выпуске'
        ordering = ('role', 'position',)

    def __unicode__(self):
        return u'%s' % self.position


class Question(ExtendedModel):
    """
    Модель вопроса авторам выпуска.
    """
    entry = models.ForeignKey(Entry, verbose_name=u'Выпуск', related_name=u'questions')
    fio = models.CharField(max_length=512, verbose_name=u'Имя (для незарегистрированных)', blank=True)
    email = models.EmailField(verbose_name=u'Email (для незарегистрированных)', blank=True)
    text = models.TextField(verbose_name=u'Текст вопроса')
    reply = models.ForeignKey('self', blank=True, null=True, verbose_name=u'Ответ')
    author = models.ForeignKey(User, verbose_name=u'Автор вопроса', related_name='questions')
    enabled = models.BooleanField(verbose_name=u'Разрешен', default=True)
    created_at = models.DateTimeField(verbose_name=u'Время создания', auto_now_add=True)
    support = models.IntegerField(default=0, verbose_name=u'Количество поддержавших')

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'

    @models.permalink
    def get_absolute_url(self):
        """
        Абсолютный URL вопроса на сайте.
        """
        return ('entries.view.item', (), {'slug': self.entry.slug})


class QuestionSupport(ExtendedModel):
    """
    Модель поддержки вопроса авторам выпуска.
    """
    question = models.ForeignKey(Question, verbose_name=u'Вопрос', related_name='supports')
    user = models.ForeignKey(User, verbose_name=u'Поддержавший', blank=True, null=True, related_name='question_supports')
    ip = models.IPAddressField(verbose_name=u'IP поддержавшего')

    class Meta:
        verbose_name = u'Поддержка вопроса'
        verbose_name_plural = u'Поддержка вопросов'


class EntryPromotion(models.Model):
    entry = models.ForeignKey(Entry, verbose_name=u'Выпуск')
    start = models.DateTimeField(null=True, blank=True, verbose_name=u'Начало периода')
    end = models.DateTimeField(null=True, blank=True, verbose_name=u'Конец периода')
    text = models.CharField(max_length=1024, verbose_name=u'Описание')

    class Meta:
        verbose_name = u'Рекламируемый выпуск'
        verbose_name_plural = u'Рекламируемые выпуски'

    def clean(self):
        try:
            if self.start > self.end:
                raise ValidationError(u'Начало периода раскрутки должно быть раньше окончания периода')
        except TypeError:
            # type error is raised when `start` or `end` is None
            # which means, that it is unbounded and it's ok
            pass


class EntryImportTask(models.Model):
    link     = models.CharField(verbose_name=u'Ссылка на YouTube', max_length = 128)
    finished = models.BooleanField(verbose_name=u'Завершено', default=False, editable=False)
    program  = models.ForeignKey(Program, verbose_name=u'Поместить в программу')
    entry    = models.ForeignKey(Entry, null=True, editable=False)

    def get_youtube_id(self):
        u = urlparse(self.link)
        q = parse_qs(u.query)
        return q.get('v', q.get('video_id'))[0]

    def get_canonical_url(self):
        return 'http://www.youtube.com/watch?v=%s' % self.get_youtube_id()

    class Meta:
        verbose_name = u'Импорт ролика'
        verbose_name_plural = u'Импорт роликов'


class EntryExportTask(models.Model):
    entry = models.ForeignKey(Entry, unique=True)
    finished = models.BooleanField(default=False)


class YTLiveEvent(models.Model):
    link  = models.CharField(max_length=200)
    start = models.DateTimeField()
    stop  = models.DateTimeField()

    def __unicode__(self):
        return self.link

    def get_video_id(self):
        try:
            u = urlparse(self.link)
            q = parse_qs(u.query)
            return q.get('v', q.get('video_id'))[0]
        except KeyError:
            return ''

    def yt(self):
        return """<iframe class="video" type="text/html" width="640" height="360" src="http://youtube.com/embed/{0}?autoplay=1&html5=1" frameborder="0" allowfullscreen></iframe>""".format(self.get_video_id(),)

    class Meta:
        ordering = ('-start',)
        verbose_name = u'YouTube-трансляция'
        verbose_name_plural = u'YouTube-трансляции'


class DebateItem(models.Model):
    live = models.ForeignKey(YTLiveEvent, verbose_name=u'Эфир', related_name='debatees')
    name = models.CharField(max_length=200, verbose_name=u'ФИО')

    class Meta:
        ordering = ('id',)
        verbose_name = u'Участник дебатов'
        verbose_name_plural = u'Участники дебатов'


class DebateItemVote(models.Model):
    item   = models.ForeignKey(DebateItem)
    upvote = models.BooleanField(default=False)
    ip     = models.IPAddressField()


class AirSchedule(models.Model):
    date = models.DateField(verbose_name = u'Дата', default=datetime.now(), unique=True)

    def __unicode__(self):
        return unicode(self.date)

    @staticmethod
    def actual():
        now = datetime.now()
        dnow = now.date()
        t = now.time()
        week_start = (now - timedelta(days=now.weekday())).date()
        week_end = week_start + timedelta(days=7)
        schedules = AirSchedule.objects.select_related().filter(date__gte = week_start, date__lt = week_end)
        result = []
        for schedule in schedules.all():
            result.extend([(schedule.date, schedule.date == dnow and t >= e.start and t <= e.end, e) for e in schedule.entries.all()])
        return result

    class Meta:
        verbose_name = u'Расписание на день'
        verbose_name_plural = u'Расписания эфиров'


class EntryOnAir(models.Model):
    schedule = models.ForeignKey(AirSchedule, verbose_name = u'Расписание', related_name='entries')
    entry    = models.ForeignKey(Entry, null=True, blank=True, verbose_name=u'Выпуск')
    title    = models.CharField(max_length=200, verbose_name=u'Название для сетки')
    start    = models.TimeField(verbose_name=u'Время начала трансляции')
    end      = models.TimeField(verbose_name=u'Время окончания трансляции')
    is_live  = models.BooleanField(verbose_name=u'Лайв?', default=True, editable=False)

    """
    @staticmethod
    def today():
        now = datetime.now()
        today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        tomorrow = today + timedelta(days=1)
        grid = list(EntryOnAir.objects.select_related().filter(date__gte = today, date__lt = tomorrow))
        glen = timedelta(minutes=1)*sum(g.duration for g in grid)
        if len(grid) > 0:
            run = 0
            while True:
                for g in grid:
                    begin = g.date + run*glen
                    duration = timedelta(minutes=1)*g.duration
                    end = begin+duration
                    yield (g, begin, begin <= now < end)
                    if end >= tomorrow: raise StopIteration
                run += 1
    """

    class Meta:
        ordering = ('start',)
        verbose_name = u'Элемент программы эфира'
        verbose_name_plural = u'Программа эфира'


class EntryAnnounce(TitledSlugEntry):
    start_at = models.DateTimeField(verbose_name=u'Дата начала показа')
    stop_at = models.DateTimeField(verbose_name=u'Дата окончания показа')
    content = models.TextField(verbose_name=u'Текст анонса')
    thumb_source = models.ImageField(upload_to='announces/thumbs', verbose_name=u'Изображение')

    thumb_640 = ImageSpecField([ResizeToFill(640, 360, 'b')], source='thumb_source', format='JPEG')
    thumb_400 = ImageSpecField([ResizeToFill(400, 225, 'b')], source='thumb_source', format='JPEG')
    thumb_190 = ImageSpecField([ResizeToFill(190, 113, 'b')], source='thumb_source', format='JPEG')
    thumb_300 = ImageSpecField([ResizeToFill(300, 169, 'b')], source='thumb_source', format='JPEG')

    class Meta:
        verbose_name = u'Анонс'
        verbose_name_plural = u'Анонсы'

    def __unicode__(self):
        return self.title


import entries.signals
