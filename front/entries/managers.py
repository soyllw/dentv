#-*- coding: utf-8 -*-
"""
~~~~~~~~~~~~~
ORM-менеджеры
~~~~~~~~~~~~~
"""
from datetime import datetime

from django.db import models
from djorm_expressions.models import ExpressionManager


class EntryArchiveManager(ExpressionManager):
    """
    Менеджер архивных выпусков.
    """
    def get_query_set(self):
        now = datetime.now()
        return super(EntryArchiveManager, self).get_query_set()\
               .filter(released_at__lte=now, enabled=True)\
               .exclude(models.Q(thumb_source=None) | models.Q(thumb_source=''))


class EntryAnnounceManager(ExpressionManager):
    """
    Менеджер анонсов.
    """
    def get_query_set(self):
        now = datetime.now()
        return super(EntryAnnounceManager, self).get_query_set()\
               .filter(released_at__gt=now, enabled=True)
