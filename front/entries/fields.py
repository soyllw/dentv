from widgets import FCKEditorWidget
from django import forms


class RichTextField(forms.fields.Field):
  def __init__(self, *args, **kwargs):
    kwargs.update({'widget': FCKEditorWidget()})
    super(RichTextField, self).__init__(*args, **kwargs)