#-*- coding: utf-8 -*-
"""
~~~~~~~
Сигналы
~~~~~~~
"""
from django.db.models.signals import post_save, post_delete
from django.core.cache import cache

from entries.models import Entry, EntryExportTask


def drop_entries_cache(*args, **kwargs):
    """
    Обработчик сигнала сброса кеша анонсов.
    """
    cache.delete('announce')

def recompile_content(sender, instance, *args, **kwargs):
    compiled_content = instance.compile()
    Entry.objects.filter(pk=instance.pk).update(content=compiled_content)

def create_export_task(sender, instance, *args, **kwargs):
    if instance.enabled and EntryExportTask.objects.filter(entry=instance).count() == 0:
        EntryExportTask.objects.create(entry=instance)

post_save.connect(recompile_content, sender=Entry, dispatch_uid='entries.signals.post_save_recompile')
post_save.connect(create_export_task, sender=Entry, dispatch_uid='entries.signals.create_export_task')
post_save.connect(drop_entries_cache, sender=Entry, dispatch_uid='entries.signals.post_save_drop_cache')
post_delete.connect(drop_entries_cache, sender=Entry, dispatch_uid='entries.signals.post_delete_drop_cache')
