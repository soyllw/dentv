#-*- coding: utf-8 -*-
"""
~~~~~~~~~~~~~~~
RSS/Atom-потоки
~~~~~~~~~~~~~~~
"""
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.utils import feedgenerator
from django.shortcuts import Http404

from entries.models import Entry


class EntriesFeed(Feed):
    """
    RSS поток выпусков.
    """
    feed_type = feedgenerator.Rss201rev2Feed
    ttl = 600

    def link(self):
        return reverse('entries.feed')

    def items(self, obj):
        items = Entry.archive.all()[0:10]
        if len(items) == 0:
            raise Http404
        return items

    def item_link(self, obj):
        return obj.get_absolute_url()

    def item_title(self, obj):
        return obj.title

    def item_description(self, obj):
        return obj.description
