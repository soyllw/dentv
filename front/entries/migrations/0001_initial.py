# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Entry'
        db.create_table(u'entries_entry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('slug', self.gf('autoslug.fields.AutoSlugField')(unique=True, max_length=1024, populate_from=None, unique_with=())),
            ('rating', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('views_count', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('comments_count', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('released_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('program', self.gf('django.db.models.fields.related.ForeignKey')(related_name='entries', to=orm['programs.Program'])),
            ('thumb_source', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'entries', ['Entry'])

        # Adding model 'EntryPart'
        db.create_table(u'entries_entrypart', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entry', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'parts', to=orm['entries.Entry'])),
            ('position', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('kind', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('content', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'entries', ['EntryPart'])

        # Adding model 'EntryUserRole'
        db.create_table(u'entries_entryuserrole', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('entry', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user_roles', to=orm['entries.Entry'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='entries', to=orm['siteuser.User'])),
            ('role', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('role_text', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'entries', ['EntryUserRole'])

        # Adding model 'Question'
        db.create_table(u'entries_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entry', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'questions', to=orm['entries.Entry'])),
            ('fio', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('reply', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['entries.Question'], null=True, blank=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questions', to=orm['siteuser.User'])),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('support', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'entries', ['Question'])

        # Adding model 'QuestionSupport'
        db.create_table(u'entries_questionsupport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(related_name='supports', to=orm['entries.Question'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='question_supports', null=True, to=orm['siteuser.User'])),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(max_length=15)),
        ))
        db.send_create_signal(u'entries', ['QuestionSupport'])


    def backwards(self, orm):
        # Deleting model 'Entry'
        db.delete_table(u'entries_entry')

        # Deleting model 'EntryPart'
        db.delete_table(u'entries_entrypart')

        # Deleting model 'EntryUserRole'
        db.delete_table(u'entries_entryuserrole')

        # Deleting model 'Question'
        db.delete_table(u'entries_question')

        # Deleting model 'QuestionSupport'
        db.delete_table(u'entries_questionsupport')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'entries.entry': {
            'Meta': {'ordering': "('-released_at', 'id')", 'object_name': 'Entry'},
            'comments_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'program': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': u"orm['programs.Program']"}),
            'rating': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'released_at': ('django.db.models.fields.DateTimeField', [], {}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '1024', 'populate_from': 'None', 'unique_with': '()'}),
            'thumb_source': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'views_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'})
        },
        u'entries.entrypart': {
            'Meta': {'ordering': "('position',)", 'object_name': 'EntryPart'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'parts'", 'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'entries.entryuserrole': {
            'Meta': {'ordering': "('role', 'position')", 'object_name': 'EntryUserRole'},
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_roles'", 'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'role': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'role_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': u"orm['siteuser.User']"})
        },
        u'entries.question': {
            'Meta': {'object_name': 'Question'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questions'", 'to': u"orm['siteuser.User']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'questions'", 'to': u"orm['entries.Entry']"}),
            'fio': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reply': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Question']", 'null': 'True', 'blank': 'True'}),
            'support': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'entries.questionsupport': {
            'Meta': {'object_name': 'QuestionSupport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'supports'", 'to': u"orm['entries.Question']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'question_supports'", 'null': 'True', 'to': u"orm['siteuser.User']"})
        },
        u'programs.program': {
            'Meta': {'ordering': "('-created_at', 'id')", 'object_name': 'Program'},
            'comments_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'on_top': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'programs'", 'to': u"orm['siteuser.User']"}),
            'rating': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '1024', 'populate_from': 'None', 'unique_with': '()'}),
            'subscribers_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'views_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'})
        },
        u'siteuser.user': {
            'Meta': {'object_name': 'User', '_ormbases': [u'auth.User']},
            'about': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'activation': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'avatar_source': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'comments_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'dob': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'generated': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'has_blog': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_comment_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'occupation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'update_value': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['entries']