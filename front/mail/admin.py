from django.contrib import admin

from mail.models import EmailTemplate, EmailQueue, EmailConfirmation, Subscriber


class EmailTemplateAdmin(admin.ModelAdmin):
  list_display = ('key', 'title', 'subj')


class EmailQueueAdmin(admin.ModelAdmin):
  date_hierarchy = 'sent_at'
  list_display = ('from_field', 'to_field', 'subject', 'created_at', 'sent_at')

class EmailConfirmationAdmin(admin.ModelAdmin):
  list_display = ('user', 'verkey')


admin.site.register(EmailTemplate, EmailTemplateAdmin)
admin.site.register(EmailQueue, EmailQueueAdmin)
admin.site.register(EmailConfirmation, EmailConfirmationAdmin)
admin.site.register(Subscriber)
