#-*- coding: utf-8 -*-
"""
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Команды консольного управления
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
from django.core.management.base import BaseCommand
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags

from mail.models import EmailQueue


class Command(BaseCommand):
    """
    Команда рассылки почты из очереди.
    """
    def handle(self, *args, **kwargs):
    # SET SEMAPHORE!!!
        q = EmailQueue.objects.filter(sent_at=None)[0:50]
        for m in q:
            msg = EmailMultiAlternatives(m.subject, strip_tags(m.body), m.from_field, [m.to_field])
            msg.attach_alternative(m.body, 'text/html')
            msg.send()
            m.mark_sent()
