#coding: utf-8
"""
~~~~~~
Модели
~~~~~~
"""
from datetime import datetime
import string
import random

from django.db import models
from django.template import Template, Context
from django.conf import settings
from django.core.cache import cache

from siteuser.models import User


class EmailTemplate(models.Model):
    """
    Модель шаблона письма.
    """

    class Meta:
        verbose_name = u'Шаблон письма'
        verbose_name_plural = u'Шаблоны писем'

    key = models.CharField(max_length=250, verbose_name=u'Ключ', unique=True, db_index=True)
    title = models.CharField(max_length=250, verbose_name=u'Название')
    subj = models.CharField(max_length=250, verbose_name=u'Тема письма')
    body = models.TextField(verbose_name=u'Тело письма')

    def send(self, receivers, sender=None, data={}):
        """
        Отправляет письмо списку получателей ``receivers`` от имени ``sender``.
        Перед отправкой письмо рендерится по шаблону с ипользованием словаря ``data``.
        """
        c = Context(data)
        subject = Template(self.subj).render(c)
        body = Template(self.body).render(c)
        if sender is None:
            sender = settings.DEFAULT_FROM_EMAIL
        for receiver in receivers:
            EmailQueue.objects.create(
            from_field=sender, to_field=receiver, subject=subject, body=body
            )

    def save(self, *args, **kwargs):
        """
        Записывает шаблон, сбрасывая его кеш.
        """
        super(EmailTemplate, self).save(*args, **kwargs)
        cache.delete('email-template-%s' % self.key)

    @staticmethod
    def get(key):
        """
        dict-like доступ к шаблонам по ключу, с прозрачным кешированием.
        """
        cache_key = 'email-template-%s' % key
        res = cache.get(cache_key)
        if res is None:
            res = EmailTemplate.objects.get(key=key)
            cache.set(cache_key, res, 60 * 60 * 24)
        return res


class EmailQueue(models.Model):
    """
    Модель почтовой очереди.
    """
    class Meta:
        verbose_name = u'Очередь email'
        verbose_name_plural = u'Очередь email'

    from_field = models.CharField(max_length=250, verbose_name=u'От')
    to_field = models.TextField(verbose_name=u'Получатель')
    subject = models.CharField(max_length=250, verbose_name=u'Тема')
    body = models.TextField(verbose_name=u'Содержимое')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Создано')
    sent_at = models.DateTimeField(blank=True, null=True, verbose_name=u'Отослано')

    def mark_sent(self):
        """
        Пометить письмо как отправленное.
        """
        self.sent_at = datetime.now()
        self.save()


class EmailConfirmation(models.Model):
    """
    Модель подтверждения.
    """
    class Meta:
        verbose_name = u"Подтверждение"
        verbose_name_plural = u"Подтверждения"

    verkey = models.CharField(max_length=128, verbose_name=u'Ключ подтверждения')
    user = models.ForeignKey(User, verbose_name=u'Пользователь')

    @staticmethod
    def create_confirmation(user):
        """
        Создаёт письмо о подтверждении, генерируя уникальный ключ.
        """
        alphabet = string.lowercase + string.digits
        conf_string = ''.join([random.choice(alphabet) for _ in xrange(100)])
        confirm = EmailConfirmation(user=user, verkey=conf_string)
        confirm.save()
        return confirm


class Subscriber(models.Model):
    email = models.EmailField(verbose_name=u'Email', blank=False, null=False)

    class Meta:
        verbose_name = u'Подписчики'
        verbose_name_plural = u'Подписчики'

    def __unicode__(self):
        if self.email:
            return self.email
        else:
            return u'Email'