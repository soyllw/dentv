# coding: utf-8
from django.db.models.signals import post_save
from django.db.models.loading import get_models

from topbanners.models import TopBanner


def disable_others_banners(sender, instance, *args, **kwargs):
    other_banners_in_same_position = TopBanner.objects.filter(active=True, position=instance.position).all().remove(instance)
    other_banners_in_same_position.update(active=False)

post_save.connect(disable_others_banners, sender=TopBanner, dispatch_uid='topbanners.signals.post_save_disable_others')