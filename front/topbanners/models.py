# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

class TopBanner(models.Model):
    """Слайдер."""
    code = models.TextField(max_length=5000, blank=True, verbose_name=u'Код для вставки')
    POSITIONS = (
        (1, u'Первый'),
        (2, u'Второй'),
        (3, u'Третий')
    )

    position = models.IntegerField(choices=POSITIONS, verbose_name=u'Позиция', default=1)
    active = models.BooleanField(blank=False, default = False, verbose_name=u'Отображать', null=False)

    #text = models.TextField(max_length=512, blank=False, verbose_name=u'Текст для вставки', null=True)
    image_source = models.ImageField(
        verbose_name=u"Изображение",
        upload_to='sliderimages/',
        blank=False,
        null=False
    )

    class Meta:
        verbose_name = u'Слайдер'
        verbose_name_plural = u'Слайдер'
