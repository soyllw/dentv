from datetime import datetime

from django.test import TestCase
from django.core.urlresolvers import reverse

from siteuser.models import User


def sieg_heil():
    test_user = User.objects.create(
        first_name = 'Adolf',
        last_name  = 'Hitler',
        username   = 'adolf',
        dob        = datetime(year=1889,month=4, day=20),
        gender     = 1,
        location   = 'Berlin, Deutschland',
        occupation = 'You know... Europe ;)',
        about      = 'Famous writer and painter')
    test_user.set_password('reich')
    test_user.save()
    return test_user


class UserTestCase(TestCase):

    def test_user_can_login(self):
        adolf = sieg_heil()
        self.assertEqual(self.client.login(username="adolf", password="reich"), True)
        adolf.delete()

    def test_user_profile_view(self):
        adolf = sieg_heil()
        self.client.login(username="adolf", password="reich")
        self.assertEqual(self.client.get(reverse('accounts.view.self')).status_code, 200)
        adolf.delete()