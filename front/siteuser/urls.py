from django.conf.urls import patterns, url

from siteuser.views import LoginView, LogoutView, UserProfileView, \
    RegisterView, RegisterComplete, UserProfileEdit, \
    RecoverFormView, ConfirmView, AuthorsListView, QuestionFormView

urlpatterns = patterns('',
    url(r'^register/$', RegisterView.as_view(), name='accounts.register'),
    url(r'^complete/$', RegisterComplete.as_view(), name='accounts.complete'),
    url(r'^user/(?P<userid>[0-9]+)/$', UserProfileView.as_view(), name='accounts.view.user'),
    url(r'^profile/$', UserProfileEdit.as_view(), name='accounts.view.self'),
    url(r'^login/$', LoginView.as_view(), name='accounts.view.login'),
    url(r'^logout/$', LogoutView.as_view(), name='accounts.view.logout'),
    url(r'^recover/$', RecoverFormView.as_view(), name='accounts.view.recover'),
    url(r'^confirm/(?P<verkey>[a-z0-9]+)/$', ConfirmView.as_view(), name='accounts.view.confirm'),
    url(r'^list/$', AuthorsListView.as_view(), name='accounts.view.list'),
    url(r'^send_question/$', QuestionFormView.as_view(), name='accounts.question'),
)
