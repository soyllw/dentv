#coding: utf-8
"""
Представления
~~~~~~~~~~~~~
"""
import json
from warnings import warn

from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.views.generic import TemplateView, ListView, CreateView
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.conf import settings
from django.db.models import Q
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, get_object_or_404, HttpResponse
from django.core.mail import send_mail


from utils.cache import cached
from utils.view_utils import TemplateViewMixin
from entries.models import EntryUserRole, Entry
from programs.models import Program, Subscription
from siteuser.models import User
from siteuser.forms import RegisterForm, ProfileEditForm, RecoverForm
from mail.models import EmailTemplate, EmailConfirmation
from qa_app.models import QAItem


def paginate_queryset(qs, request, per_page):
    """Разбивает ``queryset`` на страницы."""
    paginator = Paginator(qs, per_page)
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)
    return objects


def get_aux_stuff(profile):
    """Получает дополнительную информацию о пользователе, для вставки в контекст при отображении профиля"""
    entries = EntryUserRole.objects.filter(user=profile).\
        order_by('entry__program__pk', 'entry__program__created_at').distinct('entry__program')
    guest, host = [], []
    for e in entries:
        if e.role == 1:
            host.append(e.entry.program)
        else:
            guest.append(e.entry.program)

    blog = None
    try:
        blog = Program.objects.get(owner=profile)
    except Program.DoesNotExist:
        pass

    subscriptions = Subscription.objects.filter(user=profile).prefetch_related('program')

    blog_subs, subs = [], []
    subs = subscriptions

    return {"guest": guest, "host": host, "blog": blog, 'subs': subs, "blog_subs": blog_subs}


class RegisterView(TemplateViewMixin, FormView):
    """Представление регистрации пользователя"""
    template_name = 'siteuser/register.html'

    def post(self, request, *args, **kwargs):
        """Обработчик данных, отправленных пользователем при регистрации"""
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            data = register_form.cleaned_data
            email = data.get('email')
            user = User.objects.create_user(
                email=email,
                username=data.get('email'),
                password=data.get('password'))
            user.display_name = data.get('display_name')
            user.generated = False
            user.save()
            try:
                mail_key = settings.EMAIL_REGISTRATION_KEY
                template = EmailTemplate.get(mail_key)
                template.send([email])
            except EmailTemplate.DoesNotExist:
                warn('Email template for registration does not exist')
            except AttributeError:  # no key specified in settings.py
                warn('No key specified for registration emails')
            return redirect('accounts.complete')
        return self.render_to_response(self.get_context_data(form=register_form))

    def get(self, request, *args, **kwargs):
        """Отображает форму регистрации"""
        register_form = RegisterForm()
        return self.render_to_response(self.get_context_data(form=register_form))


class RegisterComplete(TemplateViewMixin, TemplateView):
    """Представление, отображающее страницу успешной регистрации"""
    template_name = 'siteuser/registration_complete.html'


class UserProfileView(TemplateViewMixin, DetailView):
    """Представление, отображающее профиль пользователя"""
    context_object_name = 'profile'
    model = User
    pk_url_kwarg = 'userid'
    template_name = 'siteuser/profile.html'

    def get(self, request, *args, **kwargs):
        """Показ страницы с профилем"""
        self.object = self.get_object()
        aux = get_aux_stuff(self.object)
        if self.object.has_blog:
            aux['entries'] = paginate_queryset(
                Entry.archive.filter(Q(user_roles__user=self.object) | Q(program__owner=self.object)).distinct(),
                request,
                3
            )
        return self.render_to_response(self.get_context_data(**aux))

    def get_context_data(self, **kwargs):
        context = super(UserProfileView, self).get_context_data(**kwargs)
        context['top_entries'] = cached(Entry.get_top_blog_entries, 'top_entries', 60 * 10)
        try:
            qa_list = QAItem.objects.filter(siteuser=self.object, approved=True).exclude(answer_text='').order_by('email').all()
        except:
            qa_list = []
        context['qa_list'] = qa_list
        context['question_count'] = qa_list.count() #FIXME:TEMPORARY
        context['entries_count'] = Entry.archive.filter(Q(user_roles__user=self.object) | Q(program__owner=self.object)).count()
        context['tag'] = '#' + self.object.display_name
        # TODO:FXIME
        last_entryies = Entry.archive.filter(Q(user_roles__user=self.object) | Q(program__owner=self.object)).order_by('-id')[:4]
        context['last_entryies'] = last_entryies
        return context


class UserProfileEdit(TemplateViewMixin, TemplateView):
    """Представление, отображающее форму редактирования своего профиля"""
    template_name = 'siteuser/account_data.html'

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """Получение изменений и их запись в базу"""
        profile = request.user
        form = ProfileEditForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data

            old_pass = data.get('old_password')
            new_pass = data.get('new_password')
            if old_pass and new_pass:
                if request.user.check_password(old_pass):
                    request.user.set_password(new_pass)
                    request.user.save()
                else:
                    form.errors["old_password"] = u"Введен неверный пароль"

            user_changed = False
            for k in ["first_name", "last_name"]:
                if data.get(k) != getattr(request.user, k, None):
                    setattr(request.user, k, data.get(k))
                    user_changed = True
            if user_changed:
                request.user.save()

            profile_changed = False
            for k in ["location", "occupation", "about", "dob", "display_name"]:
                if data.get(k) != getattr(profile, k, None):
                    setattr(profile, k, data.get(k))
                    profile_changed = True
            if profile_changed:
                profile.save()

            if request.FILES:
                avatar = request.FILES["avatar"]
                if avatar:
                    profile.avatar_source = avatar
                    profile.save()

        aux = get_aux_stuff(profile)
        return self.render_to_response(self.get_context_data(profile=profile, form=form, **aux))

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        """Отображение формы редактирования своего профиля"""
        profile = request.user
        profile_dict = {}
        for k in ["gender", "location", "occupation", "about", "dob", "display_name"]:
            profile_dict.update({k: getattr(profile, k, None)})
        for k in ["last_name", "first_name"]:
            profile_dict.update({k: getattr(request.user, k, None)})
        form = ProfileEditForm(profile_dict)

        aux = get_aux_stuff(profile)

        return self.render_to_response(self.get_context_data(profile=profile, form=form, **aux))


class SelfProfileView(RedirectView):
    """Перенаправление на свой профиль."""

    def get_redirect_url(self, **kwargs):
        """Ссылка на свой профиль для аутентифицированного пользователя"""
        return self.request.user.get_absolute_url()

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """Перенаправление. Перегружено в этом классе, чтобы добавить декоратор ``login_required``"""
        return super(SelfProfileView, self).dispatch(request, *args, **kwargs)


class LoginView(TemplateViewMixin, TemplateView):
    """Представление, отвечающее за аутентификацию пользователей"""
    template_name = "siteuser/login.html"

    def post(self, request, *args, **kwargs):
        """Получение данных из формы логина"""
        name     = request.POST.get('login', None)
        password = request.POST.get('password', None)
        next     = request.GET.get('next', request.POST.get('next', settings.LOGIN_REDIRECT_URL))
        user     = None
        if name != '' and password != '':
            user = authenticate(username=name, password=password)
            if user is None:
                try:
                    user = User.objects.get(email=name)
                    user = authenticate(username=user.username, password=password)
                except User.DoesNotExist:
                    pass
            if user is not None and user.is_active:
                login(request, user)
        if user is not None and user.is_authenticated():
            return redirect(next)
        return self.render_to_response(self.get_context_data(error=u'Такие логин и пароль не подходят. Возможно, вы ошиблись.', username=name, next=next))

    def get(self, request, *args, **kwargs):
        """Отображение формы логина"""
        return super(LoginView, self).get(request, *args, **kwargs)


class LogoutView(RedirectView):
    """Представление для выхода из системы"""
    url = settings.LOGIN_REDIRECT_URL

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).dispatch(request, *args, **kwargs)


class RecoverFormView(TemplateViewMixin, FormView):
    """Представление для восстановление утерянного пароля"""
    template_name = 'siteuser/recover_form.html'

    def post(self, request, *args, **kwargs):
        """Обработчик данных, присланных пользователем. В случае успеха отсылает пользователю письмо, содержащее ссылку-подтверждение"""
        recover_form = RecoverForm(request.POST)
        if recover_form.is_valid():
            data = recover_form.cleaned_data
            email = data.get('email')
            user = User.objects.get(email=email)
            confirm = EmailConfirmation.create_confirmation(user)
            try:
                mail_key = settings.EMAIL_RECOVERY_KEY
                template = EmailTemplate.get(mail_key)
                site = Site.objects.get(pk=1)
                template.send([email], data={'recovery_url': 'http://' + site.domain + reverse('accounts.view.confirm', kwargs={'verkey': confirm.verkey})})
            except EmailTemplate.DoesNotExist:
                warn('Email template for recovery does not exist')
            except AttributeError:  # no key specified in settings.py
                warn('No key specified for recovery emails')
            self.template_name = 'siteuser/recover_sent.html'
        return self.render_to_response(self.get_context_data(form=recover_form))

    def get(self, request, *args, **kwargs):
        """Отображение формы восстановления пароля"""
        recover_form = RecoverForm()
        return self.render_to_response(self.get_context_data(form=recover_form))


class ConfirmView(TemplateViewMixin, TemplateView):
    """Представление для подтверждения действия (общий механизм, в данном случае используется для подтверждения сброса пароля"""
    template_name = 'siteuser/recover_finish.html'

    def get(self, request, *args, **kwargs):
        """Переход по ссылке из письма - подтверждение действия"""
        verkey = kwargs['verkey']
        confirmation = get_object_or_404(EmailConfirmation, verkey=verkey)
        new_pass = User.objects.make_random_password()
        try:
            mail_key = settings.EMAIL_PASSWORD_KEY
            template = EmailTemplate.get(mail_key)
            template.send([confirmation.user.email], data={'password': new_pass})
            user = confirmation.user
            user.set_password(new_pass)
            user.save()
            confirmation.delete()
        except EmailTemplate.DoesNotExist:
            warn('Email template for recovery does not exist')
        except AttributeError:  # no key specified in settings.py
            warn('No key specified for recovery emails')
        return self.render_to_response(self.get_context_data())


class AuthorsListView(TemplateViewMixin, ListView):
    """Страница, отображающая список авторов"""

    template_name = 'siteuser/list.html'
    queryset = User.objects.filter(has_blog=True).exclude(pk=1)
    context_object_name = 'profiles'
    paginate_by = 18


class QuestionFormView(CreateView):
    def post(self, request, *args, **kwargs):
        if self.request.is_ajax():
            fio = self.request.POST.get('fio') or ''
            email = self.request.POST.get('email') or ''
            question = self.request.POST.get('question') or ''
            user_id = self.request.POST.get('user') or ''
            user = User.objects.get(pk=int(user_id)) or 0
            item = QAItem()
            item.siteuser = user
            item.email = email
            item.question_text = question
            item.fio = fio
            item.save()
            to_json_response = dict()
            to_json_response['result'] = True
            send_mail(
                    'Question', 'Here is the question.\n %s' % (question),
                    'admin@example.com', ['to@example.com'],
                    fail_silently=False
                    )

            return HttpResponse(json.dumps(to_json_response), content_type='application/json')
        else:
            return HttpResponse()
