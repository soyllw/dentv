#coding: utf-8
import uuid
import datetime

#from social_auth.backends import USERNAME


def create_or_update_profile(backend, details, user, *args, **kwargs):
    #adapted from social_auth.backends.pipeline.user
    profile = user  
    if profile.generated:
        if user.first_name and user.last_name:
            profile.display_name = u"{0} {1}".format(user.first_name, user.last_name)
        elif user.first_name:
            profile.display_name = u"{0}".format(user.first_name)
        elif user.last_name:
            profile.display_name = u"{0}".format(user.last_name)
        #elif details.get(USERNAME):
        #    profile.display_name = details.get(USERNAME)
        elif user.username:
            profile.display_name = u"{0}".format(user.username)
        else:
            profile.display_name = uuid.uuid1().hex
        profile.dob = datetime.datetime.now()
        profile.generated = False
        profile.save()
    if profile.update_value:
        changed = False
        for name, value in details.iteritems(): #TODO: change this to custom-tailored fields that correspond to the profile keys
            if name in ('id', 'pk',):
                continue
            if value and value != getattr(profile, name, None):
                setattr(profile, name, value)
                changed = True
        if changed:
            profile.save()
    else:
        return {}