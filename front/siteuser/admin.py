#coding: utf-8
from django.contrib import admin

from siteuser.models import User


class UserAdmin(admin.ModelAdmin):
    list_fields = (
      'username', 'first_name', 'last_name', 
      'is_staff', 'is_active', 'is_superuser', 
      'has_blog'
    )
    search_fields = ('username', 'first_name', 'last_name', 'display_name', 'email')

    fieldsets = (
        (None, {
            'fields': ['username', 'first_name', 'last_name', 'display_name', 'email', 'avatar_source',
                       'avatar_personal_source', 'position']
        }),
        (u'Дополнительные данные', {
            'fields': ['dob', 'about', 'occupation', 'location', 'gender', 'is_active_email', 'person_email']
        }),
        (u'Роли пользователя', {
            'fields': ['is_active', 'is_superuser', 'is_staff', 'has_blog']
        }),
        (u'Служебная информация', {
            'fields': ['update_value', 'last_login', 'date_joined']
        }),
        (u'Права', {
            'fields': ['groups', 'user_permissions']
        }),
    )

    model = User

    filter_horizontal = ('groups', 'user_permissions')

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.is_active_email != True:
            return self.readonly_fields + ('person_email', )
        return self.readonly_fields

admin.site.register(User, UserAdmin)
