#-*- coding: utf-8 -*-
"""
Модели
~~~~~~"""
from datetime import datetime

from django.db import models
from django.contrib.auth.models import User as OldUser, UserManager

from imagekit.models import ImageSpecField
from imagekit.processors.resize import ResizeToFit
from imagekit.processors import ResizeToFill


def filter_alphanum(instr):
    return [ch for ch in instr if ch.isalnum()]


class LooksLikeUserManager(models.Manager):
    """Менеджер объектов-пользователей."""

    def create_user(self, username, email=None, password=None):
        """Создает нового пользователя. Механизм необходим для создания новых пользователей в модели ``siteuser.models.User``.З"""
        now = datetime.now()
        if not username:
            raise ValueError('The given username must be set')
        email = super(LooksLikeUserManager, self).normalize_email(email)
        user = self.model(username=username, email=email,
                          is_staff=False, is_active=True, is_superuser=False,
                          last_login=now, date_joined=now)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        """Создает суперпользователя"""
        superuser = self.create_user(username, email, password)
        superuser.is_staff = True
        superuser.is_active = True
        superuser.is_superuser = True
        superuser.save(using=self._db)
        return superuser


class NoFirstUserManager(models.Manager):
    """Менеджер, не выгружающий пользователя с ``id=1`` (служебного суперпользователя)."""

    def get_query_set(self):
        return super(NoFirstUserManager, self).get_query_set().exclude(pk=1)


class User(OldUser):
    """Модель пользователей"""

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    objects = UserManager()
    common = NoFirstUserManager()

    GENDER = (
        (1, u'Мужской'),
        (2, u'Женский')
    )

    position = models.SmallIntegerField(verbose_name=u"Позиция", choices=tuple((i, i) for i in range(1, 30)), blank=True, null=True)

    display_name = models.CharField(max_length=200, verbose_name=u"Отображаемое имя пользователя")
    dob = models.DateField(verbose_name=u'Дата рождения', blank=True, null=True)
    gender = models.IntegerField(choices=GENDER, verbose_name=u'Пол', default=1)
    location = models.CharField(max_length=100, verbose_name=u'Страна, город', blank=True)
    occupation = models.CharField(max_length=200, verbose_name=u'Род занятий', blank=True)
    about = models.TextField(verbose_name=u'О себе', blank=True)
    activation = models.CharField(max_length=32, editable=False, verbose_name=u'Код активации', blank=True)
    avatar_source = models.ImageField(
        verbose_name=u"Аватар для главной",
        upload_to=lambda ins, fname: u'avatar/%s/%s' % ('/'.join([x for x in filter_alphanum(ins.display_name)][0:2]), fname),
        blank=True
    )

    avatar_personal_source = models.ImageField(
        verbose_name=u"Аватар для личной страницы",
        upload_to=lambda ins, fname: u'personal_avatar/%s/%s' % (
        '/'.join([x for x in filter_alphanum(ins.display_name)][0:2]), fname),
        blank=True,
        null=True
    )

    avatar_personal_240 = ImageSpecField([ResizeToFit(240)], source='avatar_personal_source', format='JPEG')
    avatar_240 = ImageSpecField([ResizeToFit(240)], source='avatar_source', format='JPEG')
    avatar_150 = ImageSpecField([ResizeToFit(150, 88)], source='avatar_source', format='JPEG')
    avatar_50 = ImageSpecField([ResizeToFit(50, 50)], source='avatar_source', format='JPEG')
    update_value = models.BooleanField(default=True, verbose_name=u'Обновлять данные из аккаунта в соц. сети')
    has_blog = models.BooleanField(default=False, verbose_name=u'Ведет блог')
    created = models.DateField(verbose_name=u"Дата регистрации", auto_now_add=True, null=True)
    generated = models.BooleanField(default=True, verbose_name=u"Аккаунт не был обновлен из соцсетей")

    last_comment_date = models.DateField(verbose_name=u"Дата последнего комментария", blank=True, null=True, editable=False)
    comments_count = models.IntegerField(verbose_name=u"Всего комментариев", default=0, editable=False)
    person_email = models.EmailField(verbose_name=u"Электронный адрес", null=True, blank=True)
    is_active_email = models.NullBooleanField(verbose_name=u"Электронный адрес активен", null=True, blank=True)

    enabled = models.BooleanField(verbose_name=u"Отображать на главной?", null=False, blank=False, default=False)

    def __unicode__(self):
        return u'%s' % self.display_name

    @models.permalink
    def get_absolute_url(self):
        """Получить ссылку на профиль пользователя"""
        return ('accounts.view.user', (), {'userid': str(self.pk)})

    @property
    def screen_name(self):
        """Получить отображаемое имя пользователя"""
        if self.display_name == '' or self.display_name is None:
            return self.user.get_full_name()
        return self.display_name

    @property
    def comments_rating(self):
        """Получить рейтинг всех комментариев пользователя"""
        return self.comments.aggregate(models.Sum('rating')).get('rating__sum')

    def save(self, *args, **kwargs):
        """Сохранить пользователя в базе данных."""
        if not self.username or self.username == '':
            if self.email is not None and self.email != '':
                self.username = self.email
            else:
                self.username = self.display_name
        super(User, self).save(*args, **kwargs)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "display_name__icontains",)

#OldUser._meta.get_field("date_joined").editable = False
#OldUser._meta.get_field("last_login").editable = False
#OldUser._meta.get_field("password").editable = False
#OldUser._meta.get_field("username").editable = False

import siteuser.signals
