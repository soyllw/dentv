from django.db.models.signals import post_save, post_delete
from django.core.cache import cache

from siteuser.models import User


def drop_profiles_cache(*args, **kwargs):
    cache.delete('top-bloggers')

post_save.connect(drop_profiles_cache, sender=User, dispatch_uid='siteuser.signals.post_save_drop_cache')
post_delete.connect(drop_profiles_cache, sender=User, dispatch_uid='siteuser.signals.post_delete_drop_cache')
