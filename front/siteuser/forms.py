# coding: utf-8
from datetime import datetime 

from django import forms
from django.db.models import Q
from django.contrib.auth.models import User
from django.forms.extras.widgets import SelectDateWidget

from siteuser.models import User
from siteuser.fields import DualPasswordField


class RegisterForm(forms.Form):
    display_name = forms.CharField(max_length=255, label=u"Ваше имя")
    email = forms.EmailField(label=u"Электронная почта", error_messages={'invalid': u'Введите правильный адрес электронной почты'})
    password = DualPasswordField(label=u"Пароль")

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        email = cleaned_data.get("email")
        if User.objects.filter(Q(username=email) | Q(email=email)).count() > 0:
            self._errors["email"] = self.error_class([u'Такую электронную почту использовать, к сожалению, нельзя'])
        return cleaned_data


class ProfileEditForm(forms.Form):
    first_name = forms.CharField(max_length=255, label=u"Имя", required=False)
    last_name = forms.CharField(max_length=255, label=u"Фамилия", required=False)
    display_name = forms.CharField(max_length=255, label=u"Отображаемое имя", required=False)
    old_password = forms.CharField(label=u"Старый пароль", widget=forms.PasswordInput, required=False)
    new_password = forms.CharField(label=u"Новый пароль", widget=forms.PasswordInput, required=False)
    gender = forms.ChoiceField(label=u'Пол', widget=forms.RadioSelect(), choices=User.GENDER, required=False)
    location = forms.CharField(max_length=255, label=u"Местонахождение", required=False)
    occupation = forms.CharField(max_length=255, label=u"Место работы", required=False)
    about = forms.CharField(max_length=8192, label=u"О себе", widget=forms.Textarea, required=False)
    dob = forms.DateField(label=u'Дата рождения', widget=SelectDateWidget(years=range(1900, datetime.now().year - 15)), required=False)
    avatar = forms.FileField(label=u"Аватар", required=False)


class RecoverForm(forms.Form):
    email = forms.EmailField(label=u"Электронная почта")

    def clean(self):
        cleaned_data = super(RecoverForm, self).clean()
        email = cleaned_data.get("email")
        if User.objects.filter(Q(email=email)).count() < 1:
            raise forms.ValidationError(u'На этот адрес электронной почты не зарегистрирован ни один аккаунт')
        return cleaned_data