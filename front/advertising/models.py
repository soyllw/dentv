# -*- coding: utf-8 -*-
from django.db import models


class AdBanner(models.Model):
    """Рекламный баннер."""
    code = models.TextField(max_length=5000, blank=True, verbose_name=u'Код для вставки')
    POSITIONS = (
        (1, u'Слева'),
        (2, u'Справа'),
    )

    position = models.IntegerField(choices=POSITIONS, verbose_name=u'Позиция', default=1)
    active = models.BooleanField(blank=False, default = False, verbose_name=u'Отображать')

    class Meta:
        verbose_name = u'Реклама'
        verbose_name_plural = u'Реклама'

    @classmethod
    def get_next(cls, current_id):  # current_id is the id of current record
        try:
            return cls.objects.filter(id__gt=current_id).order_by("id")[0]

        except:
            return None


    @classmethod
    def get_previous(cls, current_id):
        try:
            return cls.objects.filter(id__lt=current_id).order_by("-id")[0]
        except:
            return None