#coding: utf-8
"""
Модели
~~~~~~"""
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models

from siteuser.models import User

from voting.managers import VoteManager

SCORES = (
    (+1, u'+1'),
    (-1, u'-1'),
)

class Vote(models.Model):
    "Модель голосов пользователей"
    user         = models.ForeignKey(User, related_name = 'voted_users', verbose_name=u'Пользователь')
    content_type = models.ForeignKey(ContentType, related_name='votes')
    object_id    = models.PositiveIntegerField()
    object       = generic.GenericForeignKey('content_type', 'object_id')
    vote         = models.SmallIntegerField(choices=SCORES, verbose_name=u'Значение голоса')

    objects = VoteManager()

    class Meta:
        db_table = 'votes'
        # One vote per user per object
        unique_together = (('user', 'content_type', 'object_id'),)
        verbose_name = u'Оценка пользователя'
        verbose_name_plural = u'Оценки пользователей'


    def __unicode__(self):
        return u'%s поставил %d объекту %s "%s"' % (self.user, self.vote, self.content_type, self.object)

    def is_upvote(self):
        "Является ли голос голосом за"
        return self.vote == 1

    def is_downvote(self):
        "Является ли голос голосом против"
        return self.vote == -1
