from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url('^vote/(?P<ct>[0-9]+)/(?P<oi>[0-9]+)/(?P<direction>(up|down))/$', 'voting.views.vote', name='voting.vote'),
)