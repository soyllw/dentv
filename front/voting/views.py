#coding: utf-8
"""
Представления
~~~~~~~~~~~~~"""
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect
from django.utils.simplejson import simplejson
from django.http import HttpResponse

from utils.models import Rateable

from voting.models import Vote


@login_required
def vote(request, ct, oi, direction):
    "Проголосовать за объект"
    vote = {'up': 1, 'down': -1}[direction]
    ct = get_object_or_404(ContentType, pk=ct)
    model = ct.model_class()
    obj = get_object_or_404(model, pk=oi)
    try:
        v = Vote.objects.get(user=request.user, content_type=ct, object_id=oi)
        created = False
    except Vote.DoesNotExist:
        v = Vote(user=request.user, content_type=ct, object_id=oi)
        created = True
    if v.vote != vote:
        v.vote = vote
        v.save()
        if isinstance(obj, Rateable):
            if created:
                obj.rating += vote
            else:
                obj.rating += vote + vote
            # Save or update?
            obj.save()
    if request.is_ajax():
        return HttpResponse(simplejson.dumps({'rating': obj.rating}), content_type='appliaction/json')
    return redirect(obj.get_absolute_url())
