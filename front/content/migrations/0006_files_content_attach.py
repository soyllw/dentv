# -*- coding: utf-8 -*-
import datetime
import glob
import os.path
import urlparse

from south.db import db
from south.v2 import DataMigration
from django.db import models

from django.conf import settings

class Migration(DataMigration):

    def compile_part(self, part, orm):
        if isinstance(part, orm['content.YoutubeVideo']):
            content_parts = urlparse.urlparse(part.content)
            video_id = urlparse.parse_qs(content_parts.query).get('v')[0].strip()
            #return """<iframe class="video" type="text/html" width="640" height="360" src="http://youtube.com/embed/{0}?html5=1" frameborder="0" allowfullscreen></iframe>""".format(video_id)
            return """<div class="dentv-video-player" data-source="youtube" data-video-id="{0}"></div>""".format(video_id)
        elif isinstance(part, orm['content.DentvVideo']):
            pt = os.path.relpath(part.content, settings.VIDEO_ROOT)
            pt = os.path.join(settings.VIDEO_URL, pt)
            return """<div class="dentvplayer"><video src="{0}"></video></div>""".format(pt)
        else:
            return part.content

    def compile_entry(self, entry, orm):
        """
        Компилирует выпуск из имеющихся в БД частей различного рода, сортируя их в соответствии с атрибутом `position`.
        Скомпилированное содержимое выпуска помещается в поле `content` объекта.
        """
        parts = []
        for kind in ['content.YoutubeVideo', 'content.HtmlText', 'content.DentvVideo']:
            for part in orm[kind].objects.filter(entry=entry, enabled=True).all():
                parts.append(part)
        parts = sorted(parts, key=lambda x: x.position)
        entry.content = ''.join(map(lambda x: self.compile_part(x, orm), parts))
        entry.save()

    def get_files_hash(self):
        files = {}
        root_path = settings.VIDEO_ROOT
        for pt in glob.iglob(os.path.join(root_path, "*/*/*/*.*")): #match files like 2012/2/22/youtubeid.mp4
            ident, _ = os.path.splitext(os.path.split(pt)[1])
            files[ident] = pt
        print "got {0} files in VIDEO_ROOT\n".format(len(files.keys()))
        return files          

    def get_youtube_id(self, ytv): #workaround for south not being able to get custom model methods
        content = urlparse.urlparse(ytv.content)
        return urlparse.parse_qs(content.query).get('v')[0].strip()

    def forwards(self, orm):
        "Write your forwards methods here."
        videos_hash = self.get_files_hash()
        keys = videos_hash.keys()
        for ytv in orm['content.YoutubeVideo'].objects.all():
            vid = self.get_youtube_id(ytv)
            if vid in keys:
                path = videos_hash[vid]
                print "video {0} found in files_hash, path {1}\n".format(ytv.content, path)
                try:
                    dentv_video = orm['content.DentvVideo'].objects.get(content=path, entry=ytv.entry)
                    dentv_video.enabled = True
                    dentv_video.save()
                except orm['content.DentvVideo'].DoesNotExist:
                    orm['content.DentvVideo'].objects.create(entry=ytv.entry, position=ytv.position, enabled=ytv.enabled, content=path)
                ytv.enabled = False
                ytv.save()
                self.compile_entry(ytv.entry, orm)
        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."

    def backwards(self, orm):
        for video in orm['content.DentvVideo'].objects.all():
            ident, _ = os.path.splitext(os.path.split(video.content)[1])
            youtubes = orm['content.YoutubeVideo'].objects.filter(content__contains=ident, entry=video.entry, position=video.position)
            if len(youtubes) > 0:
                youtubes.update(enabled=True)
                video.enabled = False
                video.save()
                self.compile_entry(video.entry, orm)

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'content.dentvvideo': {
            'Meta': {'object_name': 'DentvVideo'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'content.htmltext': {
            'Meta': {'ordering': "('position',)", 'object_name': 'HtmlText'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'content.markdowntext': {
            'Meta': {'ordering': "('position',)", 'object_name': 'MarkdownText'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'content.youtubevideo': {
            'Meta': {'ordering': "('position',)", 'object_name': 'YoutubeVideo'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'entries.entry': {
            'Meta': {'ordering': "('-released_at', 'id')", 'object_name': 'Entry'},
            'comments_allowed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comments_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'program': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': u"orm['programs.Program']"}),
            'rating': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'released_at': ('django.db.models.fields.DateTimeField', [], {}),
            'search_index': ('djorm_pgfulltext.fields.VectorField', [], {'default': "''", 'null': 'True', 'db_index': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '1024', 'populate_from': "'title'", 'unique_with': '()'}),
            'tags': ('djorm_pgarray.fields.ArrayField', [], {'default': 'None', 'dbtype': "'text'", 'null': 'True', 'blank': 'True'}),
            'thumb_source': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'views_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'})
        },
        u'entries.entryuserrole': {
            'Meta': {'ordering': "('role', 'position')", 'object_name': 'EntryUserRole'},
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_roles'", 'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'role': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'role_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': u"orm['siteuser.User']"})
        },
        u'entries.question': {
            'Meta': {'object_name': 'Question'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questions'", 'to': u"orm['siteuser.User']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'questions'", 'to': u"orm['entries.Entry']"}),
            'fio': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reply': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Question']", 'null': 'True', 'blank': 'True'}),
            'support': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'entries.questionsupport': {
            'Meta': {'object_name': 'QuestionSupport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'supports'", 'to': u"orm['entries.Question']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'question_supports'", 'null': 'True', 'to': u"orm['siteuser.User']"})
        },
        u'programs.program': {
            'Meta': {'ordering': "('-created_at', 'id')", 'object_name': 'Program'},
            'comments_allowed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comments_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'on_top': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'programs'", 'to': u"orm['siteuser.User']"}),
            'rating': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '1024', 'populate_from': "'title'", 'unique_with': '()'}),
            'subscribers_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'views_count': ('django.db.models.fields.BigIntegerField', [], {'default': '0'})
        },
        u'siteuser.user': {
            'Meta': {'object_name': 'User', '_ormbases': [u'auth.User']},
            'about': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'activation': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'avatar_source': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'comments_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'dob': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'generated': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'has_blog': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_comment_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'occupation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'update_value': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['entries', 'content']
    symmetrical = True
