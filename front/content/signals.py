#coding: utf-8
from django.db.models.signals import post_save, post_delete
from django.db.models.loading import get_models

#from entries.models import Entry

def recompile_entry(sender, instance, *args, **kwargs):
    from entries.models import Entry # circular import >_<
    try:
        content = instance.entry.compile()
        Entry.objects.filter(pk=instance.entry.pk).update(content=content)
    except Entry.DoesNotExist: #the entry does not exist anymore
        pass

# FIXME:
# do we REALLY need to recompile here, since user has no way to change content part
# not changing entry

def attach_signals(model):
    post_save.connect(recompile_entry, sender=model, dispatch_uid='content.signals.post_save_recompile')
    post_delete.connect(recompile_entry, sender=model, dispatch_uid='content.signals.post_delete_recompile')
