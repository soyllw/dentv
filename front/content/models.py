#coding: utf-8
"""
~~~~~~
Типы контента
~~~~~~
"""
import os.path

from django.db import models
from django.conf import settings
import markdown
import urlparse

import utils.models as utils
# import entries.models as entries


class YoutubeVideo(utils.ContentModel):

    __content_parts = None
    is_video = True

    class Meta:
        verbose_name = u'Видео-объект YouTube'
        verbose_name_plural = u'Видео-объект YouTube'
        ordering = ('position',)

    def __parse_content(self):
        try:
            if self.__content_parts is None:
                self.__content_parts = urlparse.urlparse(self.content)
        except AttributeError:
            pass
        return self.__content_parts

    def get_video_id(self):
        """
        Возвращает ``VIDEO_ID`` для API YouTube по ссылке.
        """
        try:
            self.__parse_content()
            video_id = urlparse.parse_qs(self.__content_parts.query).get('v')[0].strip()
        except AttributeError:
            video_id = ''
        return video_id

    def compile(self):
        return """<iframe class="video" type="text/html" width="1049" height="590" """ \
                """src="http://youtube.com/embed/{0}?html5=1" """ \
                """frameborder="0" allowfullscreen></iframe>""".format(self.get_video_id(),)

    def __unicode__(self):
        return u'%s' % self.content


class HtmlText(utils.ContentModel):
    is_transcript = models.BooleanField(default=True, verbose_name=u'Транскрипт?')

    class Meta:
        verbose_name = u'Текст в формате HTML'
        verbose_name_plural =u'Текст в формате HTML'
        ordering = ('position',)

    def compile(self):
        content = self.content
        if self.is_transcript:
            content = u'<div itemprop="transcript">%s</div>' % content
        return content


class MarkdownText(utils.ContentModel):
    is_transcript = models.BooleanField(default=True, verbose_name=u'Транскрипт?')

    class Meta:
        verbose_name = u"Текст в формате Markdown"
        verbose_name_plural = u"Текст в формате Markdown"
        ordering = ('position',)

    def compile(self):
        content = markdown.markdown(self.content)
        if self.is_transcript:
            content = u'<div itemprop="transcript">%s</div>' % content
        return content


from content.signals import attach_signals

attach_signals(YoutubeVideo)
attach_signals(HtmlText)
attach_signals(MarkdownText)
