from django.db import models


class Token(models.Model):
    SERVICES = (
        (1, u'YouTube'),
        (2, u'Facebook'),
        (3, u'Vkontakte')
    )
    service       = models.IntegerField(choices=SERVICES)
    client_id     = models.CharField(max_length = 128)
    api_key       = models.CharField(max_length = 255, blank=True)
    token         = models.CharField(max_length = 255, blank=True)
    refresh_token = models.CharField(max_length = 255, blank=True)

    def __unicode__(self):
        return self.get_service_display()
