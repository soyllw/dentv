# -*- coding: utf-8 -*-
from django.db import models
from django.core.validators import URLValidator


class Book(models.Model):
    """Книжный магазин."""
    name = models.CharField(max_length=32, blank=True, null=True, default=None, verbose_name=u'Название')
    image = models.ImageField(upload_to='books', verbose_name=u'Изображение')
    price = models.DecimalField(max_digits=6, decimal_places=2, verbose_name=u'Цена')
    active = models.BooleanField(blank=False, default=False, verbose_name=u'Отображать')
    link = models.TextField(validators=[URLValidator()], verbose_name=u'Ссылка')

    class Meta:
        verbose_name = u'Книги'
        verbose_name_plural = u'Книги'


    def __unicode__(self):
        if self.name:
            return self.name
        else:
            return 'Книга'
