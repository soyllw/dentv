from django.conf.urls import patterns, url

from qa_app.views import QAListView

urlpatterns = patterns('',
  url(r'^$', QAListView.as_view(), name='qa_app.list'),
)
