from django.contrib import admin

from qa_app.models import QAItem


admin.site.register(QAItem)
