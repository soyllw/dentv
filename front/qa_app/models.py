# -*- coding: utf-8 -*-
from django.db import models

from siteuser.models import User


class QAItem(models.Model):

    class Meta:
        verbose_name = u'Вопрос-ответ'
        verbose_name_plural = u'Вопросы-ответы'
        ordering = ('-created_at', 'email')


    siteuser = models.ForeignKey(User, related_name='qa_app', verbose_name=u'Кому задан вопрос')
    created_at = models.DateTimeField(verbose_name=u'Время создания', auto_now_add=True)
    email = models.EmailField(verbose_name=u'Email', blank=False, null=True)
    fio = models.CharField(max_length=255, verbose_name=u'ФИО', blank=False, null=True)
    question_text = models.TextField(verbose_name=u'Текст вопроса', blank=False, null=True)
    answer_text = models.TextField(verbose_name=u'Текст ответа', blank=True, null=True)

    approved = models.BooleanField(default=False, verbose_name=u'Подтверждено')

    def __unicode__(self):
        return '%s | %s | %s' % (self.created_at, self.fio, self.email)
