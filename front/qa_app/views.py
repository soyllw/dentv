# coding: utf-8

from django.views.generic import ListView
from utils.view_utils import TemplateViewMixin

from qa_app.models import QAItem


class QAListView(TemplateViewMixin, ListView):
    """
    Список вопрос-ответов публицистам
    """
    template_name = 'qa_app/list.html'

    def get_queryset(self):
        return QAItem.objects.exclude(answer_text='').order_by('email','-created_at').all()