#coding: utf-8

from fabric.api import *

try:
    from local_fabfile import *
except ImportError:
    pass


def test():
    "Test task"
    local("./manage.py test")  # TODO: system-independent way (broken on windows now)


@hosts('173.45.226.135')
def deploy(dropdb=False):
    """
    Функция развёртывания кодовой базы проекта на сервере.
    Производит выгрузку проекта из git, собирает ассеты, перезапускает сервер.
    В режиме debug делает всё тоже самое, но для поддомена dev.
    """
    remote_dir = '/home/dentv/DenTv'
    remote_dev_dir = '/home/dentv/dev/DenTv'
    media_dir = '/srv/dentv/media'
    with settings(warn_only=True):
        if sudo("test -d %s" % remote_dir, user="dentv").failed:
            sudo("git clone git://github.com/H359/DenTv.git %s" % remote_dir, user="dentv")
            with cd(remote_dir + '/front/front/'):
                sudo("ln -s ../../../local_settings.py", user="dentv")
    with cd(remote_dir):
        sudo("git pull", user="dentv")  # pull the repo
    with cd(remote_dir + '/assets'):
        sudo("./build.py --mode=prod", user="dentv")
    with cd(remote_dir + '/front'):
        with settings(warn_only=True):
            if sudo("test -d %s" % media_dir, user="dentv").failed:
                sudo("mkdir %s" % media_dir, user="dentv")  # create media dir
            #with cd(media_dir):
            #    sudo("tar -xf %s/front/ava.tar avatar" % remote_dir, user="dentv")
            with cd(remote_dir + '/front/admin/admin'):
                sudo("brunch b --minify", user="dentv")
            sudo("python2 manage.py collectstatic --noinput", user="dentv")
        """
        if dropdb:
            sudo("dropdb dentv")
            sudo("createdb dentv")
            sudo("python2 manage.py syncdb", user="dentv")
        """
        with settings(warn_only=True):  # stop gunicorn server
            sudo("killall gunicorn")
        sudo("gunicorn -D front.wsgi:application --workers=4", user="dentv")
    with cd(remote_dev_dir + '/front'):
        sudo("gunicorn -D front.wsgi:application", user="dentv")
