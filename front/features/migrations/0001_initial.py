# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FeatureType'
        db.create_table(u'features_featuretype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(default=1.0, max_digits=8, decimal_places=2)),
        ))
        db.send_create_signal(u'features', ['FeatureType'])

        # Adding model 'Feature'
        db.create_table(u'features_feature', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['features.FeatureType'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('is_dirty', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'features', ['Feature'])

        # Adding model 'FeatureFrequency'
        db.create_table(u'features_featurefrequency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('feature', self.gf('django.db.models.fields.related.ForeignKey')(related_name='frequencies', to=orm['features.Feature'])),
            ('document_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('frequency', self.gf('django.db.models.fields.DecimalField')(default=1.0, max_digits=9, decimal_places=8)),
            ('relative_weight', self.gf('django.db.models.fields.DecimalField')(default=1.0, max_digits=8, decimal_places=2)),
        ))
        db.send_create_signal(u'features', ['FeatureFrequency'])

        # Adding model 'FeatureInDocument'
        db.create_table(u'features_featureindocument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('feature', self.gf('django.db.models.fields.related.ForeignKey')(related_name='documents', to=orm['features.Feature'])),
            ('document_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('document_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'features', ['FeatureInDocument'])

        # Adding unique constraint on 'FeatureInDocument', fields ['feature', 'document_type', 'document_id']
        db.create_unique(u'features_featureindocument', ['feature_id', 'document_type_id', 'document_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'FeatureInDocument', fields ['feature', 'document_type', 'document_id']
        db.delete_unique(u'features_featureindocument', ['feature_id', 'document_type_id', 'document_id'])

        # Deleting model 'FeatureType'
        db.delete_table(u'features_featuretype')

        # Deleting model 'Feature'
        db.delete_table(u'features_feature')

        # Deleting model 'FeatureFrequency'
        db.delete_table(u'features_featurefrequency')

        # Deleting model 'FeatureInDocument'
        db.delete_table(u'features_featureindocument')


    models = {
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'features.feature': {
            'Meta': {'object_name': 'Feature'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_dirty': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['features.FeatureType']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '512'})
        },
        u'features.featurefrequency': {
            'Meta': {'object_name': 'FeatureFrequency'},
            'document_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'feature': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'frequencies'", 'to': u"orm['features.Feature']"}),
            'frequency': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '9', 'decimal_places': '8'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'relative_weight': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '8', 'decimal_places': '2'})
        },
        u'features.featureindocument': {
            'Meta': {'unique_together': "(('feature', 'document_type', 'document_id'),)", 'object_name': 'FeatureInDocument'},
            'document_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'document_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'feature': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'documents'", 'to': u"orm['features.Feature']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'features.featuretype': {
            'Meta': {'object_name': 'FeatureType'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '8', 'decimal_places': '2'})
        }
    }

    complete_apps = ['features']