#-*- coding: utf-8 -*-
from decimal import Decimal

from celery import task

from features.models import Feature, FeatureFrequency, FeatureInDocument


@task(name='features.tasks.weight_features')
def weight_features():
  for feature in Feature.objects.filter(is_dirty=True)[0:50]:
    for frequency in feature.frequencies.all():
      qs = FeatureInDocument.objects.filter(document_type=frequency.document_type)
      total = qs.count()
      if total == 0:
        frequency.delete()
      else:
        value = Decimal(qs.filter(feature=feature).count())
        total = Decimal(total)
        frequency.frequency = value / total
        frequency.relative_weight = frequency.feature.type.weight * (1 - frequency.frequency)
        frequency.save()
    if feature.frequencies.count() == 0:
      feature.delete()
    else:
      feature.is_dirty = False
      feature.save()