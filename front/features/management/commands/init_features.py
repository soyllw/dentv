#-*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from entries.models import Entry
from features.models import extract_features


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        for e in Entry.archive.all():
            extract_features(None, e)