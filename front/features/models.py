#-*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db.models.signals import post_save, post_delete


class FeatureType(models.Model):
  class Meta:
    verbose_name = u'Тип свойства документа'
    verbose_name_plural = u'Типы свойств докментов'

  code = models.CharField(max_length=64, verbose_name=u'Код свойства', editable=False, unique=True)
  title = models.CharField(max_length=128, verbose_name=u'Название свойства', blank=True)
  weight = models.DecimalField(verbose_name=u'Вес свойства', default=1.0, max_digits=8, decimal_places=2)


class Feature(models.Model):
  type = models.ForeignKey(FeatureType)
  value = models.CharField(max_length=512)
  is_dirty = models.BooleanField(default=True)


class FeatureFrequency(models.Model):
  feature = models.ForeignKey(Feature, related_name='frequencies')
  document_type = models.ForeignKey(ContentType)
  # number of document of this type with this feature / total number of documents of this type
  # `term` frequency
  frequency = models.DecimalField(max_digits=9, decimal_places=8, default=1.0)
  relative_weight = models.DecimalField(default=1.0, max_digits=8, decimal_places=2)


class FeatureInDocument(models.Model):
  feature = models.ForeignKey(Feature, related_name='documents')
  document_type = models.ForeignKey(ContentType)
  document_id = models.PositiveIntegerField()
  document = generic.GenericForeignKey('document_type', 'document_id')

  class Meta:
    unique_together = (('feature', 'document_type', 'document_id'),)


def extract_features(sender, instance, *args, **kwargs):
  fcode = None
  ctype = ContentType.objects.get_for_model(instance)
  FeatureInDocument.objects.filter(
    document_id = instance.id,
    document_type__id = ctype.id
  ).delete()
  for code, value in instance.features:
    if fcode != code:
      typ, _ = FeatureType.objects.get_or_create(code=code)
      fcode = code
    feature, _ = Feature.objects.get_or_create(type=typ, value=value)
    feature.is_dirty = True
    feature.save()
    FeatureFrequency.objects.get_or_create(
      feature = feature,
      document_type = ctype
    )
    FeatureInDocument.objects.get_or_create(
      feature = feature,
      document_id = instance.id,
      document_type = ctype
    )


# TODO: add settings validation

for app, models in settings.FEATURES_CONF['sources'].items():
  module = __import__('%s.models' % app, globals(), locals(), models, -1)
  for model_name in models:
    model = getattr(module, model_name)
    # TODO: add model validation [hasattr(model, 'features')]
    post_save.connect(extract_features, sender=model, dispatch_uid='features.signals.post_save')
    #post_delete.connect(extract_features, sender=model, dispatch_uid='features.signals.post_delete')
