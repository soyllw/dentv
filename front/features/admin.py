#coding: utf-8
from django.contrib import admin

from features.models import FeatureType


class FeatureTypeAdmin(admin.ModelAdmin):
  list_display = ('code', 'title')


admin.site.register(FeatureType, FeatureTypeAdmin)
