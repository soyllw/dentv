from django.core.cache import cache


def cached(func, key, duration=60*60*24):
    res = cache.get(key)
    if res is None:
        res = func()
        cache.set(key, res, duration)
    return res
