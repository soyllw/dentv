# -*- coding: utf-8 -*-
import re

class Hyphenator(object):
  rus_v = ur"[аеёиоуыэюя]"
  rus_n = ur"[бвгджзклмнпрстфхцчшщ]"
  rus_x = ur"[йъь]"
  rus_a = ur"[абвгдеёжзийклмнопрстуфхцчшщъыьэюя]"

  def __init__(self):
    self.rules = map(self.compileRule, [
      ( self.rus_v + self.rus_n + self.rus_n, self.rus_n + self.rus_n + self.rus_v ),
      ( self.rus_v + self.rus_n, self.rus_n + self.rus_n + self.rus_v ),
      ( self.rus_n + self.rus_v, self.rus_n + self.rus_v ),
      ( self.rus_v + self.rus_n, self.rus_n + self.rus_v ),
      ( self.rus_v, self.rus_v + self.rus_a ),
      ( self.rus_x, self.rus_a + self.rus_a )
    ])

  def compileRule(self, rule):
    pred, post = rule
    return re.compile(u"({0})({1})".format(pred, post), re.U | re.I)

  def hyphen(self, instr):
    # reduce here?
    for rule in self.rules:
      instr = rule.sub(ur"\1&shy;\2", instr)
    return instr