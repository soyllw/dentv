#-*- coding: utf-8 -*-
"""
Модели
~~~~~~"""
from django.db import models
from django.contrib.contenttypes.models import ContentType

from autoslug import AutoSlugField


class TitledSlugEntry(models.Model):
    """Модель-примесь, добавляющая общие для большинства моделей поля"""
    class Meta:
        abstract = True
    title = models.CharField(max_length=1024, verbose_name=u'Название')
    slug = AutoSlugField(max_length=1024, unique=True, editable=False, populate_from='title', verbose_name=u'Слаг')


class Rateable(models.Model):
    """Модель-примесь, добавляющая возможность набирать рейтинг"""
    class Meta:
        abstract = True
    rating = models.BigIntegerField(default=0, verbose_name=u'Рейтинг', editable=False)

    def get_rating(self):
        """Возвращает рейтинг данного объекта"""
        # reserved for rating manipulations
        return self.rating


class ViewsCountable(models.Model):
    """Модель-примесь, добавляющая возможность подсчитывать просмотры"""
    class Meta:
        abstract = True
    views_count = models.BigIntegerField(default=0, verbose_name=u'Кол-во просмотров', editable=False)


class Commentable(models.Model):
    """Модель-примесь, добавляющая возможность комментировать объекты модели"""
    class Meta:
        abstract = True
    comments_allowed = models.BooleanField(default=True, verbose_name=u'Разрешить комментирование')
    comments_count = models.BigIntegerField(default=0, verbose_name=u'Кол-во комментариев', editable=False)

    def bubble_changes(self):
        """Метод для проброса изменившихся значений `comments_count` при необходимости."""
        pass


class ExtendedModel(models.Model):
    """Расширенная модель. Содержит дополнительные служебные функции."""
    class Meta:
        abstract = True

    def get_content_type(self):
        """Возвращает ``content_type`` данного объекта"""
        return get_content_type(self)


class ContentModel(models.Model):
    """Обобщенная абстрактная модель содержимого"""
    is_video = False

    class Meta:
        abstract = True

    entry = models.ForeignKey('entries.Entry', verbose_name=u'Выпуск')
    position = models.SmallIntegerField(default=0, verbose_name=u'Позиция')
    content = models.TextField(verbose_name=u'Содержимое')
    enabled = models.BooleanField(default=True, verbose_name=u'Включать в выпуск')

    def get_content_type(self):
        """Возвращает ``content_type`` данного объекта"""
        return get_content_type(self)


def get_content_type(obj):
    """Возвращает ``content_type`` переданного в качестве параметра ``obj`` объекта"""
    # TODO: cache whole content-type mapping
    return ContentType.objects.get_for_model(obj.__class__)


def get_content_type_by_class(klass):
    """Возвращает ``content_type`` переданного класса"""
    return ContentType.objects.get_for_model(klass)


def run_raw(query):
    from django.db import connection
    cursor = connection.cursor()
    cursor.execute(query)
    desc = cursor.description
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        yield dict(zip([col[0] for col in desc], row))
