#-*- coding: utf-8 -*-
"""
Дополнительные утилиты
~~~~~~~~~~~~~~~~~~~~~~"""
import re
import urllib

from functools import wraps

from django.conf import settings
from django.http import HttpResponse, QueryDict
from django.template import RequestContext
from jinja2 import Environment, FileSystemLoader

from pytils.dt import ru_strftime, distance_of_time_in_words
from pytils.numeral import get_plural

from utils.hyphen import Hyphenator
from colloquium.models import get_poll_for_item

try:
    if 'devserver' not in settings.INSTALLED_APPS:
        raise ImportError
    from devserver.modules.profile import devserver_profile
except ImportError:
    class devserver_profile(object):
        def __init__(self, *args, **kwargs):
            pass

        def __call__(self, f):
            def noop(*args, **kwargs):
                return f(*args, **kwargs)
            return wraps(func)(noop)


class TemplateViewMixin(object):
    """Примесь, обеспечивающая отображение шаблонов ``jinja2``"""

    def render_to_response(self, context, **kwargs):
        """Построить страницу по шаблону."""
        return render_to_response(self.template_name, self.request, context)


def fuzzy_distance(value, now):
    """"Нечеткое" описание дистанции (вчера, сегодня). """
    diff = now - value
    if diff.days == 0:
        if now.day == value.day:
            return u'Сегодня'
        else:
            return u'Вчера'
    elif diff.days == 1:
        return u'Вчера'
    else:
        return distance_of_time_in_words(value)


def pages_range(page):
    """ Подсчет числа выводимых страниц для пагинатора """
    starting_number = (page.number - 5) if page.number >= 5 else 0
    return page.paginator.page_range[starting_number:starting_number + 10]


_para_re = re.compile(r'(?:\r\n|\r|\n){2,}')


def nl2br(value):
    return u''.join(u'<p>%s</p>' % p.replace('\n', '<br>\n') for p in _para_re.split(value))


def groupsOf(value, card):
    k = 0
    while k < len(value):
        yield value[k:k+card]
        k += card


def pageiter(request):
    d = request.GET.copy()
    d.pop('page', None)
    d.pop('id', None)
    #d.pop('slug', None)
    def iterator(page):
        d['page'] = page
        return u'{0}?{1}'.format(request.path, d.urlencode())
    return iterator


jinja_extensions = (
    'utils.extensions.contrib.URLExtension',
    #'utils.extensions.jinja2htmlcompress.HTMLCompress',
    'jinja2.ext.with_',
)


environment = Environment(
    loader=FileSystemLoader(searchpath=settings.TEMPLATE_DIRS),
    extensions=jinja_extensions,
    trim_blocks=True
)


environment.filters['ru_strftime'] = lambda v, i=False, f=u'%d %B': ru_strftime(date=v, inflected=i, format=unicode(f))
environment.filters['fuzzy_distance'] = fuzzy_distance
environment.filters['pages_range'] = pages_range
environment.filters['nl2br'] = nl2br
environment.filters['urlencode'] = urllib.quote_plus
environment.filters['get_plural'] = get_plural

environment.globals['pageiter'] = pageiter
environment.globals['groupsOf'] = groupsOf
environment.globals['MEDIA_URL'] = settings.MEDIA_URL
environment.globals['STATIC_URL'] = settings.STATIC_URL
environment.globals['get_poll_for_item'] = get_poll_for_item
environment.globals['hyphenator'] = Hyphenator()


def render_to_response(filename, request, context_dict):
    """Построение страницы по шаблону и отправка объекта ``HttpResponse``."""
    if not isinstance(context_dict, dict):
        return context_dict
    context = RequestContext(request, context_dict)
    data = {}
    [data.update(d) for d in context]
    response = environment.get_template(filename).render(**data)
    return HttpResponse(content=response, content_type='text/html')


def render_to(filename):
    """Построение страницы по шаблону, переданному в параметре ``filename``"""
    def decoratee(func):
        @wraps(func)
        def decorator(*args, **kwargs):
            return render_to_response(filename, args[0], func(*args, **kwargs))
        return decorator
    return decoratee
