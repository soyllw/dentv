import os
import traceback
import codecs

from django.core.management.base import BaseCommand
from django.conf import settings

from utils.view_utils import environment


class JSJinja(object):
    def __init__(self, ast):
        self.blocks = []
        self.extends = 'baseTpl'
        self.ast = ast

    def dump(self):
        js = self.convert(self.ast)
        return "klass = function(context) {base = %s;%s}" % js

    def convert_template(self, node):
        subs = map(self.convert, node.iter_child_nodes())
        return """
            var collector = [];
            %s
            return collector.join("");
        """ % '\n'.join(subs)

    def convert_extends(self, node):
        self.extends = ''.join(map(self.convert, node.iter_child_nodes()))
        return ''

    def convert_const(self, node):
        return unicode(node.value)

    def convert_templatedata(self, node):
        data = node.data.strip()
        if len(data) > 0:
            res = "collector.push('%s');" % data
        else:
            res = ''
        return res

    def convert_output(self, node):
        return ''.join(map(self.convert, node.iter_child_nodes()))

    def convert_block(self, node):
        # node.scoped -- wtf???
        return 'var %s' % node.name

    def convert_name(self, node):
        return 'NAME'
        #return '%s in %s' % (node.name, node.ctx)

    def convert_call(self, node):
        return 'CALL'
        #return '%s(%s);' % (node.node, subs)

    def convert_keyword(self, node):
        return '%s=%s' % (node.key, node.value)

    def convert_getattr(self, node):
        return u'%s[%s]' % (node.node, node.attr)

    def convert_fromimport(self, node):
        tpl = 'var {0} = import("{0}", "{1}");'
        return '\n'.join([tpl.format(name, "SUBS") for name in node.names])

    def convert_mod(self, node):
        return '%s / %s' % (node.left, node.right)

    def convert(self, node):
        #subs = '\n'.join([self.dump(c) for c in node.iter_child_nodes()])
        method = 'convert_%s' % node.__class__.__name__.lower()
        try:
            val = getattr(self, method)(node)
        except Exception:
            val = 'UNPARSED'
        return val


class Command(BaseCommand):
    def filenames(self):
        for templateDir in settings.TEMPLATE_DIRS:
            for root, folders, files in os.walk(templateDir):
                for filename in files:
                    aname = os.path.join(root, filename)
                    rname = os.path.relpath(aname, templateDir)
                    yield aname, rname, filename

    def template_asts(self):
        for aname, rname, name in self.filenames():
            with codecs.open(aname, 'r', 'utf-8') as f:
                yield name, environment.parse(f.read())

    def handle(self, *args, **kwargs):
        num = 0
        for name, ast in self.template_asts():
            if name == 'macros.html':
                continue
            print "%s.js" % name
            print JSJinja(ast).dump()
            num += 1
            if num > 1:
                break
