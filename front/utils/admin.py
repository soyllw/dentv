from django.contrib import admin

from django.contrib.sites.models import Site
from social_auth.db.django_models import UserSocialAuth, Nonce, Association

admin.site.unregister(Site)
admin.site.unregister(UserSocialAuth)
admin.site.unregister(Nonce)
admin.site.unregister(Association)
