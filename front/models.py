# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish to give write DB access
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class AdminPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=255)
    application = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    mode = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'admin_permission'

class AdminUserpermission(models.Model):
    id = models.IntegerField(primary_key=True)
    permission = models.ForeignKey(AdminPermission)
    user = models.ForeignKey('SiteuserUser')
    class Meta:
        managed = False
        db_table = 'admin_userpermission'

class AdminUsertoken(models.Model):
    id = models.IntegerField(primary_key=True)
    token = models.CharField(max_length=128)
    user = models.ForeignKey('SiteuserUser', unique=True)
    class Meta:
        managed = False
        db_table = 'admin_usertoken'

class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(unique=True, max_length=80)
    class Meta:
        managed = False
        db_table = 'auth_group'

class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')
    class Meta:
        managed = False
        db_table = 'auth_group_permissions'

class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'auth_permission'

class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    password = models.CharField(max_length=128)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    is_superuser = models.BooleanField()
    last_login = models.DateTimeField()
    date_joined = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'auth_user'

class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)
    class Meta:
        managed = False
        db_table = 'auth_user_groups'

class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)
    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'

class CeleryTaskmeta(models.Model):
    id = models.IntegerField(primary_key=True)
    task_id = models.CharField(unique=True, max_length=255)
    status = models.CharField(max_length=50)
    result = models.TextField(blank=True)
    date_done = models.DateTimeField()
    traceback = models.TextField(blank=True)
    hidden = models.BooleanField()
    meta = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'celery_taskmeta'

class CeleryTasksetmeta(models.Model):
    id = models.IntegerField(primary_key=True)
    taskset_id = models.CharField(unique=True, max_length=255)
    result = models.TextField()
    date_done = models.DateTimeField()
    hidden = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'celery_tasksetmeta'

class ColloquiumAnswer(models.Model):
    id = models.IntegerField(primary_key=True)
    poll = models.ForeignKey('ColloquiumPoll')
    user = models.ForeignKey('SiteuserUser')
    created_at = models.DateTimeField()
    chosen = models.ForeignKey('ColloquiumOption')
    class Meta:
        managed = False
        db_table = 'colloquium_answer'

class ColloquiumOption(models.Model):
    id = models.IntegerField(primary_key=True)
    poll = models.ForeignKey('ColloquiumPoll')
    position = models.IntegerField()
    option = models.CharField(max_length=255)
    class Meta:
        managed = False
        db_table = 'colloquium_option'

class ColloquiumPoll(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=1024)
    slug = models.CharField(max_length=1024)
    created_at = models.DateTimeField()
    allow_multiple = models.BooleanField()
    is_active = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'colloquium_poll'

class CommentsComment(models.Model):
    id = models.IntegerField(primary_key=True)
    rating = models.BigIntegerField()
    created_at = models.DateTimeField()
    parent = models.ForeignKey('self', blank=True, null=True)
    author = models.ForeignKey('SiteuserUser')
    comment = models.TextField()
    comment_html = models.TextField()
    ip = models.GenericIPAddressField()
    content_type = models.ForeignKey('DjangoContentType')
    enabled = models.BooleanField()
    reason = models.CharField(max_length=255)
    object_id = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'comments_comment'

class ContentDentvvideo(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey('EntriesEntry')
    position = models.SmallIntegerField()
    content = models.TextField()
    enabled = models.BooleanField()
    width = models.IntegerField()
    height = models.IntegerField()
    link = models.CharField(max_length=200, blank=True)
    class Meta:
        managed = False
        db_table = 'content_dentvvideo'

class ContentHtmltext(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey('EntriesEntry')
    position = models.SmallIntegerField()
    content = models.TextField()
    enabled = models.BooleanField()
    is_transcript = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'content_htmltext'

class ContentMarkdowntext(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey('EntriesEntry')
    position = models.SmallIntegerField()
    content = models.TextField()
    enabled = models.BooleanField()
    is_transcript = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'content_markdowntext'

class ContentYoutubevideo(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey('EntriesEntry')
    position = models.SmallIntegerField()
    content = models.TextField()
    enabled = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'content_youtubevideo'

class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)
    action_time = models.DateTimeField()
    user = models.ForeignKey(AuthUser)
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    class Meta:
        managed = False
        db_table = 'django_admin_log'

class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'django_content_type'

class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'django_session'

class DjangoSite(models.Model):
    id = models.IntegerField(primary_key=True)
    domain = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    class Meta:
        managed = False
        db_table = 'django_site'

class DjceleryCrontabschedule(models.Model):
    id = models.IntegerField(primary_key=True)
    minute = models.CharField(max_length=64)
    hour = models.CharField(max_length=64)
    day_of_week = models.CharField(max_length=64)
    day_of_month = models.CharField(max_length=64)
    month_of_year = models.CharField(max_length=64)
    class Meta:
        managed = False
        db_table = 'djcelery_crontabschedule'

class DjceleryIntervalschedule(models.Model):
    id = models.IntegerField(primary_key=True)
    every = models.IntegerField()
    period = models.CharField(max_length=24)
    class Meta:
        managed = False
        db_table = 'djcelery_intervalschedule'

class DjceleryPeriodictask(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(unique=True, max_length=200)
    task = models.CharField(max_length=200)
    interval = models.ForeignKey(DjceleryIntervalschedule, blank=True, null=True)
    crontab = models.ForeignKey(DjceleryCrontabschedule, blank=True, null=True)
    args = models.TextField()
    kwargs = models.TextField()
    queue = models.CharField(max_length=200, blank=True)
    exchange = models.CharField(max_length=200, blank=True)
    routing_key = models.CharField(max_length=200, blank=True)
    expires = models.DateTimeField(blank=True, null=True)
    enabled = models.BooleanField()
    last_run_at = models.DateTimeField(blank=True, null=True)
    total_run_count = models.IntegerField()
    date_changed = models.DateTimeField()
    description = models.TextField()
    class Meta:
        managed = False
        db_table = 'djcelery_periodictask'

class DjceleryPeriodictasks(models.Model):
    ident = models.SmallIntegerField(primary_key=True)
    last_update = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'djcelery_periodictasks'

class DjceleryTaskstate(models.Model):
    id = models.IntegerField(primary_key=True)
    state = models.CharField(max_length=64)
    task_id = models.CharField(unique=True, max_length=36)
    name = models.CharField(max_length=200, blank=True)
    tstamp = models.DateTimeField()
    args = models.TextField(blank=True)
    kwargs = models.TextField(blank=True)
    eta = models.DateTimeField(blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    result = models.TextField(blank=True)
    traceback = models.TextField(blank=True)
    runtime = models.FloatField(blank=True, null=True)
    retries = models.IntegerField()
    worker = models.ForeignKey('DjceleryWorkerstate', blank=True, null=True)
    hidden = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'djcelery_taskstate'

class DjceleryWorkerstate(models.Model):
    id = models.IntegerField(primary_key=True)
    hostname = models.CharField(unique=True, max_length=255)
    last_heartbeat = models.DateTimeField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'djcelery_workerstate'

class EntriesAirschedule(models.Model):
    id = models.IntegerField(primary_key=True)
    date = models.DateField(unique=True)
    class Meta:
        managed = False
        db_table = 'entries_airschedule'

class EntriesDebateitem(models.Model):
    id = models.IntegerField(primary_key=True)
    live = models.ForeignKey('EntriesYtliveevent')
    name = models.CharField(max_length=200)
    class Meta:
        managed = False
        db_table = 'entries_debateitem'

class EntriesDebateitemvote(models.Model):
    id = models.IntegerField(primary_key=True)
    item = models.ForeignKey(EntriesDebateitem)
    upvote = models.BooleanField()
    ip = models.GenericIPAddressField()
    class Meta:
        managed = False
        db_table = 'entries_debateitemvote'

class EntriesEntry(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=1024)
    slug = models.CharField(unique=True, max_length=1024)
    rating = models.BigIntegerField()
    views_count = models.BigIntegerField()
    released_at = models.DateTimeField()
    description = models.TextField()
    program_id = models.IntegerField()
    thumb_source = models.ForeignKey('ProgramsProgram', db_column='thumb_source')
    content = models.TextField(blank=True)
    tags = models.TextField(blank=True) # This field type is a guess.
    search_index = models.TextField(blank=True) # This field type is a guess.
    enabled = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'entries_entry'

class EntriesEntryannounce(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=1024)
    slug = models.CharField(max_length=1024)
    start_at = models.DateTimeField()
    stop_at = models.DateTimeField()
    content = models.TextField()
    thumb_source = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'entries_entryannounce'

class EntriesEntryexporttask(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey(EntriesEntry, unique=True)
    finished = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'entries_entryexporttask'

class EntriesEntryimporttask(models.Model):
    id = models.IntegerField(primary_key=True)
    link = models.CharField(max_length=128)
    finished = models.BooleanField()
    entry = models.ForeignKey(EntriesEntry, blank=True, null=True)
    program = models.ForeignKey('ProgramsProgram')
    class Meta:
        managed = False
        db_table = 'entries_entryimporttask'

class EntriesEntryonair(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey(EntriesEntry, blank=True, null=True)
    title = models.CharField(max_length=200)
    is_live = models.BooleanField()
    schedule_id = models.IntegerField()
    start = models.TimeField()
    end = models.ForeignKey(EntriesAirschedule, db_column='end')
    class Meta:
        managed = False
        db_table = 'entries_entryonair'

class EntriesEntrypromotion(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey(EntriesEntry)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    text = models.CharField(max_length=1024)
    class Meta:
        managed = False
        db_table = 'entries_entrypromotion'

class EntriesEntryuserrole(models.Model):
    id = models.IntegerField(primary_key=True)
    position = models.IntegerField()
    entry = models.ForeignKey(EntriesEntry)
    user = models.ForeignKey('SiteuserUser')
    role = models.IntegerField()
    role_text = models.CharField(max_length=255)
    comment = models.TextField()
    class Meta:
        managed = False
        db_table = 'entries_entryuserrole'

class EntriesQuestion(models.Model):
    id = models.IntegerField(primary_key=True)
    entry = models.ForeignKey(EntriesEntry)
    fio = models.CharField(max_length=512)
    email = models.CharField(max_length=75)
    text = models.TextField()
    reply = models.ForeignKey('self', blank=True, null=True)
    author = models.ForeignKey('SiteuserUser')
    enabled = models.BooleanField()
    created_at = models.DateTimeField()
    support = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'entries_question'

class EntriesQuestionsupport(models.Model):
    id = models.IntegerField(primary_key=True)
    question = models.ForeignKey(EntriesQuestion)
    user = models.ForeignKey('SiteuserUser', blank=True, null=True)
    ip = models.GenericIPAddressField()
    class Meta:
        managed = False
        db_table = 'entries_questionsupport'

class EntriesYtliveevent(models.Model):
    id = models.IntegerField(primary_key=True)
    link = models.CharField(max_length=200)
    start = models.DateTimeField()
    stop = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'entries_ytliveevent'

class ExternalToken(models.Model):
    id = models.IntegerField(primary_key=True)
    service = models.IntegerField()
    client_id = models.CharField(max_length=128)
    token = models.CharField(max_length=255)
    refresh_token = models.CharField(max_length=255)
    api_key = models.CharField(max_length=255)
    class Meta:
        managed = False
        db_table = 'external_token'

class FeaturesFeature(models.Model):
    id = models.IntegerField(primary_key=True)
    type = models.ForeignKey('FeaturesFeaturetype')
    value = models.CharField(max_length=512)
    is_dirty = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'features_feature'

class FeaturesFeaturefrequency(models.Model):
    id = models.IntegerField(primary_key=True)
    feature = models.ForeignKey(FeaturesFeature)
    document_type = models.ForeignKey(DjangoContentType)
    frequency = models.DecimalField(max_digits=9, decimal_places=8)
    relative_weight = models.DecimalField(max_digits=8, decimal_places=2)
    class Meta:
        managed = False
        db_table = 'features_featurefrequency'

class FeaturesFeatureindocument(models.Model):
    id = models.IntegerField(primary_key=True)
    feature = models.ForeignKey(FeaturesFeature)
    document_type = models.ForeignKey(DjangoContentType)
    document_id = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'features_featureindocument'

class FeaturesFeaturetype(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(unique=True, max_length=64)
    title = models.CharField(max_length=128)
    weight = models.DecimalField(max_digits=8, decimal_places=2)
    class Meta:
        managed = False
        db_table = 'features_featuretype'

class FriggEvent(models.Model):
    id = models.IntegerField(primary_key=True)
    kind = models.CharField(max_length=128)
    source = models.CharField(max_length=128)
    type = models.CharField(max_length=128)
    client_time = models.DateTimeField()
    token = models.CharField(max_length=128, blank=True)
    class Meta:
        managed = False
        db_table = 'frigg_event'

class FriggEventdata(models.Model):
    id = models.IntegerField(primary_key=True)
    event = models.ForeignKey(FriggEvent)
    key = models.CharField(max_length=128)
    class Meta:
        managed = False
        db_table = 'frigg_eventdata'

class FriggEventnumberdata(models.Model):
    eventdata_ptr = models.ForeignKey(FriggEventdata, primary_key=True)
    value = models.DecimalField(max_digits=14, decimal_places=4)
    class Meta:
        managed = False
        db_table = 'frigg_eventnumberdata'

class FriggEventtextdata(models.Model):
    eventdata_ptr = models.ForeignKey(FriggEventdata, primary_key=True)
    value = models.TextField()
    class Meta:
        managed = False
        db_table = 'frigg_eventtextdata'

class MailEmailconfirmation(models.Model):
    id = models.IntegerField(primary_key=True)
    verkey = models.CharField(max_length=128)
    user = models.ForeignKey('SiteuserUser')
    class Meta:
        managed = False
        db_table = 'mail_emailconfirmation'

class MailEmailqueue(models.Model):
    id = models.IntegerField(primary_key=True)
    from_field = models.CharField(max_length=250)
    to_field = models.TextField()
    subject = models.CharField(max_length=250)
    body = models.TextField()
    created_at = models.DateTimeField()
    sent_at = models.DateTimeField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'mail_emailqueue'

class MailEmailtemplate(models.Model):
    id = models.IntegerField(primary_key=True)
    key = models.CharField(unique=True, max_length=250)
    title = models.CharField(max_length=250)
    subj = models.CharField(max_length=250)
    body = models.TextField()
    class Meta:
        managed = False
        db_table = 'mail_emailtemplate'

class NewsNewsitem(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=1024)
    slug = models.CharField(unique=True, max_length=1024)
    rating = models.BigIntegerField()
    comments_count = models.BigIntegerField()
    original_url = models.CharField(max_length=1024)
    pub_date = models.DateTimeField()
    text = models.TextField()
    comments_allowed = models.BooleanField()
    tags = models.TextField(blank=True) # This field type is a guess.
    is_important = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'news_newsitem'

class ProgramsProgram(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=1024)
    slug = models.CharField(unique=True, max_length=1024)
    rating = models.BigIntegerField()
    views_count = models.BigIntegerField()
    comments_count = models.BigIntegerField()
    description = models.TextField()
    created_at = models.DateTimeField()
    email = models.CharField(max_length=75)
    logo = models.CharField(max_length=100)
    owner = models.ForeignKey('SiteuserUser')
    subscribers_count = models.BigIntegerField()
    on_top = models.BooleanField()
    comments_allowed = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'programs_program'

class ProgramsProgramlink(models.Model):
    id = models.IntegerField(primary_key=True)
    program = models.ForeignKey(ProgramsProgram)
    title = models.CharField(max_length=1024)
    url = models.CharField(max_length=200)
    kind = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'programs_programlink'

class ProgramsSubscription(models.Model):
    id = models.IntegerField(primary_key=True)
    created_at = models.DateTimeField()
    program = models.ForeignKey(ProgramsProgram)
    user = models.ForeignKey('SiteuserUser')
    class Meta:
        managed = False
        db_table = 'programs_subscription'

class SiteuserUser(models.Model):
    user_ptr = models.ForeignKey(AuthUser, primary_key=True)
    display_name = models.CharField(max_length=200)
    dob = models.DateField(blank=True, null=True)
    gender = models.IntegerField()
    location = models.CharField(max_length=100)
    occupation = models.CharField(max_length=200)
    about = models.TextField()
    activation = models.CharField(max_length=32)
    avatar_source = models.CharField(max_length=100)
    update_value = models.BooleanField()
    has_blog = models.BooleanField()
    created = models.DateField(blank=True, null=True)
    generated = models.BooleanField()
    last_comment_date = models.DateField(blank=True, null=True)
    comments_count = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'siteuser_user'

class SocialAuthAssociation(models.Model):
    id = models.IntegerField(primary_key=True)
    server_url = models.CharField(max_length=255)
    handle = models.CharField(max_length=255)
    secret = models.CharField(max_length=255)
    issued = models.IntegerField()
    lifetime = models.IntegerField()
    assoc_type = models.CharField(max_length=64)
    class Meta:
        managed = False
        db_table = 'social_auth_association'

class SocialAuthNonce(models.Model):
    id = models.IntegerField(primary_key=True)
    server_url = models.CharField(max_length=255)
    timestamp = models.IntegerField()
    salt = models.CharField(max_length=40)
    class Meta:
        managed = False
        db_table = 'social_auth_nonce'

class SocialAuthUsersocialauth(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(SiteuserUser)
    provider = models.CharField(max_length=32)
    uid = models.CharField(max_length=255)
    extra_data = models.TextField()
    class Meta:
        managed = False
        db_table = 'social_auth_usersocialauth'

class SouthMigrationhistory(models.Model):
    id = models.IntegerField(primary_key=True)
    app_name = models.CharField(max_length=255)
    migration = models.CharField(max_length=255)
    applied = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'south_migrationhistory'

class StaticContentEntry(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=1024)
    slug = models.CharField(unique=True, max_length=1024)
    text = models.TextField()
    class Meta:
        managed = False
        db_table = 'static_content_entry'

class TaggitTag(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    slug = models.CharField(unique=True, max_length=100)
    class Meta:
        managed = False
        db_table = 'taggit_tag'

class TaggitTaggeditem(models.Model):
    id = models.IntegerField(primary_key=True)
    tag = models.ForeignKey(TaggitTag)
    object_id = models.IntegerField()
    content_type = models.ForeignKey(DjangoContentType)
    class Meta:
        managed = False
        db_table = 'taggit_taggeditem'

class TasksScheduledtask(models.Model):
    id = models.IntegerField(primary_key=True)
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    added = models.DateTimeField()
    started = models.DateTimeField(blank=True, null=True)
    finished = models.DateTimeField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'tasks_scheduledtask'

class TopicsTopic(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=1024)
    slug = models.CharField(unique=True, max_length=1024)
    is_actual = models.BooleanField()
    tags = models.TextField(blank=True) # This field type is a guess.
    latest_entry = models.ForeignKey(EntriesEntry, blank=True, null=True)
    position = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'topics_topic'

class TopicsTopicTags(models.Model):
    id = models.IntegerField(primary_key=True)
    topic = models.ForeignKey(TopicsTopic)
    tag = models.ForeignKey(TaggitTag)
    class Meta:
        managed = False
        db_table = 'topics_topic_tags'

class Votes(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(SiteuserUser)
    content_type = models.ForeignKey(DjangoContentType)
    object_id = models.IntegerField()
    vote = models.SmallIntegerField()
    class Meta:
        managed = False
        db_table = 'votes'

