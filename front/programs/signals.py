from django.db.models.signals import post_save, post_delete
from django.core.cache import cache

from programs.models import Program

def drop_programs_cache(*args, **kwargs):
    cache.delete('top-programs')

post_save.connect(drop_programs_cache, sender=Program, dispatch_uid='programs.signals.post_save_drop_cache')
post_delete.connect(drop_programs_cache, sender=Program, dispatch_uid='programs.signals.post_delete_drop_cache')