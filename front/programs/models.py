#-*- coding: utf-8 -*-
"""
Модели
~~~~~~"""
from django.db import models
from django.db.models import Sum

from utils.models import ExtendedModel, TitledSlugEntry, Rateable, ViewsCountable, Commentable

from siteuser.models import User


class Program(TitledSlugEntry, ExtendedModel, Commentable, Rateable, ViewsCountable):
    """Модель программы."""
    description = models.TextField(verbose_name=u'Описание', blank=True)
    created_at = models.DateTimeField(verbose_name=u'Время создания')
    email = models.EmailField(verbose_name=u'Email', blank=True)
    logo = models.ImageField(upload_to='programs', verbose_name=u'Логотип')
    # owner_id == 1 --> common program, /= 1 --> blog
    owner = models.ForeignKey(User, related_name='programs', verbose_name=u'Владелец передачи')
    subscribers_count = models.BigIntegerField(default=0, verbose_name=u'Кол-во подписчиков', editable=False)
    on_top = models.BooleanField(default=False, verbose_name=u'Выводить в верхнем меню')

    class Meta:
        verbose_name = u'Программа'
        verbose_name_plural = u'Программы'
        ordering = ('-created_at', 'id')

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        """Ссылка на программу"""
        return ('programs.view.item', (), {'slug': self.slug})

    @models.permalink
    def get_feed_url(self):
        """Ссылка на RSS-поток программы"""
        return ('programs.feed.item', (), {'slug': self.slug})

    @models.permalink
    def get_entries_url(self):
        """Ссылка на список выпусков программы"""
        return ('programs.view.entries', (), {'slug': self.slug})

    @property
    def latest_entry(self):
        """Получить последний выпуск программы"""
        # TODO: per-program cache
        try:
            return self.entries.latest('released_at')
        except:
            return None

    @property
    def latest_entry_date(self):
        latest_entry = self.latest_entry
        if latest_entry is not None:
            return latest_entry.released_at
        return None

    @property
    def entries_count(self):
        """Получить количество выпусков программы."""
        # TODO: denorm?
        return self.entries.count()

    def get_comments_count(self):
        """Число комментариев для программы"""
        res = self.entries.aggregate(comments_sum=Sum('comments_count')).get('comments_sum')
        if res is None:
            res = 0
        return res

    def recalculate_comments_count(self):
        Program.objects.filter(id=self.id).update(comments_count=self.get_comments_count())


class Subscription(ExtendedModel):
    """Подписка пользователей на программы"""
    created_at = models.DateTimeField(verbose_name=u'Начало подписки')
    program = models.ForeignKey(Program, verbose_name=u'Программа', related_name='subscriptions')
    user = models.ForeignKey(User, verbose_name=u'Пользователь', related_name='subscriptions')

    class Meta:
        verbose_name = u'Подписка на программу'
        verbose_name_plural = u'Подписки на программы'


class ProgramLink(ExtendedModel):
    """Ссылки программ"""
    LINK_TYPES = (
        (1, u'YouTube'),
        (2, u'LiveJournal'),
        (3, u'Blogspot'),
        (4, u'LiveInternet'),
    )
    program = models.ForeignKey(Program, verbose_name=u'Программа', related_name='links')
    title = models.CharField(max_length=1024, verbose_name=u'Название')
    url = models.URLField(verbose_name=u'URL')
    kind = models.IntegerField(choices=LINK_TYPES, verbose_name=u'Тип ссылки', default=1)

    class Meta:
        verbose_name = u'Ссылка программы'
        verbose_name_plural = u'Ссылки программ'

import programs.signals