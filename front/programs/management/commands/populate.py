#-*- coding: utf-8 -*-
import os

from datetime import datetime, timedelta
from random import randint, choice, sample

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.contrib.webdesign.lorem_ipsum import sentence, paragraphs, words
from django.core.files import File
from django.conf import settings

from programs.models import Program, ProgramLink
from siteuser.models import User
from entries.models import Entry, EntryPart, EntryUserRole
from tags.models import Tag, TaggedItem
from news.models import NewsItem

class Command(BaseCommand):
    start_programs = [
        u'Есть обоснованное мнение',
        u'Необоснованные предположения',
        u'Диалоги',
        u'Футурология России',
        u'Конспирология: теория и практика',
        u'Наука и жизнь'
    ]
    videos = [
        u'http://www.youtube.com/watch?v=Fok1KUhjAKs',
        u'http://www.youtube.com/watch?v=NDMuQsZcsIo',
        u'http://www.youtube.com/watch?v=O-liDyNO4Tg',
        u'http://www.youtube.com/watch?v=Nkl3vdl4AII',
        u'http://www.youtube.com/watch?v=w11m-Hvf6Po',
        u'http://www.youtube.com/watch?v=BSddP7rNTHM'
    ]

    links = [
        {'kind': 1, 'url': 'http://www.youtube.com/channel/UCZNKg6NI7mah4i3dE6m0oYw'},
        {'kind': 2, 'url': 'krispotupchik.livejournal.com'},
    ]

    def create_links(self, program):
        for x in self.links:
            link = ProgramLink(**x)
            link.title = u'"%s" в %s' % (program.title, link.get_kind_display())
            link.program = program
            link.save()

    def create_entry(self, program, date, v, pg, tags):
        entry = Entry.objects.create(
            title=words(count=randint(3,6)),
            released_at=date,
            description=sentence(),
            program=program,
        )
        entry.thumb_source.save('gintovt.jpg', File(open(os.path.abspath(os.path.join(settings.ROOT_PATH, '../fixtures/gintovt.jpg')))))
        entry.save()
        for y in range(0,randint(1,4)):
            part = EntryPart(entry=entry, title=sentence(), position=y)
            part.kind     = 1 if y == 0 else randint(1,2)
            if part.kind == 1:
                part.content = choice(self.videos)
            else:
                part.content = '\n'.join(paragraphs(randint(1,4)))
            part.save()
        [EntryUserRole.objects.create(entry=entry, user=x, role=1) for x in v]
        [EntryUserRole.objects.create(entry=entry, user=x, role=2) for x in sample(pg, randint(1,len(pg)))]
        [entry.tags.add(x) for x in sample(tags, randint(1,len(tags)))]
        return entry

    def handle(self, *args, **kwargs):
        now       = datetime.now()
        oneday    = timedelta(days=1)
        oneminute = timedelta(minutes=1)
        users     = User.objects.order_by('id').filter(user__is_staff=True, has_blog=True).all()
        tags      = Tag.objects.all()

        TaggedItem.objects.all().delete()
        EntryUserRole.objects.all().delete()
        EntryPart.objects.all().delete()
        Entry.objects.all().delete()
        ProgramLink.objects.all().delete()
        Program.objects.all().delete()

        programs = [Program.objects.create(title=x, description=words(count=randint(3,6)), email='web@zavtra.ru', owner=users[0], created_at=now-randint(10,30)*oneday) for x in self.start_programs]
        programs.extend([Program.objects.create(title=u'Авторская программа "%s"' % x.display_name, description=sentence(), email='web@zavtra.ru', owner=x, created_at=now-randint(10,30)*oneday) for x in users[1:]])
        for p in programs:
            self.create_links(p)
            v  = sample(users, randint(1,2))
            pg = filter(lambda w: w not in v, users)
            for x in range(0,randint(8,16)):
                entry = self.create_entry(p, p.created_at + randint(1,3)*oneday, v, pg, tags)
            for x in range(0, randint(1,3)):
                entry = self.create_entry(p, now + randint(1,3)*oneday, v, pg, tags)

	nstart = now - 2*oneday
	while True:
	    NewsItem.objects.create(pub_date=nstart, text=words(count=randint(5,15)))
	    if nstart > now:
		break
	    nstart += randint(1,800)*oneminute