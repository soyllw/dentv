#-*- coding: utf-8 -*-
import os
import gdata.youtube
import gdata.youtube.service
import gdata.service
import urllib2
import urlparse
import codecs
import time
import pickle

from pytils.translit import translify
from datetime import datetime, timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.files import File
from django.core.files.base import ContentFile
#from django.contrib.auth.models import User

from entries.models import Entry, EntryPart, EntryUserRole
from programs.models import Program
from siteuser.models import User
#from news.models import NewsItem

from yaml import load
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class Command(BaseCommand):
	now = datetime.now()
	yt = None
	users = {}
	programs = {}
	retries = set()

	def handle(self, *args, **kwargs):
		yml, email, password = args
		self.yt = gdata.youtube.service.YouTubeService()
		self.yt.ssl = True
		self.yt.email = email
		self.yt.password = password
		self.yt.source = 'dentv-importer'
		self.yt.ProgrammaticLogin()	
		self.system_user = User.objects.get(id=1)	
		data = load(codecs.open(yml, 'r', 'utf-8'))
		for p in data:
			self.create_entry(p)
			time.sleep(1)
		if len(self.retries) > 0:
			print "Errors while importing encountered, retrying in 20 seconds"
			time.sleep(20)
		while len(self.retries) > 0:
			self.create_entry(pickle.loads(self.retries.pop()))
			time.sleep(5)
		self.post_announce()
		try:
			e = Entry.objects.get(slug='bez-pravitelstva')
			e.released_at = e.released_at.replace(month=3)
			e.save()
		except:
			pass
		deactivate = [u'Политика', u'Культура', u'Экономика', u'Протест', u'Протесты', u'Национализм', u'Выпуски', u'Темы', u'Архив']
		# Program.objects.filter(title__in=deactivate).update(on_top=False)

	def post_announce(self):
		entry = Entry.objects.create(
			released_at = datetime(year=2012, month=5, day=24, hour=20, minute=0),
			title=u'Где грань?',
			description=u'обсуждение нашумевшего фильма "Матч"',
			program=Program.objects.get(pk=1)
		)
		EntryUserRole.objects.create(role=1, entry=entry, user=self.get_user({'first_name':u'Михаил', 'last_name': u'Леонтьев'}))
		EntryUserRole.objects.create(role=2, entry=entry, user=self.get_user({'first_name':u'Елена', 'last_name': u'Белокурова'}))

		entry = Entry.objects.create(
			released_at = datetime(year=2012, month=5, day=25, hour=15, minute=0),
			title=u'Репортаж с назначения министра МВД В.Колокольцева',
			description=u'',
			program=Program.objects.get(pk=1)
		)
		EntryUserRole.objects.create(role=1, entry=entry, user=self.get_user({'first_name':u'Александр', 'last_name': u'Бородай'}))

		entry = Entry.objects.create(
			released_at = datetime(year=2012, month=5, day=26, hour=0, minute=0),
			title=u'Дети и интернет',
			description=u'Обсуждение проблем безопасности в сети',
			program=Program.objects.get(pk=1)
		)
		EntryUserRole.objects.create(role=2, entry=entry, user=self.get_user({'first_name':u'Денис', 'last_name': u'Давыдов'}))
		EntryUserRole.objects.create(role=1, entry=entry, user=self.get_user({'first_name':u'Андрей', 'last_name': u'Фефелов'}))

	def get_user(self, data):
		fname = data.get('first_name')
		lname = data.get('last_name')
		user = self.users.get( (fname, lname) )
		if user is None:
			try:
				user = User.objects.get(first_name=fname, last_name=lname)
			except User.DoesNotExist:
				username = translify(u'_'.join([fname, lname]))
				user = User.objects.create(username=username, first_name=fname, last_name=lname, is_active=False, is_staff=True, is_superuser=False, password='!')
			self.users[(fname,lname)] = user
		return user

	def create_entry(self, data):
		print data.get('video')
		video_id = urlparse.urlparse(data.get('video')).query.split('=')[1]
		try:
			video_entry = self.yt.GetYouTubeVideoEntry(video_id=video_id)
		except gdata.service.RequestError:
			print "503 error"
			self.retries.add(pickle.dumps(data))
			return
		pname = data.get('program')
		pdata = {}
		if pname is None:
			blogger = data.get('blog')[0]
			user = self.get_user(blogger)
			pdata['owner'] = user
			pdata['title'] = blogger.get('blogname')
		else:
			pdata['owner'] = self.system_user
			pdata['title'] = pname
		program = self.programs.get(pname)
		if program is None:
			try:
				program = Program.objects.get(**pdata)
			except Program.DoesNotExist:
				pdata['created_at'] = self.now
				program = Program.objects.create(**pdata)
			self.programs[program.title] = program
		entry = Entry.objects.create(
			title       = video_entry.media.title.text.decode('utf-8'),
			released_at = video_entry.published.text.decode('utf-8'),
			description = video_entry.media.description.text.decode('utf-8'),
			program     = program,
			views_count = video_entry.statistics.view_count
		)
		if 'thumb' in data:
			entry.thumb_source.save(data.get('thumb'), File(open(os.path.abspath(os.path.join(settings.ROOT_PATH, '../fixtures/%s' % data.get('thumb'))))))
		else:
			videothumb = ContentFile(urllib2.urlopen(video_entry.media.thumbnail[0].url).read())
			entry.thumb_source.save('thumb.jpg', videothumb)
		url = [link.href for link in video_entry.link if link.rel == 'alternate' and link.type == 'text/html'][0]
		part = EntryPart.objects.create(
			entry       = entry,
			title       = video_entry.media.title.text.decode('utf-8'),
			position    = 1,
			kind        = 1,
			content     = url,
		)
		for author in data.get('authors', []):
			author_user = self.get_user(author)
			EntryUserRole.objects.create(entry=entry, user=author_user, role=author.get('role'))
		entry.add_tags(data.get('tags', []))