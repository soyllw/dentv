from datetime import datetime, timedelta

from django.test import TestCase, Client
from django.core.urlresolvers import reverse

from siteuser.models import User
from programs.models import Program
from entries.models import Entry

def sieg_heil():
    return User.objects.create(
        first_name = 'Adolf',
        last_name  = 'Hitler',
        dob        = datetime(year=1889,month=4, day=20),
        gender     = 1,
        location   = 'Berlin, Deutschland',
        occupation = 'You know... Europe ;)',
        about      = 'Famous writer and painter')

class ProgramModelsTestCase(TestCase):
    def setUp(self):
        self.adolf = sieg_heil()

    def tearDown(self):
        for m in [User, Program, Entry]:
            m.objects.all().delete()

    def test_hasnt_blog_onsave(self):
        self.assertFalse(self.adolf.has_blog)
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        entry   = Entry.objects.create(released_at=datetime.now(), program=program, title='test entry')
        self.assertTrue(self.adolf.has_blog)

class ProgramsViewsTestCase(TestCase):
    def setUp(self):
        self.adolf = sieg_heil()

    def tearDown(self):
        for m in [User, Program, Entry]:
            m.objects.all().delete()

    def test_programs_index(self):
        self.assertEqual(self.client.get(reverse('programs.view.list')).status_code, 200)

    def test_entryless_program_view(self):
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        self.assertEqual(self.client.get(program.get_absolute_url()).status_code, 404)

    def test_thumbless_latest_entry_progam_view(self):
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        entry   = Entry.objects.create(released_at=datetime.now(), program=program, title='test entry')
        self.assertEqual(self.client.get(program.get_absolute_url()).status_code, 404)

    def test_progam_view(self):
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        entry   = Entry.objects.create(released_at=datetime.now(), program=program, title='test entry', thumb_source='kekekeke')
        self.assertRedirects(self.client.get(program.get_absolute_url()), entry.get_absolute_url())

    def test_entryless_program_feed_view(self):
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        self.assertEqual(self.client.get(program.get_feed_url()).status_code, 404)

    def test_thumbless_latest_entry_progam_feed_view(self):
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        entry   = Entry.objects.create(released_at=datetime.now(), program=program, title='test entry')
        self.assertEqual(self.client.get(program.get_feed_url()).status_code, 404)

    def test_progam_feed_view(self):
        program = Program.objects.create(title='test program', owner=self.adolf, created_at=datetime.now())
        entry   = Entry.objects.create(released_at=datetime.now(), program=program, title='test entry', thumb_source='kekekeke')
        self.assertEqual(self.client.get(program.get_feed_url()).status_code, 200)