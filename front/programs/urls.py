from django.conf.urls import patterns, url

from views import ProgramsView, ProgramEntriesView
from feeds import ProgramFeed

urlpatterns = patterns('',
    url(r'^list/$', ProgramsView.as_view(), name='programs.view.list'),
    url(r'^view/(?P<slug>[-_A-Za-z0-9]+)/$', 'programs.views.item', name='programs.view.item'),
    url(r'^view/(?P<slug>[-_A-Za-z0-9]+)/entries/$', ProgramEntriesView.as_view(), name='programs.view.entries'),
    url(r'^feed/(?P<slug>[-_A-Za-z0-9]+)/$', ProgramFeed(), name='programs.feed.item'),
)
