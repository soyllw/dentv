#coding: utf-8
"""
Представления
~~~~~~~~~~~~~"""
from datetime import datetime
from django.shortcuts import redirect, Http404, get_object_or_404
from django.views.generic import ListView, DetailView

from utils.view_utils import TemplateViewMixin

from programs.models import Program
from entries.models import Entry


class ProgramsView(TemplateViewMixin, ListView):
    """Список программ"""
    template_name = 'programs/list.html'
    context_object_name = 'programs'
    # paginate_by = 20

    def get_queryset(self):
        "Получение выпусков программы"
        return []

    def get_context_data(self, **kwargs):
        """Установка дополнительных контекстных переменных"""
        context = super(ProgramsView, self).get_context_data(**kwargs)
        context['programs'] = Program.objects.filter(owner__id=1)
        return context


class ProgramEntriesView(TemplateViewMixin, ListView):
    """Список выпусков программы"""
    template_name = 'programs/entries_list.html'
    context_object_name = 'entries'
    paginate_by = 27

    def get_queryset(self):
        now = datetime.now()
        "Получение выпусков программы"
        self.program = get_object_or_404(Program, slug=self.kwargs['slug'])
        return self.program.entries.filter(released_at__lte=now).exclude(thumb_source="").all()

    def get_context_data(self, **kwargs):
        """Установка дополнительных контекстных переменных"""
        context = super(ProgramEntriesView, self).get_context_data(**kwargs)
        context['program'] = self.program
        return context


def item(request, slug):
    """Перенаправление на последний по времени выпуск программы по слагу самой программы."""
    try:
        program = Program.objects.get(slug=slug)
        entry = Entry.archive.filter(program=program).latest('released_at')
    except (Program.DoesNotExist, Entry.DoesNotExist):
        raise Http404
    return redirect(entry.get_absolute_url())
