# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.utils import feedgenerator
from django.shortcuts import get_object_or_404, Http404

from programs.models import Program
from entries.models import Entry

class ProgramFeed(Feed):
    feed_type = feedgenerator.Rss201rev2Feed
    ttl = 600
    
    def get_object(self, request, slug):
        return get_object_or_404(Program, slug = slug)

    def link(self, obj):
        return obj.get_absolute_url()

    def description(self, obj):
        return obj.description

    def items(self, obj):
        items = Entry.archive.filter(program=obj)[0:10]
        if len(items) == 0:
            raise Http404
        return items
    
    def item_link(self, obj):
        return obj.get_absolute_url()
    
    def item_title(self, obj):
        return obj.title
    
    def item_description(self, obj):
        return obj.description